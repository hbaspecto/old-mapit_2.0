#
# mapit_2.0/mapit2/qgis_util/qgis_util_main.py ---
#

from __future__ import (
    absolute_import,
    division,
    print_function,
    unicode_literals,
)

import argparse
import os
import pprint
import sys
import pdb

from .qgsfile import (
    QgsFile,
)


#####

DEBUG = None
VERBOSE = None
VERSION = '$Revision: 1.31 $'

#####

def main_func(raw_args):
    global DEBUG, VERBOSE, VERSION
    #
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.description = """
DESCRIPTION:

"""
    parser.epilog = """
EXAMPLES:

"""
    #
    g = parser.add_argument_group('GENERAL')
    g.add_argument("--debug", "-d",
                   action="store_true",
                   help="Turn on the debugging flag.")
    g.add_argument("--pdb",
                   action="store_true",
                   help="Enter the debugger after reading args.")
    g.add_argument("--verbose", "-v",
                   action="store_true",
                   help="Be more verbose.")

    g = parser.add_argument_group('QGIS UTIL')

    g.add_argument("--output-file","-O",
                   help="Path to output file. (only one file at a time.)")

    g.add_argument("--output-suffix",
                   default=".new",
                   help="Suffix for output files when not using '-O'.")

    g = parser.add_argument_group('QGIS UTIL DB CHANGE')

    g.add_argument("--db-change",
                   action="store_true",
                   help="Change the Postgres DB settings to ones from the current env. (PG*)")

    g = parser.add_argument_group('QGIS UTIL UPDATE')

    g.add_argument("--update-db-info",
                   action="store_true",
                   help="Update the DB info in the file from MAPITDB_DEFAULT_URI")

    g.add_argument("--update-metadata",
                   action="store_true",
                   help="Update some metadata in the file.")

    g.add_argument("--update-layer-ref",
                   metavar="SCHEMA.TABLE",
                   help="Update the first layer in the file (ref) to use SCHEMA.TABLE.")
    
    g.add_argument("--update-layer-policy",
                   metavar="SCHEMA.TABLE",
                   help="Update the second layer in the file (policy) to use SCHEMA.TABLE.")

    g.add_argument("--update-layer-diff",
                   metavar="SCHEMA.TABLE",
                   help="Update the third layer in the file (diff) to use SCHEMA.TABLE.")

    #
    g = parser.add_argument_group('ARGS')
    g.add_argument("args", nargs="*",
                   help="QGS files to process.")

    #
    args = parser.parse_args(raw_args)

    #
    if args.pdb:
        pdb.set_trace()
    if args.debug:
        DEBUG = args.debug
    if args.verbose:
        VERBOSE = args.verbose

    # Just change the DB.
    if args.db_change:
        for path in args.args:
            qgsfile=QgsFile(path=path)
            qgsfile.update_db_info(
                db_info={
                    # QGS name : ENV value.
                    "user": os.environ.get("PGUSER","postgres"),
                    "password": os.environ.get("PGPASSWORD","postgres"),
                    "host": os.environ.get("PGHOST","localhost"), # 
                    "port": os.environ.get("PGPORT","5432"),
                    "dbname": os.environ.get("PGDATABASE","postgres"),
                })
            #
            out_path=path+(args.output_suffix or ".changed")
            tmp_path=out_path+".tmp"
            qgsfile.write_file(tmp_path)
            os.rename(tmp_path,out_path)
            
        return True

    #
    for path in args.args:
        qgsfile=QgsFile(path=path)

        #
        if args.update_db_info:
            qgsfile.update_db_info()

        #
        if args.update_metadata:
            qgsfile.update_metadata()
        #
        if args.update_layer_ref:
            qgsfile.update_layer(1,args.update_layer_policy)
        if args.update_layer_policy:
            qgsfile.update_layer(2,args.update_layer_ref)
        if args.update_layer_diff:
            qgsfile.update_layer(3,args.update_layer_diff)

        #
        if args.output_file:
            qgsfile.write_file(args.output_file)
            break
        else:
            out_path=path+args.output_suffix
            qgsfile.write_file(out_path)

    return True

def main():
    rv=main_func(sys.argv[1:])
    if rv in [True,0]:
        sys.exit(0)
    else:
        sys.exit(1)

#
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
