#
# mapit_2.0/mapit2/qgis_util/qgisfile.py ---
#

"""We can generate QGIS files in a couple ways.

One way would be to make a template out of it, then expand the template.
This required editing it and inserting the template markers.
Which requires someone to edit the file and all that.

Another way is to use the XML parser and modifing the xml of
an existing QGIS file.  This means we dont have to edit it
by hand, rather we rewrite one as needed.

The second way means we can just drop in a new file
and the magic just happens.

"""

import io
import pprint
import re
import os

import xml.etree.ElementTree as ET

from mapit2.util import (
    parse_db_uri,
    lst_re_filter,
)

#####

MAPITDB_DEFAULT_URI = os.environ.get('MAPITDB_DEFAULT_URI')

DEBUG=False

#####

class QgsFile(object):
    """This is a Qgs file.
    It transforms the XML in a '.gqs' file in useful ways.
    """

    #: Pull out key=val data from the data.
    # We have key=val data embedded in the "source" attribs.
    # so do the next level of parsing ourselves.
    XML_KEYVAL_RE=re.compile(
        "(^| +)"
        "(?:(?P<key>[a-z0-9]+)=(?P<val>[^ ]*)|"
        "(?P<extra>\\(geom\\)))")

    #: Maps the keys QGS uses for db info to the keys 'parse_db_uri' uses.
    DBSOURCE_MAP_KEYS={
        # QGS : parse_db_uri
        'host': 'host',
        'port': 'port',
        'user': 'user',
        'password': 'password',
        'dbname': 'database',
        # the table info.
        'table': 'table',
    }

    ###

    def __init__(self, path=None,xml_data=None):
        self._path=None
        self._xml=None
        #: The default db info to update with.
        self._db_info_default={}
        #
        if path and xml_data:
            raise ValueError("cant give both a path and xml_data")
        elif path:
            self.read_file(path)
        elif xml_data:
            self.from_xml_data(xml_data)

    def __str__(self):
        return "#<{} {:#08x}>".format(
            self.__class__.__name__,
            id(self))

    ###

    def from_xml_data(self,xml_data):
        """Create the XML from the xml data."""
        self._path=None
        # ET.parse()      returns an ElementTree.
        # ET.fromstring() returns an Element.
        # So make it a tree as well.
        self._xml=ET.ElementTree(ET.fromstring(xml_data))

    def read_file(self,path):
        self._path=path
        # print("read_file: {!r}".format(path))
        self._xml=ET.parse(path)

    def write_file(self,path):
        print("write_file: {!r}".format(path))

        path_tmp=path+".tmp"
        with open(path_tmp,"wb") as fh:
            fh.write(self.as_xml_bytes())

        os.rename(path_tmp,path)

    def as_xml_bytes(self):
        """The QgsFile as XML.
        """
        # We would like to write:
        #   return self._xml.tostring(
        #      encoding="utf-8")
        # but that ends up with "AttributeError: 'ElementTree' object has no attribute 'tostring'"
        # even though the docs say that should work.

        #
        with io.BytesIO() as fh:
            self._xml.write(
                fh,
                encoding="utf-8")
            xml_bytes=fh.getvalue()
        return xml_bytes

    ###

    def dbsource_parse(self,source):
        """Parse a string like::

            dbname=at_mapit_prd host=leduc.office.hbaspecto.com port=5432

        returning [(key,val,), ....]
        """

        # print("source is ",source)
        kv_lst=[]
        for m in re.finditer(self.XML_KEYVAL_RE,source):
            extra=m.group("extra")
            if extra:
                kv_lst.append((extra,None,))
                continue
            #
            key=m.group("key")
            key=key.strip()
            val=m.group("val")
            val=val.strip()
            #val=val.strip('"')
            #val=val.strip("'")
            kv_lst.append((key,val,))

        # pprint.pprint(kv_lst)
        return kv_lst

    def dbsource_update(
            self,
            source,
            db_info):
        # pprint.pprint(db_info)

        source_kv_lst=self.dbsource_parse(source)

#        if True:
#            print("PRE: dbsource_update: source_kv_lst=",source_kv_lst)

        # We want to stuff in these keys every time.
        # So if they are missing, set them to "" so they are present.
        for key in ["user","password",]:
            # "filter()" returns an iterator; eval it.
            x=list(filter(lambda rec: rec[0]==key,source_kv_lst))
            if not x:
                source_kv_lst.append((key,""))

        # merge the passed info with defaults.
        merged_db_info=dict(self._db_info_default.items())
        merged_db_info.update(db_info)

        out_kv_lst=[]
        for key,val in source_kv_lst:
            # We dont like "authcfg", as it differs for each person,
            # and we will be providing the name and password anyways.
            # Leave it out.
            if key in ["authcfg",]:
                continue

            # singleton key
            if val is None:
                out_kv_lst.append(key)
                continue

            #
            mapped_key=self.DBSOURCE_MAP_KEYS.get(key)
            if mapped_key in merged_db_info:
                mapped_val=db_info.get(mapped_key)
                if mapped_val is not None:
                    val="{}".format(mapped_val)

            out_kv_lst.append("{}={}".format(key,val))

        #
        out=" ".join([item for item in out_kv_lst])

#        if True:
#            print("PRE: dbsource_update: out=",out)

        return out

    ###

    def maplayer_count(self):
        """Count the number of ``maplayers``.
        Should be 1 or 3 of them.

        Used to match QGS templates to mapviews.
        """

        if self._xml is None:
            return None

        count=0
        # The top level "<qgis>" tag is the root,
        # count the maplayers the user defined.
        for node in self._xml.findall("./projectlayers/maplayer"):
            count+=1
        return count

    ###

    def xml_find(self,path,none_ok=False):
        """Find the node or raise an error.
        error_ok=True means return None.
        """
        node=self._xml.find(path)

        if node is not None:
            return node

        print(f"xml_find: didnt find: {path}")
        if none_ok:
            return None

        # die!
        raise ValueError("update_layer: didnt find path: {}".format(path))

    #####

    def update_db_info(self,db_info=None):
        # print("update_db_info:")

        #
        if db_info is None:
            db_info=MAPITDB_DEFAULT_URI
        if isinstance(db_info,(str,)):
            db_info=parse_db_uri(db_info)

        if db_info:
            self._db_info_default.update(db_info.items())

        # pprint.pprint(self._db_info_default)

        # Change stuff line this:
        # <layer-tree-layer checked="Qt::Checked" expanded="0" id="T254_ref_quantity_20200903102255_xtab_geom_a23cb2a5_52e6_47f8_8d42_6928e21fcda6" name="T254_ref_quantity_20200903102255_xtab_geom" providerKey="postgres" source="dbname='at_mapit_prd' host=leduc.office.hbaspecto.com port=5432 user='postgres' key='zonenumber' estimatedmetadata=true type=MultiPolygon checkPrimaryKeyUnicity='1' table=&quot;mapit2_views&quot;.&quot;T254_ref_quantity_20200903102255_xtab_geom&quot; (geom) sql=">

        # The <layer-tree-layer> need their attribs updated.
        for node in self._xml.findall(".//layer-tree-layer"):
            # print(node)
            source_orig=node.attrib.get("source")
            source_new=self.dbsource_update(
                source=source_orig,
                db_info=db_info)
            node.attrib["source"]=source_new
            if False:
                print(f"layer-tree-layer source_orig={source_orig}")
                print(f"layer-tree-layer source_new={source_new}")

        # And the "<datasource>" needs it text updated.
        for node in self._xml.findall(".//datasource"):
            # print(node)
            source_orig=node.text
            source_new=self.dbsource_update(
                source=source_orig,
                db_info=db_info)
            node.text=source_new
            if DEBUG:
                print(f"datasource source_orig={source_orig}")
                print(f"datasource source_new={source_new}")

    def update_metadata(self):
        print("update_metadata")

        node=self.xml_find(".//layer-tree-group/layer-tree-layer[1]")
        node.attrib["name"]="REF"

        node=self.xml_find(".//layer-tree-group/layer-tree-layer[2]")
        node.attrib["name"]="POLICY"

        node=self.xml_find(".//layer-tree-group/layer-tree-layer[3]")
        node.attrib["name"]="DIFF"

    def update_layer(self,layer_idx,layer_schema_tablename):
        """Update the layer at INDEX with the tablename.

        The tablename has a time in it, so it ought to be ok as an id.

        Note: layer_idx is 1-indexed, as XML is 1-indexed.
        """

        (l_schema,l_tablename)=layer_schema_tablename.split(".",1)

        # properly quoted...
        q_schema_tablename='"{}"."{}"'.format(
            l_schema,
            l_tablename)

        print(f"update_layer[{layer_idx}]: {q_schema_tablename}")

        ### layer-tree-layer

        path=".//layer-tree-layer[{}]".format(layer_idx)
        layer_top=self.xml_find(path)

        if False:
            print("")
            print("update_layer: found ------------------------------")
            ET.dump(layer_top)

        layer_top.attrib["id"]=l_tablename
        layer_top.attrib["name"]=l_tablename
        # Quoting as we have capitals, and they need to be quoted for postgres.
        source_orig=layer_top.attrib["source"]
        source_new=self.dbsource_update(
	    source=source_orig,
            db_info={
	        'table':q_schema_tablename,
            })
        layer_top.attrib["source"]=source_new

        if False:
            ET.dump(layer_top)

        ### maplayer / { datasource | id | custom-order }

        path=".//maplayer[{}]".format(layer_idx)
        maplayer_node=self.xml_find(path)

        datasource_node=maplayer_node.find("datasource")
        source_orig=datasource_node.text
        source_new=self.dbsource_update(
            source=source_orig,
            db_info={
                'table':q_schema_tablename,
            })
        datasource_node.text=source_new

        id_node=maplayer_node.find(".//id")

        print("update_layer[{}]: Changing id from {!r} to {!r}".format(
            layer_idx,
            id_node.text,
            l_tablename))

        id_node.text=l_tablename

        layername_node=maplayer_node.find(".//layername")
        layername_node.text=l_tablename

        ### custom-order / item

        item_node=self.xml_find(
            ".//custom-order/item[{}]".format(
                layer_idx))
        item_node.text=l_tablename

        ### individual-layer-settings / layer-setting

        ls_node = self.xml_find(
            ".//individual-layer-settings/layer-setting[{}]".format(
                layer_idx))
        ls_node.attrib["id"]=l_tablename

        ### legendgroup / legendlayer

        # "example-occupied-residential-units.qgs" is missing this
        #
        # ll_node=self.xml_find(".//legendgroup/legendlayer[{}]".format(layer_idx))
        #
        # ll_node.attrib["name"]=l_tablename
        #
        # llf_node=ll_node.find(".//legendlayerfile")
        # llf_node.attrib["layerid"]=l_tablename

        ### layerorder / layer

        # lol_node=self.xml_find(".//layerorder/layer[{}]".format(layer_idx))
        # lol_node.attrib["id"]=l_tablename


    def maplayer_rm_stuff(self,maplayer_node):
        """Remove old stuff from the maplayer_node.
        These have lists of columns which we will fiddle with later.
        """

        node=maplayer_node.find("./editable")
        if node:
            node.clear()

        node=maplayer_node.find("./fieldConfiguration")
        if node:
            node.clear()

        node=maplayer_node.find("./labelOnTop")
        if node:
            node.clear()

        node=maplayer_node.find("./aliases")
        if node:
            node.clear()

        node=maplayer_node.find("./defaults")
        if node:
            node.clear()

        node=maplayer_node.find("./constraints")
        if node:
            node.clear()

        node=maplayer_node.find("./constraintExpressions")
        if node:
            node.clear()

        node=maplayer_node.find("./attributetableconfig/columns")
        if node:
            node.clear()

    def maplayer_update_histogram(
            self,
            layer_idx,
            bar_height=None,
            bar_value_max=None,
            bar_width=None,
            column_names=None):
        print(f"maplayer_update_histogram[{layer_idx}]")

        maplayer_node=self.xml_find(
            ".//maplayer[{}]".format(
                layer_idx))

        column_names=lst_re_filter(
            column_names,
            remove_regexes=["^geom"])

        self.maplayer_rm_stuff(maplayer_node)

        lidr_node=maplayer_node.find(".//LinearlyInterpolatedDiagramRenderer")
        if not lidr_node:
            return False

        lidr_node.attrib["classificationField"]=column_names[-1]

        # set
        lidr_node.attrib["lowerHeight"]=str(-bar_height)
        lidr_node.attrib["lowerValue"]=str(-bar_value_max)
        lidr_node.attrib["upperHeight"]=str(bar_height)
        lidr_node.attrib["upperValue"]=str(bar_value_max)

        dc_node=lidr_node.find("DiagramCategory")
        dc_node.attrib["barWidth"]=str(bar_width)
        dc_node.tail="\n"

        # save a copy.
        orig_attribute_node=dc_node.find("attribute")
        orig_attribute_node_attrs=dict(orig_attribute_node.items())

        # remove the old ones
        for node in dc_node.findall("attribute"):
            dc_node.remove(node)

        # Add new ones back in.
        for col_name in column_names:
            node=ET.SubElement(
                dc_node,
                "attribute",
                orig_attribute_node_attrs)
            node.attrib["field"]=col_name
            node.attrib["label"]=col_name
            node.tail="\n"

    def update_bar_colors(
            self,
            layer_idx,
            new_colors):

        maplayer_node=self.xml_find(
            ".//maplayer[{}]".format(
                layer_idx))

        lidr_node=maplayer_node.find(".//LinearlyInterpolatedDiagramRenderer")
        dc_node=lidr_node.find("DiagramCategory")

        for node in dc_node.findall("attribute"):
            if node.attrib["field"] in new_colors:
                print("Updating color of ", node.attrib["field"])
                node.attrib["color"] = new_colors[node.attrib["field"]]


    def maplayer_set_ranges(
            self,
            layer_idx,
            range_lst,
            minmax_column_name):
        """Set the list of ranges for the maplayer.
        """

        maplayer_node=self.xml_find(
            ".//maplayer[{}]".format(
                layer_idx))

        # remove old info.
        self.maplayer_rm_stuff(maplayer_node)

        # ET.dump(maplayer_node)

        ### maplayer / renderer-v2 / ranges / range

#        tmp_lst=lst_re_filter(
#            col_names,
#            remove_regexes=["^geom","^zone"])
#        minmax_column_name=tmp_lst[-1]
#
#        print("### renderer-v2: col_names: {!r}".format(
#            col_names))
#        print("### renderer-v2: tmp_lst: {!r}".format(
#            tmp_lst))
#        print("### renderer-v2: changing from {!r} to {!r}".format(
#            renderer_node.attrib["attr"],
#            minmax_column_name))

        # FIXME: this needs to be the name, but is ending up as "2"
        renderer_node=maplayer_node.find("./renderer-v2")
        renderer_node.attrib["attr"]=minmax_column_name

        # Clear out ranges for replacement...
        ranges_node=renderer_node.find("./ranges")
        tmp_attrib=ranges_node.items()
        ranges_node.clear()
        ranges_node.attrib.update(tmp_attrib)
        ranges_node.tail="\n"

        # Add the new ranges.
        val_a=range_lst[0]
        for idx,val_b in enumerate(range_lst[1:]):
            # <range upper="-4960.000000000000000"
            #        label="-6,200 - -4,960 "
            #        lower="-6200.000000000000000"
            #        symbol="0"
            #        render="true"/>

            r_node=ET.SubElement(
                ranges_node,
                "range",
                attrib={
                    "label":"{:+.6} - {:+.6}".format(val_a,val_b),
                    "lower":"{}".format(val_a),
                    "render":"true",
                    "symbol":"{}".format(idx),
                    "upper":"{}".format(val_b),
                })
            r_node.tail="\n"
            #
            val_a=val_b

        ET.dump(ranges_node)
        #ET.dump(maplayer_node)

        return True
