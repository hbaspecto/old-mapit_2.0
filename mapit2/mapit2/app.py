import os

import flask

from . import (
    appbase,
    index_page,
    view_page_generic,
)
from .app_autoimport import (
    find_and_import_page_files,
)
from .appbase import (
    app,
    define_page_by_category_build,
)
from .env_vars import *
from .models.mapqgsfile import (
    MapQgsFile,
    build_site_templates_qgs_files,
)
from .util import (
    printenv,
)

#####

# normally we would have to add each page import here,
# like:
#
# from .employment_by_naics import (
#    employment_by_naics_page,
# )
#
# So users dont have to edit this file,
# we search for and import the pages.

find_and_import_page_files()

# call it at the start to build DEFINED_PAGES_BY_CATEGORY
define_page_by_category_build()

#####


def server_run():
    # printenv()
    app.run(
        debug=MAPIT2_SERVER_DEBUG,
        host=MAPIT2_SERVER_HOST,
        port=MAPIT2_SERVER_PORT)
