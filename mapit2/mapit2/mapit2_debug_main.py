#
# mapit_2.0/mapit2/mapit2/mapit2_debug_main.py ---
#
# Use this to test and debug mapit2 jobs.
#

import argparse
import logging
import os
import pdb
import pprint
import sys
import traceback

import yaml

from .app_autoimport import (
    find_and_import_page_files,
)
from .common_vars import *
from .models.mapqgsfile import (
    MapQgsFile,
)
from .models.mapview_superclass import (
    MapView,
)
from .rqjob.base import (
    get_db_session,
    rqjob_run_object_call,
)
from .util import (
    setup_logging,
)

#####

DEBUG = None
VERBOSE = None
VERSION = '$Revision: 1.31 $'

#####

logger = logging.getLogger(__name__)

find_and_import_page_files()

#####


def run_fixture_item(item):
    """Runs an item as if it were called from RQ.
    This way we can use the debugger on it.
    """

    pprint.pprint(item)

    name = item.get("name")
    if name == "rqjob_run_object_call":
        item_class = item.get("item_class")
        if item_class == "MapQgsFile":
            item_class = MapQgsFile
        elif item_class == "MapView":
            item_class = MapView
        else:
            raise ValueError("Unknown class: {}".format(item_class))
        #
        item_id = int(item.get("item_id"))
        #
        method_name = item.get("method_name")
        method_args = item.get("method_args", [])
        method_kwargs = item.get("method_kwargs", {})
        #
        return rqjob_run_object_call(
            item_class,
            item_id,
            method_name,
            method_args,
            method_kwargs)

    #
    raise ValueError("run_fixture_item: unknown name: {!r}".format(
        func_name))


def run_fixture_yaml(path):
    with open(path, "r") as fh:
        docs = list(yaml.safe_load_all(fh))
        # pprint.pprint(docs)

    for doc in docs:
        # pprint.pprint(doc)
        data = doc.get("mapit2_debug")
        # pprint.pprint(data)
        for item in data:
            run_fixture_item(item)


def debug_make_views(item_id):
    """Find ad run the view so we can debug  directly.
    """
    item_id = int(item_id)

    print("debug_view_id: {!r}".format(item_id))

    session = get_db_session()
    print("session: {}".format(session))

    q = session.query(MapView)
    q = q.filter(MapView.id == item_id)
    item = q.first()

    print(item)

    # Match the "rqjob_update" call sequence in rqjob.base.
    item.rqjob_update(
        status="running",
        message="running in debugger.")

    args = ['luz_v4', 'luz']
    kwargs = {}

    try:
        item.make_views(*args, **kwargs)

        item.rqjob_update(
            status="done",
            message="")

    except BaseException:
        item.rqjob_update(
            status="error",
            message="doh!")

    return True

#####


def main(raw_args):
    global DEBUG, VERBOSE, VERSION

    setup_logging()

    #
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.description = """
DESCRIPTION:

``mapit2-debug`` is a program to help with debugging.

Sometimes RQ doesnt tell you what went wrong, eating the error.
This program allows you to run some of that code from a terminal,
where you can use a debugger on it.

"""
    parser.epilog = """
EXAMPLES:

To run the ``make_views`` method on the MapView with id 392:

    mapit2-debug --debug-make-views 392

"""
    #
    g = parser.add_argument_group('GENERAL')
    g.add_argument("--debug", "-d",
                   action="store_true",
                   help="Turn on the debugging flag.")
    g.add_argument("--pdb",
                   action="store_true",
                   help="Enter the debugger after reading args.")
    g.add_argument("--verbose", "-v",
                   action="store_true",
                   help="Be more verbose.")

    g = parser.add_argument_group('MAPIT-2 DEBUG')

    g.add_argument("--run-fixture",
                   action="store_true",
                   help="Runs a fixture file for debugging.")

    g.add_argument("--debug-make-views",
                   metavar="MAPVIEW_ID",
                   help="Reruns the `make_views` call without RQ; So you can debug.")

    #
    g = parser.add_argument_group('ARGS')
    g.add_argument("args", nargs="*",
                   help="The remaining args.")

    #
    args = parser.parse_args(raw_args)
    #
    if args.pdb:
        pdb.set_trace()
    if args.debug:
        DEBUG = args.debug
    if args.verbose:
        VERBOSE = args.verbose

    #
    if args.debug_make_views:
        debug_make_views(args.debug_make_views)
        return 0

    #
    if args.run_fixture:
        for path in args.args:
            print("run_fixture: {!r}".format(path))
            run_fixture_yaml(path)
        return 0

    #
    return 0

#####


def main_entry():
    sys.exit(main(sys.argv[1:]))


#
if __name__ == "__main__":
    main_entry()
