from ..common_vars import *
from .occupied_units import (
    OccupiedUnits,
    OccupiedUnitsForm,
)

# TODO can we override this as a combo update/create (i.e. if id is null then create, otherwise update on POST
# and just review on GET)


@app.route('/occupied_units/create', methods=['POST', 'GET'])
def occupied_units():
    # TODO CSRF protection
    # TODO this default URI shouldn't be here, but at least it's from the environment
    item = OccupiedUnits(
        db_connection_string=MAPITDB_DEFAULT_URI,
        # TODO put this in environment
        schema_for_views=MAPIT2_VIEWS_SCHEMA,
    )
    form = OccupiedUnitsForm()
    if flask.request.method == 'GET':
        item.initializeForm(form)

    message_err = None
    message_ok = None

    # A function to process the form, so we can use "return"
    def process_form():
        nonlocal message_ok, message_err

        # dont do anything if button not clicked, e.g. if POST is from a API call.
        if not form.button.data:
            return
        if not form.validate():
            return

        item.copy_from_form(form)
        db.session.add(item)
        db.session.commit()

        try:
            if rqjob_enabled():
                rqjob_enqueue_call(
                    item,
                    "make_views",
                    method_args=[luz_table, luz_key])
            else:
                item.make_views(luz_table, luz_key)

        except Exception as e:
            print('There was an issue creating your data views, ' + str(e))
            return 'There was an issue creating your data views, ' + str(e)

        return None

    if flask.request.method == 'POST':
        if form.validate():
            print("Posting units view...")
            # TODO test for content type JSON because we want this to be an API as well.
            # Store it
            vals = process_form()
            if vals is None:
                return flask.redirect(flask.url_for('index'))
            else:
                message_err = vals
                # return flask.render_template('occupied_units/occupied_units.html',  **locals())
        else:
            message_err = form.errors

    return flask.render_template('occupied_units/occupied_units.html', **locals())


@app.route('/occupied_units/view/<int:id>', methods=['GET', ])
def view_occupied_units(id):
    item = db.session.query(OccupiedUnits).get_or_404(id)
    response = flask.make_response(item.generate_layer_file(), 200)
    response.mimetype = "text/plain"
    response.headers.set(
        'Content-Disposition', 'attachment', filename='{view}.qlr'.format(view=item.data_view_name))
    return response

#####


define_page(
    func='occupied_units',
    category="14: Units",
    title="Occupied units comparison for two scenarios")
