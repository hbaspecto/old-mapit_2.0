select
   scenario_ref.zonenumber,
   scenario_ref.year_run,
   scenario_policy.number_of_hh_using_space_type - scenario_ref.number_of_hh_using_space_type as hh_occup_diff

from
(
SELECT
       a.zonenumber,
       a.year_run,
       sum(a.probability * b.quantity) AS number_of_hh_using_space_type

FROM
  output.all_technology_choice_widx a
  JOIN output.activity_numbers an ON an.activitynumber = a.activity
 JOIN output.all_activity_locations b ON an.activity::text = b.activity::text AND
      a.scenario::text = b.scenario::text AND
      a.zonenumber = b.zonenumber AND a.year_run = b.year_run
 JOIN output.option_use_rates c ON a.option_id = c.option_id
 JOIN output.technology_space_type x ON a.option_id = x.id
WHERE
      a.scenario::text = '{{ item.scenario_ref }}' and
      {{item.year_start}} <= a.year_run and
      a.year_run <= {{item.year_end}} and
      an.activity_type_id = 2 AND
      an.activity != 'HH_RCPatients' and
      b.quantity > 0 and a.probability > 0 and
      x.commodity in ({{ item.commodities | safe }})

GROUP BY
  a.zonenumber,
  a.year_run
order by
  a.zonenumber,
  a.year_run
) scenario_ref

full join

(
SELECT
       a.zonenumber,
       a.year_run,
       sum(a.probability * b.quantity) AS number_of_hh_using_space_type

FROM output.all_technology_choice_widx a
 JOIN output.activity_numbers an ON an.activitynumber = a.activity
 JOIN output.all_activity_locations b ON an.activity::text = b.activity::text AND
      a.scenario::text = b.scenario::text AND a.zonenumber = b.zonenumber AND a.year_run = b.year_run
 JOIN output.option_use_rates c ON a.option_id = c.option_id
 JOIN output.technology_space_type x ON a.option_id = x.id

WHERE
  a.scenario::text = '{{ item.scenario_policy }}' and
  {{item.year_start}} <= a.year_run and
  a.year_run <= {{item.year_end}} and
  an.activity_type_id = 2 AND
  an.activity != 'HH_RCPatients' and
  b.quantity > 0 and
  a.probability > 0 and
  x.commodity in ({{ item.commodities | safe }})

GROUP BY
  a.zonenumber,
  a.year_run
order by
  a.zonenumber,
  a.year_run
) scenario_policy

using (zonenumber, year_run)

where
      scenario_ref.year_run is not null
      and
      scenario_policy.year_run is not null

order by
      zonenumber,
      year_run
