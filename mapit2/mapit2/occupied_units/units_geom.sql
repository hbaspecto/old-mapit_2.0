-- View: manual_views.un_mfe_p243_geom

-- DROP VIEW manual_views.un_mfe_p243_geom;

CREATE OR REPLACE VIEW manual_views.un_mfe_p243_geom
 AS
 SELECT a.luz,
    a."2019",
    a."2021",
    a."2023",
    a."2025",
    a."2027",
    a."2029",
    a."2031",
    a."2033",
    a."2035",
    a."2037",
    a."2039",
    c.geom
   FROM manual_views.un_mfe_p243 a
     JOIN luz_v4 c ON a.luz = c.luz;

ALTER TABLE manual_views.un_mfe_p243_geom
    OWNER TO postgres;
