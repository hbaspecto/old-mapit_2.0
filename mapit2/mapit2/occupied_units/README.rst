User specifies
===============


scenario_policy (policy scenario)
scenario_ref (reference scenario)

commodities (comma separated list of space type names)

year_start (default 2020)
year_end (default 2039)

Generate two through-time crosstab of values, one for each scenario (the standard
three views, longthin, the crosstab materialized view, and the joined view to the LUZs

Generate another through-time crosstab of differences in values (again, 3 views)

template out the three different view names, and download a QGIS project.



