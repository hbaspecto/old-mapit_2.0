#
# mapit_2.0/mapit2/mapit2/util.py ---
#

import datetime
import logging
import math
import os
import re
import sys

#####

DEBUG = False

#: the default logger
logger = logging.getLogger(__name__)

#: The filename for logs,
MAPIT2_LOG_FILE = os.environ.get("MAPIT2_LOG_FILE")

#: The level of logs to show.
MAPIT2_LOG_LEVEL = logging.DEBUG

#: This is for humans - servers should send to the file only.
MAPIT2_LOG_CONSOLE = bool(int(os.environ.get("MAPIT2_LOG_CONSOLE", 1)))
MAPIT2_LOG_CONSOLE = True

#####


def setup_logging(filename=None, level=None):
    """Setup logging.
    Call this in entrypoint of your app.
    """

    l_level = level or MAPIT2_LOG_LEVEL
    if filename:
        l_filename = filename
    elif MAPIT2_LOG_FILE in [None, 0, "0"]:
        l_filename = None
    else:
        l_filename = MAPIT2_LOG_FILE

    if l_filename:
        now = datetime.datetime.now()
        l_filename = l_filename.format(
            yyyy=now.strftime("%Y"),
            mm=now.strftime("%m"),
            dd=now.strftime("%d"),
            HH=now.strftime("%H"),
            MM=now.strftime("%M"),
            SS=now.strftime("%S"))

    l_formatter = logging.Formatter(
        fmt="%(asctime)s: %(levelname)s: %(message)s",
        datefmt="%Y%m%dT%H%M%S")

    # Configure the top logger.
    # set the level here otherwise the handlers wont see DEBUG.
    top_logger = logging.getLogger()
    top_logger.setLevel(l_level)

    # open the logfile
    if l_filename:
        handler_file = logging.FileHandler(filename=l_filename)
        handler_file.setLevel(l_level)
        handler_file.setFormatter(l_formatter)
        #
        top_logger.addHandler(handler_file)

    # send the logs to stdout as well.
    if MAPIT2_LOG_CONSOLE:
        handler_console = logging.StreamHandler(sys.stdout)
        handler_console.setLevel(l_level)
        handler_console.setFormatter(l_formatter)
        #
        top_logger.addHandler(handler_console)

    logger.info("starting logging to %s %s", l_filename, l_level)

#####


def columns_for_class_table(cls):
    """Return the columns this class has in a table.
    If the class doesnt have a table, then empty list.
    """
    if not hasattr(cls, "__table__"):
        return []

    # We do have a table! Sort and return.
    cols = cls.__table__.columns.values()
    cols = sorted(cols, key=lambda col: col._creation_order)
    return cols


def columns_for_class(cls):
    """Returns a list of columns for a sqlalchemy class.
    Our classes have subtyping, so we need to collect
    the colums we have.

    ``cls._sa_class_manager.values`` has a list of columns,
    but they arent sorted.  So we walk the classes and build our own.
    """
    cols = []
    for super_cls in cls.__mro__[::-1]:
        cols.extend(columns_for_class_table(super_cls))
    return cols

#####


def yyyymmddThhmmss(now=None):
    """Return a time like ``yyyymmddThhmmss``."""
    now = now or datetime.datetime.now()
    return now.strftime("%Y%m%dT%H%M%S")


def datetime_create_func():
    """Return the time we were created.
    As a function, to pass to 'default='.

    We use localtime, as that is less confusing than 'utcnow'.
    """
    now = datetime.datetime.now()
    # truncate, so it looks pretty.
    now = now.replace(
        microsecond=0)
    print("### datetime_create_func", now)
    return now


def datetime_expire_func(base=None):
    """Return the time we will expire.
    As a function, to pass to 'default='.
    Default is a year from now.
    """
    expire = base or datetime_create_func()
    # add a year
    expire = expire + datetime.timedelta(days=365)
    # truncate to midnight.
    expire = expire.replace(
        hour=0,
        minute=0,
        second=0,
        microsecond=0)
    #
    print("### datetime_expire_func:", expire)
    return expire

#####


PARSE_DB_URI_RE = re.compile(
    "(?P<scheme>[-_a-z0-9]*)?"
    "://"
    "((?P<user>[-_a-zA-Z0-9]+)?(:(?P<password>.*))?@)?"
    "(?P<host>[-_a-zA-Z0-9.]+)?"
    "(:(?P<port>[0-9]+))?"
    "/"
    "(?P<database>[-_a-zA-Z0-9]+)?")


def parse_db_uri(uri):
    """Parse a database URI and extract the info out.
    Return None if the parse failed.

    postgresql://${PGUSER}:${PGPASSWORD}@${PGHOST}:${PGPORT}/${PGDATABASE}
    """
    if not uri:
        return None

    m = re.match(PARSE_DB_URI_RE, uri)

    if not m:
        return None

    # print("parse_db_uri: {!r}".format(uri))
    # print(m.groupdict())
    return m.groupdict()

#####


def printenv():
    """Print os.environ.  (for debugging.)"""
    print("printenv ----------")
    for key, val in sorted(os.environ.items()):
        print("{}={!r}".format(key, val))
    print()

#####


def int_safe(value):
    """Turn the value into an int or None."""
    try:
        return int(value)
    except BaseException:
        return None


def float_safe(value, default=None):
    """Turn a value into a float or None."""
    try:
        return float(value)
    except BaseException:
        return default


def float_range(f_start, f_end, num_steps=None, f_step=None):
    """Generate a range from f_start to f_end by f_step, including the start and end.
    """
    f_start = float(f_start)
    f_end = float(f_end)

    # num steps
    if f_step is not None:
        f_step = float(f_step)
    if num_steps:
        f_step = (f_end - f_start) / num_steps

    lst = []
    f_cur = f_start
    while f_cur < f_end:
        lst.append(f_cur)
        f_cur += f_step
    lst.append(f_end)

    return lst

#####


def lst_re_filter(
        lst,
        keep_regexes=None,
        remove_regexes=None):
    """Copy lst and filter by keep and remove regexes.
    Preserve the order of the list.
    """

    if lst is None:
        return None

    if not isinstance(lst, (list,)):
        raise ValueError("lst_re_filter: not a list: {!r}".format(lst))

    # cast to lists.
    if keep_regexes and \
       (not isinstance(keep_regexes, (list,))):
        keep_regexes = [keep_regexes]
    if remove_regexes and \
       (not isinstance(remove_regexes, (list,))):
        remove_regexes = [remove_regexes]

    if DEBUG:
        print("lst_re_filter:")
        print("  keep_regexes={!r}".format(keep_regexes))
        print("  remove_regexes={!r}".format(remove_regexes))

    # handy internal funcs.

    def is_keep(item):
        if not keep_regexes:
            return None
        for keep_re in keep_regexes:
            if re.search(keep_re, item):
                return True
        return False

    def is_remove(item):
        if not remove_regexes:
            return None
        for remove_re in remove_regexes:
            if re.search(remove_re, item):
                return True
        return False

    ###

    rv = []

    for item in lst:
        if DEBUG:
            print("lst_re_filter: item: {!r}".format(item))
        k = is_keep(item)
        if k is True:
            rv.append(item)
            continue
        if k is False:
            continue
        #
        if is_remove(item) is True:
            continue
        #
        rv.append(item)

    return rv

#####


def round_pretty(value,
                 mode=None,
                 digits=None):
    """Do some pretty rounding, selecteded by the mode.

    mode:
      number=
    """

    # digits -> use it.
    tmp = int_safe(digits or mode)
    if isinstance(tmp, (int,)):
        return round_pretty_digits(value, digits=tmp)

    # should be one of these two.
    if mode in [None, "log"]:
        return round_pretty_log(value)

    raise ValueError("round_pretty: invalid rounding mode")


def round_pretty_digits(value, digits=None):
    """Round the value in a pretty way.
    We always round UP, as this will be used for the map scales,
    and we need them to be larger than the data.

    143.456 => 150.0


    python3 -m unittest test_util.TestRoundPretty

    """
    # digits is the number of digits to keep.
    #    1230000 = 3
    #     45.600 = 3
    if digits is None:
        digits = 3

    v_tmp = value
    if v_tmp is None:
        return None

    v_tmp = float(v_tmp)

    if v_tmp == 0.0:
        return v_tmp

    #
    v_sign = +1.0
    if v_tmp < 0:
        v_sign = -1.0
        v_tmp = abs(v_tmp)

    #
    v_digits = math.ceil(math.log10(v_tmp))
    v_scale = math.pow(10.0, v_digits - digits)
    v_tmp = math.ceil(v_tmp / v_scale) * v_scale
    v_tmp = v_tmp * v_sign

    if DEBUG:
        print("### round_pretty_digits({!r},{!r}) -> {!r}".format(
            value, digits, v_tmp))

    return v_tmp


#: The log-like values for rounding.
ROUND_PRETTY_LOG_VALUES = [
    1.0,
    1.2,
    1.5,
    2.0,
    2.5,
    3.0,
    3.5,
    4.0,
    5.0,
    6.0,
    8.0,
    10.0,
]


def round_pretty_log(value, log_values=None):
    """Round the value to one of the values in ROUND_PRETTY_LOG_VALUES."""

    if log_values is None:
        log_values = ROUND_PRETTY_LOG_VALUES

    if value is None:
        return None

    v_tmp = float(value)

    if v_tmp == 0.0:
        return v_tmp

    v_sign = +1.0
    if v_tmp < 0.0:
        v_sign = -1.0
        v_tmp = abs(v_tmp)

    print("### round_pretty_log: v_tmp={}".format(v_tmp))
    v_digits = math.floor(math.log10(v_tmp))
    v_scale = math.pow(10.0, v_digits)
    v_tmp = v_tmp / v_scale

    for last_value in log_values:
        if v_tmp <= last_value:
            break
    #
    v_tmp = v_sign * last_value * v_scale
    if DEBUG:
        print("### round_pretty_log: {!r} -> {!r}".format(
            value, v_tmp))
    return v_tmp
