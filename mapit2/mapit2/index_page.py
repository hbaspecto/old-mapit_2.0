#
# mapit_2.0/mapit2/mapit2/index_page.py ---
#

from .common_vars import *

#####


class IndexRefreshForm(wtforms.Form):
    refresh_sec = wtforms.fields.SelectField(
        id="refresh_freq",
        choices=[
            (0, "none",),
            (5, "5s",),
            (30, "30s",),
            (60, "60s",),
            (90, "90s",),
        ])


@app.route(
    '/',
    methods=['GET', 'POST'])
def index():

    form = IndexRefreshForm(flask.request.form)

    # new items at the top, were we can see them.
    q = db.session.query(MapView)
    # 'id' is better then 'date_created'.
    q = q.order_by(MapView.id.desc())
    q = q.limit(INDEX_PAGE_ROW_LIMT)
    items = q.all()

    # print("There are %r views" % len(items))
    return flask.render_template(
        'templates/index.html',
        form=form,
        items=items)
