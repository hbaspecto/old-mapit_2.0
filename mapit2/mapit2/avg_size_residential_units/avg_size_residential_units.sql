SELECT
  a.zonenumber,
  a.year_run,
  -(sum(a.probability * b.quantity * c.use_rate))/(sum(a.probability * b.quantity)) as use_rate

FROM
  output.all_technology_choice_widx a
  JOIN output.activity_numbers an ON an.activitynumber = a.activity
  JOIN output.all_activity_locations b ON an.activity::text = b.activity::text AND
       a.scenario::text = b.scenario::text AND
       a.zonenumber = b.zonenumber AND
       a.year_run = b.year_run
  JOIN output.option_use_rates c ON a.option_id = c.option_id
  JOIN output.technology_space_type x ON a.option_id = x.id

WHERE
      a.scenario = '{{scenario}}' and
      {{item.year_start}} <= a.year_run and
      a.year_run <= {{item.year_end}} and
      an.activity_type_id = 2 and
      an.activity != 'HH_RCPatients' and
      x.commodity like '%%R0%%' and
      b.quantity > 0 and
      a.probability > 0

GROUP BY
      a.zonenumber,
      a.year_run

ORDER BY
      a.zonenumber,
      a.year_run
