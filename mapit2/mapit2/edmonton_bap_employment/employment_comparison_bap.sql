select
        coalesce(scenario_bap.luz,scenario_pol.luz) as luz,
        scenario_pol.year_run as year_run,
        case when scenario_bap.employment is null then null else coalesce(scenario_pol.employment,0.0) - scenario_bap.employment end as employment_diff
from
(

select
       R.taz/100 as luz,
       R.year_run,
       sum(R.amt) as employment
from
     output.all_employment R
where
      R.scenario = '{{item.scenario_policy}}' and
      {{item.year_start}} <= R.year_run and
      R.year_run <= {{item.year_end}}
group by
      R.taz/100,
      R.year_run
order by
      R.taz/100,
      R.year_run
) scenario_pol

full join

(
select
		"LUZ" as luz,
       "Emp_BAP" as employment
from
	mapit2_edmonton_data.bap_cpp_2016_by_luz
order by
      luz
) scenario_bap

using (luz)

where
    scenario_pol.year_run is not null

order by
      luz,
      year_run
