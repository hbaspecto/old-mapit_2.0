#
# mapit_2.0/mapit2/mapit2/env_vars.py ---
#

import os

#####

#: Directory source is in.
MAPIT2_DIR = os.environ['MAPIT2_DIR']

#: The tennant of this mapit2 instance. (hba, edmonton, ...)
MAPIT2_TENANT = os.environ.get(
    "MAPIT2_TENANT",
    "hba")

#: The deploment level of this mapit2 instance. (dev, stg, prd, local, ...)
MAPIT2_DEPLOY = os.environ.get(
    "MAPIT2_DEPLOY",
    "dev")

#: The DB uri for the mapit2 db. (not the data!)
MAPIT2_DB_URI = os.environ.get('MAPIT2_DB_URI')

#: The DB url for the data DB. (not mapit2)
MAPITDB_DEFAULT_URI = os.environ.get('MAPITDB_DEFAULT_URI')

#: Webserver IP to listen on. (default is all)
MAPIT2_SERVER_HOST = os.environ.get("MAPIT2_SERVER_HOST", "0.0.0.0")

#: Webserver port to listen on. (
MAPIT2_SERVER_PORT = int(os.environ.get("MAPIT2_SERVER_PORT", "5733"))

#: schema to put our tables and views into.
MAPIT2_VIEWS_SCHEMA = os.environ.get('MAPIT2_VIEWS_SCHEMA')

#: The schema to use for the app.
#  put it here so we can change it if needed.
MAPIT2_APP_SCHEMA = "mapit2_app"

#: Enable debugging stuff.
MAPIT2_SERVER_DEBUG = bool(int(os.environ.get("MAPIT2_SERVER_DEBUG", 0)))

#: Where we find templates for this site. (QGS files etc...)
MAPIT2_SITE_TEMPLATES_DIR = os.environ.get("MAPIT2_SITE_TEMPLATES_DIR")

#: These env vars are set in "hba-setup.env".
luz_table = os.environ.get("MAPIT2_LUZ_TABLE")
luz_key = os.environ.get("MAPIT2_LUZ_KEY")
taz_table = os.environ.get("MAPIT2_TAZ_TABLE")
taz_key = os.environ.get("MAPIT2_TAZ_KEY")

#####

# UI prefs

#: Limit number of rows on the index page.
INDEX_PAGE_ROW_LIMT = 500

#####

# RQ

#: URI for the redis worker.
MAPIT2_REDIS_URL = os.environ.get("MAPIT2_REDIS_URL")

#: The base queue name for our work queues.
#:
MAPIT2_RQ_QUEUE_BASE = os.environ.get(
    "MAPIT2_RQ_QUEUE_BASE",
    "mapit2-{}-{}".format(
        MAPIT2_TENANT,
        MAPIT2_DEPLOY))

#: The queue which processes QGS files.
MAPIT2_RQ_QUEUE_QGS = os.environ.get(
    "MAPIT2_RQ_QUEUE_QGS")

#: The queue which processes views.
MAPIT2_RQ_QUEUE_VIEW = os.environ.get(
    "MAPIT2_RQ_QUEUE_VIEW")
