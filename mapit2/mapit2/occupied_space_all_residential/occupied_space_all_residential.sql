select
  zonenumber,
  year_run,
  sum(-amount) as quantity
from
  output.all_zonalmakeuse_act_com
where
  -- The percent character is normally python escape char.
  -- So double it up so Sqlalchemy doesnt process it.
  scenario = '{{scenario}}' and
  {{item.year_start}} <= year_run and
  year_run <= {{item.year_end}} and
  commodity like '%%R0%%'
group by
  zonenumber,
  year_run
