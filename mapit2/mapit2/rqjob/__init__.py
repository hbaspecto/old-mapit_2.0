#
# mapit_2.0/mapit2/mapit2/rqjob/__init__.py ---
#
# https://python-rq.org/docs/jobs/
#

from .base import (
    rqjob_enabled,
    rqjob_enqueue_call,
)
