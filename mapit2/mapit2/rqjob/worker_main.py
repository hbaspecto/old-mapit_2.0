#
# mapit_2.0/mapit2/mapit2/rqjob/worker_main.py ---
#
# https://python-rq.org/docs/workers/
#

import argparse
import os
import sys

import redis
import rq

from ..app_autoimport import (
    find_and_import_page_files,
)
from ..common_vars import *
from ..util import (
    setup_logging,
)
from .base import *

#####

#: Crank up the logging, so we can see what is going on.
LOGGING_LEVEL = "DEBUG"

# max_jobs is the number of jobs before this worker exits.
# not the number of jobs to run at one time.

# The worker has the code for doing views and qgsfiles.
# By convention we put the views into the "view queue"
# and the qgs files into "qgs queue".

#####

setup_logging()
find_and_import_page_files()


def main(raw_args):

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.description = """
DESCRIPTION:

mapit2-rq-worker runs the backend worker.

"""
    parser.epilog = """
EXAMPLES:

mapit2-rq-worker --queue-view --queue-qgs

"""
    #
    g = parser.add_argument_group('GENERAL')
    g.add_argument("--debug", "-d",
                   action="store_true",
                   help="Turn on the debugging flag.")

    g.add_argument("--work-qgs",
                   action="store_true",
                   help="Work on the QGS queue.")
    g.add_argument("--work-view",
                   action="store_true",
                   help="Work on the VIEW queue.")
    #
    args = parser.parse_args(raw_args)
    #
    if args.debug:
        DEBUG = args.debug

    print("MAPIT2_DIR={!r}".format(
        MAPIT2_DIR))
    print("MAPIT2_REDIS_URL={!r}".format(
        MAPIT2_REDIS_URL))
    print("MAPIT2_RQ_QUEUE_QGS={!r}".format(
        MAPIT2_RQ_QUEUE_QGS))
    print("MAPIT2_RQ_QUEUE_VIEW={!r}".format(
        MAPIT2_RQ_QUEUE_QGS))

    #
    queue_names = []
    if args.work_qgs:
        queue_names.append(MAPIT2_RQ_QUEUE_QGS)
    if args.work_view:
        queue_names.append(MAPIT2_RQ_QUEUE_VIEW)

    # if no args, work on both.
    if not queue_names:
        queue_names.extend([
            MAPIT2_RQ_QUEUE_QGS,
            MAPIT2_RQ_QUEUE_VIEW,
        ])

    print("### working on: {!r}".format(
        queue_names))

    redis_conn = redis.Redis.from_url(MAPIT2_REDIS_URL)

    with rq.Connection(redis_conn):
        worker = rq.Worker(
            queue_names)
        worker.work(
            logging_level=LOGGING_LEVEL)

    # the above shouldnt exit.
    # This is just in case.
    return 0

#####


def worker_main():
    rv = main(sys.argv[1:])
    sys.exit(rv)


#
if __name__ == "__main__":
    worker_main()
