#
# mapit_2.0/mapit2/mapit2/rqjob/base.py ---
#

import logging
import os
import traceback

import redis
import rq
import sqlalchemy

from ..env_vars import *
from ..models.mapqgsfile import (
    MapQgsFile,
)
from ..models.mapview_superclass import (
    MapView,
)

#####

#: set job timeout to 1h
JOB_TIMEOUT = 60 * 60

logger = logging.getLogger(__name__)

#####


def rqjob_enabled():
    """Return True if RQ is enabled.
    """
    if MAPIT2_REDIS_URL:
        return True
    return False


def rqjob_enqueue_call(
        item,
        method_name,
        method_args=None,
        method_kwargs=None,
        queue_name=None):
    """Enqueue this method call to the RQ queue.

    We dont want to stuff an object with all its state into the queue.
    so we call a function "rqjob_run_XXX_call" which takes the id.
    the remote end will get the object from the DB.
    """

    # @TODO get fancy and do the function call ourselves if not enabled.
    if not rqjob_enabled():
        traceback.print_exc()
        raise ValueError("RQJOB is not enabled.")

    # default to the view queue if not set.
    if not queue_name:
        queue_name = MAPIT2_RQ_QUEUE_VIEW

    # Fill out some info on this item.
    item.rqjob_update(
        status="waiting")

    redis_conn = redis.Redis.from_url(
        MAPIT2_REDIS_URL)
    queue = rq.Queue(
        queue_name,
        connection=redis_conn,
        job_timeout=JOB_TIMEOUT)

    args = [
        item.id,
        method_name,
        method_args,
        method_kwargs,
    ]

    # dispatch to the correct function.
    if isinstance(item, (MapView,)):
        rqjob_run_call = rqjob_run_mapview_call
    elif isinstance(item, (MapQgsFile,)):
        rqjob_run_call = rqjob_run_mapqgsfile_call
    else:
        raise ValueError("Uhandled object type.")

    job = queue.enqueue(
        rqjob_run_call,
        args=args,
        job_timeout=JOB_TIMEOUT)

    logger.info("rqjob_enqueue_call: {}".format(args))
    logger.info("rqjob_enqueue_call: job={}".format(job))

    return True


def get_db_session():
    """Creates a session."""
    # @todo: would be nice to share with "app",
    # but that uses the flask library.
    engine = sqlalchemy.create_engine(MAPIT2_DB_URI)
    Session = sqlalchemy.orm.session.sessionmaker(bind=engine)
    session = Session()

    return session

#####


def rqjob_run_mapview_call(*args, **kwargs):
    try:
        return rqjob_run_object_call(
            MapView,
            *args,
            **kwargs)
    except (BaseException,) as e:
        traceback.print_exc()
        return False


def rqjob_run_mapqgsfile_call(*args, **kwargs):
    try:
        return rqjob_run_object_call(
            MapQgsFile,
            *args,
            **kwargs)
    except (BaseException,) as e:
        traceback.print_exc()
        return False

#####


def rqjob_run_object_call(
        item_class,
        item_id,
        method_name,
        method_args,
        method_kwargs):
    """This is the function which added to the queue.
    We add this function as it takes simple args.

    Trying to put an entire sqlalchemy object into queue might be a problem,
    but we can put its id into the queue and get it now.
    """

    method_args = method_args or []
    method_kwargs = method_kwargs or {}

    logger.info("rqjob_run_object_call:")
    logger.info("  item_class={}".format(item_class))
    logger.info("  item_id={}".format(item_id))
    logger.info("  method_name={}".format(method_name))
    logger.info("  method_args={}".format(method_args))
    logger.info("  method_kwargs={}".format(method_kwargs))

    session = get_db_session()

    # find the Mapview object
    q = session.query(item_class)
    q = q.filter(item_class.id == item_id)
    item = q.first()

    if not item:
        raise ValueError("Didnt find the item: item_id={!r}".format(
            item_id))

    method = getattr(item, method_name)

    if not method:
        raise ValueError("Didnt find method: {!r}.{!r} ".format(
            item.__class__.__name__,
            method_name))

    #
    try:
        #
        item.rqjob_update(
            status="running")
        #
        method(
            *method_args,
            **method_kwargs)
        #
        item.rqjob_update(
            status="done",
            message="")

    except (BaseException,) as e:
        # This seems to be discarded when running under RQ.
        traceback.print_exc()
        item.rqjob_update(
            status="error",
            message="error")

    return True
