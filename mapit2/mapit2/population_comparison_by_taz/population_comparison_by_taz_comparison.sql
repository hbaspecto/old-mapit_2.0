select
        coalesce(scenario_ref.zonenumber,scenario_policy.zonenumber) as zonenumber,
        coalesce(scenario_ref.year_run,scenario_policy.year_run) as year_run,
        coalesce(scenario_policy.population,0.0) - coalesce(scenario_ref.population,0.0) as population_diff
from
(

select
       R.zonenumber,
       R.year_run,
       sum(R.population_at_avg_size) as population
from
     manual_views.taz_population R
where
      R.scenario = '{{item.scenario_ref}}' and
      {{item.year_start}} <= R.year_run and
      R.year_run <= {{item.year_end}}
group by
      R.zonenumber,
      R.year_run
order by
      R.zonenumber,
      R.year_run
) scenario_ref

full join

(
select
       P.zonenumber,
       P.year_run,
       sum(P.population_at_avg_size) as population
from
     manual_views.taz_population P
where
      P.scenario = '{{item.scenario_policy}}' and
      {{item.year_start}} <= P.year_run and
      P.year_run <= {{item.year_end}}
group by
      P.zonenumber,
      P.year_run
order by
      P.zonenumber,
      P.year_run

) scenario_policy

using (zonenumber, year_run)

where
    scenario_ref.year_run is not null and
    scenario_policy.year_run is not null

order by
      zonenumber,
      year_run
