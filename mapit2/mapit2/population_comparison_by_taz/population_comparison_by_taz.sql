SELECT
       P.zonenumber,
       P.year_run,
       sum(P.population_at_avg_size) as population
FROM
     manual_views.taz_population P
where
      -- just 'scenario', not 'item.scenario'
      P.scenario = '{{ scenario }}' and
      {{item.year_start}} <= P.year_run and
      P.year_run <= {{item.year_end}}
group by
      P.zonenumber,
      P.year_run
order by
      P.zonenumber,
      P.year_run
