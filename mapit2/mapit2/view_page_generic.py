#
# mapit_2.0/mapit2/mapit2/view_page_generic.py ---
#

import datetime
import pprint
import re
import traceback

from qgs_util.qgsfile import (
    QgsFile,
)

from .common_vars import *
from .models import (
    mapqgsfile,
)
from .util import (
    columns_for_class,
    yyyymmddThhmmss,
)

#####

# @TODO: these should be rows in a table, not in the code.
# For now, we only have two sites, so lets get it working
# And if it works, put into the DB later.

#  The "host" and "port" are used with QgsFile.

# The HBA connection info.
QGS_SITE_HBA = {
    "site_name": "HBA",
    #
    "host": "leduc.office.hbaspecto.com",
    "port": "5432",
    "user": "postgres",
    "password": "postgres",
}

# This port is a port-mapping set up on fw-1.
# The user is the user we made in postgres.
QGS_SITE_COE = {
    "site_name": "CoE",
    #
    "host": "psql.hbaspecto.com",
    "port": "15432",
    "user": "coe_ro",
    "password": "FrozenEggplant",
}

# No changes to the file.
QGS_SITE_RAW = {
    "site_name": "raw",
}

#: A list of sites to customize the QGS files for.
QGS_SITE_INFO_LIST = [
    QGS_SITE_HBA,
    QGS_SITE_COE,
]

if MAPIT2_DEPLOY in ["dev", ]:
    QGS_SITE_INFO_LIST.append(QGS_SITE_RAW)


def qgs_site_info_lookup(site_name):
    """Get the info for this site.
    """
    for item in QGS_SITE_INFO_LIST:
        if site_name == item["site_name"]:
            return item
    return None

# @TODO: guess the site from the ip_addr?
# def qgs_site_info_guess_name(ip_addr):
#     """Guess the site_name based in IP address."""
#     return "raw"

#####


class CreateMapQgsFileForm(MapViewCreatorForm):
    """Form to set args for creating a MapQgsFile.
    """

    file_name = wtforms.StringField(
        "Filename",
        render_kw={
            "size": 80,
        })

    qgs_template = wtforms.SelectField(
        "QGS Template",
        choices=[],
        render_kw={
            "size": 5,
            # Cant set width on html select field...
            # "width": 40,
            # "style": 'width: 20 em;',
        })

    # @todo? best name?
    range_divisions = wtforms.IntegerField(
        "Range Divisions",
        default=11,
        render_kw={
            "size": 40,
        },
        validators=[
            wtforms.validators.InputRequired(),
        ])

    range_max = wtforms.FloatField(
        "Range Max",
        render_kw={
            "size": 40,
        })

    range_min = wtforms.FloatField(
        "Range Min",
        render_kw={
            "size": 40,
        })

    barchart_value_max = wtforms.FloatField(
        "Barchart value max",
        render_kw={
            "size": 40,
        })

    barchart_height = wtforms.FloatField(
        "Barchart height",
        default=20.0,
        render_kw={
            "size": 40,
        })

    barchart_width = wtforms.FloatField(
        "Barchart width",
        default=1.0,
        render_kw={
            "size": 40,
        })

    barchart_year_steps = wtforms.IntegerField(
        "Barchart Year steps",
        default=1,
        render_kw={
            "size": 40,
        })

    round_mode = wtforms.SelectField(
        "Rounding Mode",
        choices=[
            ("log", "log-like",),
            ("1", "1",),
            ("2", "2",),
            ("3", "3",),
            ("4", "4",),
            ("5", "5",),
        ])

    button = wtforms.SubmitField(
        "Create MapQgsFile",
        validators=[
            wtforms.validators.InputRequired(),
        ],
        render_kw={
            'style': 'background-color: #ccffcc;',
        })


@app.route(
    '/view_page_generic/<int:id>',
    methods=['GET', 'POST'])
# select id,mapview_id,file_create_time,file_name,length(file_data) from mapit2_app.mapqgsfile ;
def view_page_generic(id):
    """This is a generic page for viewing a MapView.
    The subclasses can override this.

    Show a list of fields and their values.
    Allow the user to view and download the data.
    """

    message_err = None
    message_ok = None

    #
    mapview = db.session.query(MapView).get_or_404(id)

    # Build a list of columns and their values to display.
    cols = columns_for_class(mapview.__class__)
    keyval_lst = []
    for col in cols:
        key = col.name
        val = getattr(mapview, key, None)
        keyval_lst.append((key, val,))

    #
    form = CreateMapQgsFileForm(flask.request.form)

    # populate the list of templates to pick.
    maplayer_count = mapview.maplayer_count()
    paths = mapqgsfile.build_site_templates_qgs_files().get(maplayer_count, [])
    tmp = []
    for path in paths:
        tmp.append((path, os.path.basename(path),))
    form.qgs_template.choices = tmp

    # Default the selection.
    if form.qgs_template.data is None:
        suggested_qgs = mapview.qgsfile_template()
        for (value, label, selected) in form.qgs_template.iter_choices():
            if suggested_qgs == label:
                form.qgs_template.data = value
                break
        # Didnt find suggested?...
        if form.qgs_template.data is None:
            # ...use the first one.
            try:
                # @TODO check for empty list
                form.qgs_template.data = list(form.qgs_template.iter_choices())[0][0]
            except BaseException:
                pass

    # Default the file_name
    if not form.file_name.data:
        form.file_name.data = "mapit2_{}_{}.qgs".format(
            mapview.data_view_name,
            yyyymmddThhmmss())

    # internal
    def process_form():
        nonlocal message_ok, message_err

        print(f"processing form: {form.button.data}")

        # not clicked.
        if form.button.data == "":
            return False

        #
        if form.range_divisions is not None:
            try:
                tmp = int(form.range_divisions.data)
            except (Exception,) as e:
                message_err = "error: range_divisions is not an int."
                return False

        #
        if form.range_min.data is not None or form.range_max.data is not None:
            try:
                if not (float(form.range_min.data) < float(form.range_max.data)):
                    message_err = "error: range max<min"
                    return
            except (Exception,) as e:
                message_err = "error: range values are not floats."
                return False

        # Submit the MapQgsFile request.
        mapqgsfile = MapQgsFile()
        mapqgsfile.file_name = form.file_name.data
        mapqgsfile.mapview_id = mapview.id
        mapqgsfile.file_create_time = datetime.datetime.now()
        db.session.add(mapqgsfile)
        db.session.commit()
        message_ok = "Job Submitted."

        try:
            if rqjob_enabled():
                mapqgsfile.rqjob_update(
                    status="waiting")
                rqjob_enqueue_call(
                    mapqgsfile,
                    "create_qgsfile_data",
                    method_args=[form.data],
                    # send this work to the QGS queue.
                    queue_name=MAPIT2_RQ_QUEUE_QGS)
                message_ok = "job submitted to rq."
            else:
                mapqgsfile.create_qgsfile_data(form.data)
                db.session.commit()
                message_ok = "Run without rqjob."
        except (Exception,) as e:
            traceback.print_exc()
            message_err = "Error!"

        return True

    ###
    if flask.request.method == 'POST':
        # If it worked, do a redirect to a "GET", so a refresh doesnt repost.
        # This does have the side effect of resetting the form data.
        # (could be fixed with more fuss...)
        if process_form():
            return flask.redirect(
                flask.url_for(
                    'view_page_generic',
                    id=mapview.id))

    # print(f"{message_ok}")
    # print(f"{message_err}")
    return flask.render_template(
        './templates/view_page_generic.html',
        QGS_SITE_INFO_LIST=QGS_SITE_INFO_LIST,
        **locals())

#####


@app.route('/mapqgsfile_download/<int:id>')
def mapqgsfile_download(id, site_name=None):
    """Get the qgsfile data from the DB and send it to the user.

    If ``site_name`` is set, then customize the mapqgsfile with the
    postgres connection info.
    """
    mapqgsfile = db.session.query(MapQgsFile).get_or_404(id)

    if not mapqgsfile.file_data:
        return flask.abort(404, "No Mapqgsfile data to return.")

    #
    qgsfile = QgsFile(xml_data=mapqgsfile.file_data)
    qgsfile_name = mapqgsfile.file_name

    # Apply our per-site DB changes, if set.
    site_name = site_name or flask.request.args.get("site_name")
    if site_name:
        site_info = qgs_site_info_lookup(site_name)
        if not site_info:
            return flask.abort(500, "missing site_name={!r}".format(site_name))
        #
        if False:
            print("### mapqgsfile_download(): {} {} {}".format(
                id, site_name, site_info))
        #
        qgsfile.update_db_info(db_info=site_info)
        # "FOO.qgs" => "FOO_CoE.qgs"
        qgsfile_name = re.sub(
            ".qgs$",
            "_{}.qgs".format(site_name),
            qgsfile_name)

    # return it.
    response = flask.make_response(qgsfile.as_xml_bytes(), 200)
    response.mimetype = "text/plain"
    response.headers['Content-Disposition'] = "attachment; filename={}".format(qgsfile_name)

    return response


@app.route(
    '/mapqgsfile_delete/<int:id>',
    methods=['GET'])
def mapqgsfile_delete(id):
    """Delete the mapqgsfile row.
    """
    mapqgsfile = db.session.query(MapQgsFile).get_or_404(id)

    # where we will return to.
    mapview_id = mapqgsfile.mapview.id

    db.session.delete(mapqgsfile)
    db.session.commit()

    return flask.redirect(flask.url_for('view_page_generic', id=mapview_id))
