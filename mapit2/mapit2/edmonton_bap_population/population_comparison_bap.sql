select
        coalesce(scenario_bap.luz,scenario_pol.luz) as luz,
        scenario_pol.year_run as year_run,
        case when scenario_bap.population is null then null else coalesce(scenario_pol.population,0.0) - scenario_bap.population end as population_diff
from
(

select
       R.zonenumber/100 as luz,
       R.year_run,
       sum(R.population_at_avg_size) as population
from
     manual_views.taz_population R
where
      R.scenario = '{{item.scenario_policy}}' and
      {{item.year_start}} <= R.year_run and
      R.year_run <= {{item.year_end}}
group by
      R.zonenumber/100,
      R.year_run
order by
      R.zonenumber/100,
      R.year_run
) scenario_pol

full join

(
select
		"LUZ" as luz,
       "Pop_BAP" as population
from
	mapit2_edmonton_data.bap_cpp_2016_by_luz
order by
      luz
) scenario_bap

using (luz)

where
    scenario_pol.year_run is not null

order by
      luz,
      year_run
