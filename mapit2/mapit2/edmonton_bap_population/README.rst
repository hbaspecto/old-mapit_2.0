mapit_2.0/mapit2/mapit2/edmonton_bap/README
==================================================



Original
--------------------------------------------------

A) Comparison of population against BAP population, by LUZ,
   for the year closest to 2million population. I think we
   should stick with the three layers:


1) bar graph of the BAP population, which would just be a
   single bar (since it doesn't change over time) (could be
   a bit wider of a bar).  This is only for the Edmonton
   zones.

2) bar graph of the scenario population, which is the
   standard bar graph over time. In this case the bar for
   the year that is chosen could be a slightly different
   colour or shade.  This is for all the zones, not just the
   Edmonton zones.

3) choropleth of differences at the chosen year, with the
   scale chosen based on the differences in Edmonton for the
   years.  This is only for the Edmonton zones.

B) Comparison of the population against the CPP population,
   for the same year.  Same three layers

C) Comparison of employment against the BAP employment, for
   the same year, same three layers

D) Comparison of the employment against the CPP employment,
   for the same year, same three layers.

So, there are twelve layers. In groups of three. Four
layers, each with three groups.

I think for now, let the user specify the comparison year.

Phase 1: Get group A working with user choosing comparison
year (hopefully done for Thursday).

Phase 2: Add group B, C and D.

Phase 3: Give the user the option of selecting the "year at
closest population" instead of choosing the year by its
number in the Common Era.
