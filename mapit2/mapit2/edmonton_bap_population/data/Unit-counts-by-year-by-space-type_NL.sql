--create table manual_views.detached_use_rates as
select t.commodity, t.year_run, p.taz, t.zonenumber, p.quantity/t.use_rate as unit_count from
(select ur.zonenumber,ur.commodity, ur.year_run, ur.use_rate from
(SELECT a.zonenumber, x.commodity, -(sum(a.probability * b.quantity * c.use_rate))/(sum(a.probability * b.quantity)) as use_rate,
                                    
                                    -sum(a.probability * b.quantity * c.use_rate) AS space_ued,
                                    sum(a.probability * b.quantity) AS number_of_hh,
                                    
                                    a.year_run,
                                    a.scenario
                                   FROM output.all_technology_choice_widx a
                                     JOIN output.activity_numbers an ON an.activitynumber = a.activity
                                     JOIN output.all_activity_locations b ON an.activity::text = b.activity::text AND a.scenario::text = b.scenario::text AND a.zonenumber = b.zonenumber AND a.year_run = b.year_run
                                     JOIN output.option_use_rates c ON a.option_id = c.option_id
                                     JOIN output.technology_space_type x ON a.option_id = x.id
                                  WHERE an.activity_type_id = 2 AND a.scenario::text = 'I255a'::text  AND a.year_run in(2071,2075) AND an.activity != 'HH_RCPatients' 
and  b.quantity > 0 and a.probability > 0
                                  GROUP BY a.zonenumber, x.commodity, a.scenario, a.year_run 
                                  order by  a.zonenumber, x.commodity,a.year_run ,-(sum(a.probability * b.quantity * c.use_rate))/(sum(a.probability * b.quantity))) ur join 
                                  luz_v4 l on ur.zonenumber = l.luz::integer) t join

(SELECT year_run, taz, taz/100 zonenumber, aa_commodity, sum(quantity) as quantity
  FROM output.all_floorspacei where scenario = 'I255a' and year_run in (2071,2075) and (floor(taz/10000) = 22 or taz in (112508, 112509, 112510, 112513, 112514, 
		112515, 112516, 112517, 112610, 112611, 112612, 112613, 112614, 112701, 112702, 112703, 
		112704, 112705, 112706, 112707, 112708))
  group by taz, taz/100, aa_commodity,year_run) p on t.zonenumber = p.zonenumber and t.commodity = p.aa_commodity and t.year_run = p.year_run
  
	
