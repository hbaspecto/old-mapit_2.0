--Population by region group
SELECT year_run, sum_pop FROM
(SELECT year_run, floor(sum(population_at_avg_size)) AS sum_pop,												
        CASE												
		WHEN floor(zonenumber/10000) = 22 or zonenumber in (112508, 112509, 112510, 112513, 112514, 
		112515, 112516, 112517, 112610, 112611, 112612, 112613, 112614, 112701, 112702, 112703, 
		112704, 112705, 112706, 112707, 112708) THEN 'Edmonton City'
		WHEN floor(zonenumber/10000) = 11 and zonenumber not in (112508, 112509, 112510, 112513, 112514, 
		112515, 112516, 112517, 112610, 112611, 112612, 112613, 112614, 112701, 112702, 112703, 112704, 
		112705, 112706, 112707, 112708) THEN 'Edmonton Region'
		WHEN floor(zonenumber/10000) in (11,22) THEN 'Edmonton Metro'
		WHEN floor(zonenumber/10000) in (6,21) THEN 'Calgary Metro'
		WHEN floor(zonenumber/10000) = 16 THEN 'Wood Buffalo'
		WHEN floor(zonenumber/10000) = 19 THEN 'Grande Prairie'
		WHEN floor(zonenumber/10000) = 8 THEN 'Red Deer'	
        ELSE 'Rest of Province'	
        END AS region_group	
FROM manual_views.taz_population
	--specify scenario here:												
	where scenario = 'I205a'
	--If you want to query a single year, e.g.:
	--and year_run = 2021
	--If you want to query a set of years, e.g.:
	--and year_run in (2021, 2037)
	--If you want all years, you don't need to specify this
	group by year_run, region_group) tbl
WHERE region_group = 'Edmonton City' AND (year_run % 2 !=0 OR year_run = 2016)	
	--WHERE statement filters the region_group to be 'Edmonton City' only and include only odd years and 2016.
	order by year_run;

--Employment by region group
SELECT year_run, sum_emp FROM
(SELECT year_run, floor(sum(amt)) AS sum_emp, 										
        CASE											
		WHEN floor(taz/10000) = 22 or taz in (112508, 112509, 112510, 112513, 112514, 112515, 112516, 
		112517, 112610, 112611, 112612, 112613, 112614, 112701, 112702, 112703, 112704, 112705, 112706, 
		112707, 112708) THEN 'Edmonton City'									
		WHEN floor(taz/10000) = 11 and taz not in (112508, 112509, 112510, 112513, 112514, 112515, 112516, 
		112517, 112610, 112611, 112612, 112613, 112614, 112701, 112702, 112703, 112704, 112705, 112706, 
		112707, 112708) THEN 'Edmonton Region'									
		WHEN floor(taz/10000) in (11,22) THEN 'Edmonton Metro'									
		WHEN floor(taz/10000) in (6,21) THEN 'Calgary Metro'									
		WHEN floor(taz/10000) = 16 THEN 'Wood Buffalo'									
		WHEN floor(taz/10000) = 19 THEN 'Grande Prairie'									
		WHEN floor(taz/10000) = 8 THEN 'Red Deer'									
        ELSE 'Rest of Province'											
        END AS region_group											
	FROM output.all_employment
	--specify scenario here:										
	where scenario = 'I205a'
	--If you want to query a single year, e.g.:
	--and year_run = 2021
	--If you want to query a set of years, e.g.:
	--and year_run in (2021, 2037)
	--If you want all years, you don't need to specify this
	group by year_run, region_group) TBL
WHERE region_group = 'Edmonton City' AND (year_run % 2 !=0 OR year_run = 2016)
ORDER BY year_run;	
