--
-- mapit_2.0/mapit2/mapit2/edmonton_bap_population/data/merge-Emp_BAP_Adjusted.sql ---
--
-- "mapit2_edmonton_data.emp_bap_adjusted" has the column "Emp_BAP_Adjusted",
-- which needs to be added to "mapit2_edmonton_data.bap_cpp_2016_by_luz"

alter table mapit2_edmonton_data.bap_cpp_2016_by_luz
add column "Emp_BAP_Adjusted" int
;

update mapit2_edmonton_data.bap_cpp_2016_by_luz T
set "Emp_BAP_Adjusted" = (
  select "Emp_BAP_Adjusted"
  from mapit2_edmonton_data.emp_bap_adjusted A
  where T."LUZ" = A."LUZ")
;

-- should be null - all rows should have a value.
select * from mapit2_edmonton_data.bap_cpp_2016_by_luz
where "Emp_BAP_Adjusted" is null;
