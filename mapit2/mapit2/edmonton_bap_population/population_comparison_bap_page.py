from ..common_vars import *
from .population_comparison_bap import (
    PopulationComparisonBAP,
)

#####


class PopulationComparisonBAPForm(MapViewCreatorForm):

    year_start = wtforms.IntegerField(
        "Start Year",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])

    year_end = wtforms.IntegerField(
        "End Year",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])

    year_comparison = wtforms.StringField(
        "Comparison Year (leave blank to use the minimum total difference year)",
    )
    # TODO should have a validator, so that if the user enters a number it is a valid year in a reasonable century.

    scenario_policy = wtforms.StringField(
        "Policy Scenario",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=3,
                max=20,
                message="Scenario name short and at least 3 characters"),
        ])

#####


@app.route(
    '/population_comparison_bap/create',
    methods=['POST', 'GET'])
def population_comparison_bap_create():

    item = PopulationComparisonBAP(
        db_connection_string=MAPITDB_DEFAULT_URI,
        schema_for_views=MAPIT2_VIEWS_SCHEMA,
    )

    form = PopulationComparisonBAPForm()
    if flask.request.method == 'GET':
        item.initializeForm(form)

    message_err = None
    message_ok = None

    # A function to process the form, so we can use "return"
    def process_form():
        nonlocal message_ok, message_err

        if not form.button.data:
            return
        if not form.validate():
            return

        if form.year_comparison.data == '':
            form.year_comparison.data = None
        item.copy_from_form(form)
        db.session.add(item)
        db.session.commit()

        try:
            if rqjob_enabled():
                rqjob_enqueue_call(
                    item,
                    "make_views",
                    method_args=[luz_table, luz_key])
            else:
                item.make_views(luz_table, luz_key)

        except Exception as e:
            print('There was an issue creating your data views, ' + str(e))
            return 'There was an issue creating your data views, ' + str(e)

        return None

    if flask.request.method == 'POST':
        if form.validate():
            print("Posting units view...")
            vals = process_form()
            if vals is None:
                return flask.redirect(flask.url_for('index'))
            else:
                message_err = vals
        else:
            message_err = form.errors

    return flask.render_template('edmonton_bap_population/population_comparison_bap.html', **locals())


@app.route(
    '/population_comparison_bap/view/<int:id>',
    methods=['GET', ])
def population_comparison_bap_view(id):
    item = db.session.query(PopulationComparisonBAP).get_or_404(id)
    response = flask.make_response(item.generate_layer_file(), 200)
    response.mimetype = "text/plain"
    response.headers.set(
        'Content-Disposition', 'attachment', filename='{view}.qlr'.format(view=item.data_view_name))
    return response

#####


define_page(
    func='population_comparison_bap_create',
    category="11: Population",
    title="Population comparison against Edmonton BAP by LUZ")
