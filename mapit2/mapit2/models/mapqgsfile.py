#
# mapit_2.0/mapit2/mapit2/models/mapqgsfile.py ---
#

import datetime
import glob
import os
import pprint

import sqlalchemy
import sqlalchemy.dialects.postgresql
from qgs_util.qgsfile import (
    QgsFile,
)

from ..appbase import (
    db,
)
from ..env_vars import (
    MAPIT2_APP_SCHEMA,
    MAPIT2_SITE_TEMPLATES_DIR,
    MAPIT2_VIEWS_SCHEMA,
)
from ..util import (
    datetime_create_func,
    float_range,
    float_safe,
    lst_re_filter,
    round_pretty,
)
from .mixins import (
    MixinId,
    MixinRqjob,
)
from .sqlalchemy_model_base import (
    Mapit2ModelBase,
)

# Circular import?  We just want to import it so we can check with instanceof
# from ..edmonton_bap_population.population_comparison_bap import (
#    PopulationComparisonBAP,
# )

#####


def build_site_templates_qgs_files():
    """Return a dict of qgsfiles.
    the key is the number of maplayers (3 or 1)

    The returned values are the relative paths.

    ``find_template_path`` turns them back into full paths.
    """

    path_glob = os.path.join(
        MAPIT2_SITE_TEMPLATES_DIR,
        "*.qgs")

    data = {}
    # path is the full path.
    for path in sorted(glob.glob(path_glob)):
        try:
            qgsfile = QgsFile(path)
            maplayer_cnt = qgsfile.maplayer_count()
            #
            if maplayer_cnt not in data:
                data[maplayer_cnt] = []
            # Add the name, the backend might have a different path.
            name = os.path.basename(path)
            data[maplayer_cnt].append(name)
        except (Exception,) as e:
            pass

    if False:
        print("build_site_templates_qgs_files:")
        pprint.pprint(data)

    return data

#####


class MapQgsFile(Mapit2ModelBase, MixinId, MixinRqjob):
    """A QgsFile for a mapview.

    Like a mapview, the creation of a QgsFile could take
    some time, so we also use Rqjob for a queue.

    Also, this method:

    - keeps a history of the qgsfile generated.
    - keeps the bytea blob out of the mapview table.

    """

    __abstract__ = False

    __tablename__ = 'mapqgsfile'

    #: primary key
    mapview_id = sqlalchemy.Column(
        sqlalchemy.Integer(),
        sqlalchemy.ForeignKey(
            f'{MAPIT2_APP_SCHEMA}.mapview.id',
            ondelete="CASCADE"),
        index=True)

    # Dont duplicate the relationship; MapView defines it.
    # mapview = sqlalchemy.orm.relationship(
    #    "mapit2.models.mapview_superclass.MapView")

    #: when this qgsfile was created.
    file_create_time = sqlalchemy.Column(
        sqlalchemy.DateTime(),
        default=datetime_create_func,
        nullable=False)

    #: Suggested name of file.
    file_name = sqlalchemy.Column(
        sqlalchemy.String(100),
    )

    #: The byte data for this file.
    file_data = sqlalchemy.Column(
        sqlalchemy.dialects.postgresql.BYTEA(),
    )

    #: Form data
    # @TODO: save the form data, so we can recreate the QGS file.

    ###

    def find_template_path(self, item, backup_1=None, backup_2=None):
        """Turn the item name into a full path.
        Use backup items if the others are None.
        """
        item = item or backup_1 or backup_2

        if item is None:
            raise ValueError("find_template_path: None")

        # Prefer files in the templates dir.
        path = os.path.join(
            MAPIT2_SITE_TEMPLATES_DIR,
            item)
        if os.path.isfile(path):
            return path

        # maybe it is an abspath?
        if os.path.isfile(item):
            return item

        #
        raise ValueError("find_template_path: Didnt find: {!r}".format(
            item))

    def create_qgsfile_data(self, form_data):
        """Called to create the qgsfile data.
        This is supposed to be generic.

        ``form_data`` is a dict of values from the form.
        and we can find the mapview we are attached to.
        """

        #
        mapview = self.mapview

        # Couldn't use isinstance because of a circular dependency that we were too lazy to fix
        if mapview.__class__.__name__ == 'PopulationComparisonBAP':
            # This is for comparing a scenario against some fixed value
            return self.create_qgsfile_data_fixed_comparison(form_data, ref_column_name='population')
        elif mapview.__class__.__name__ == 'EmploymentComparisonBAP':
            return self.create_qgsfile_data_fixed_comparison(form_data, ref_column_name='employment')
        elif hasattr(mapview, "geom_view_name_ref"):
            # Generally for comparing two scenarios, the three layers are each scenario, then the comparison
            return self.create_qgsfile_data_3(form_data)
        else:
            # Sometimes we're just looking at one layer, e.g. the results from one scenario
            return self.create_qgsfile_data_1(form_data)

    def create_qgsfile_data_1(self, form_data):
        """Placeholder for now.
        """

        mapview = self.mapview

        qgsfile_path = self.find_template_path(
            form_data.get("qgs_template"),
            mapview.qgsfile_template(),
            "ref-policy-diff.qgs")
        qgsfile = QgsFile(qgsfile_path)

        # Squish our data into the file!
        qgsfile.update_metadata()
        qgsfile.update_db_info()

        # ignore the other layers.
        qgsfile.update_layer(
            1,
            "{}.{}".format(
                mapview.schema_for_views,
                mapview.geom_view_name))

        self.file_data = qgsfile.as_xml_bytes()

        self.rqjob_update(
            status="done",
            message="create_qgsfile_data_1")

        return True

    ###

    def tidy_column_names(self, column_names):
        """Clean up the list of column names
        by removing things we dont want to plot."""
        rv = lst_re_filter(
            column_names,
            remove_regexes=[
                "^geom",
                "^zonenumber",
                "luz",
                "taz"])
        print("tidy_column_names: {}".format(rv))
        return rv

    def table_value_abs_max(
            self,
            mapview,
            table_name,
            column_names):
        """Return the max value found in the columns of this table."""

        sql = "select max(greatest(" +\
            ",".join(['abs("{}")'.format(name) for name in column_names]) +\
            ")) " +\
            """from "{}"."{}"
            """.format(
                mapview.schema_for_views,
                table_name)

        print("table_value_abs_max={}".format(sql))

        with mapview.get_data_conn() as data_conn:
            cursor = data_conn.execute(sql)
            row = cursor.first()
            value_max = row[0]

        return value_max

    def create_qgsfile_data_3(self, form_data):
        """Called to create the qgsfile data.  This is supposed to
        be generic.  It should be pushed into the MapView
        class.  As that would give the MapView subclasses a
        chance to provide their own method.

        ``form_data`` is a dict of values from the form.
        and we can find the mapview we are attached to.

        """

        print("==================== form_data")
        pprint.pprint(form_data)

        self.rqjob_update(
            status="running",
            message="Started")

        #
        mapview = self.mapview

        #
        qgsfile_path = self.find_template_path(
            form_data.get("qgs_template"),
            mapview.qgsfile_template())
        qgsfile = QgsFile(qgsfile_path)

        #
        range_min = float_safe(form_data.get("range_min"))
        range_max = float_safe(form_data.get("range_max"))
        range_divisions = int(form_data.get("range_divisions", 11))
        year_steps = int(form_data.get("year_steps", 1))

        #
        barchart_height = float(form_data.get("barchart_height", 20.0))
        barchart_width = float(form_data.get("barchart_width", 1.0))
        barchart_value_max = form_data.get("barchart_value_max")
        round_mode = form_data.get("round_mode", "log")

        barchart_ref_table_name = mapview.crosstab_materialized_view_name_ref
        barchart_ref_column_names = mapview.list_column_names(
            mapview.schema_for_views,
            barchart_ref_table_name)
        barchart_ref_column_names = self.tidy_column_names(barchart_ref_column_names)

        barchart_policy_table_name = mapview.crosstab_materialized_view_name_policy
        barchart_policy_column_names = mapview.list_column_names(
            mapview.schema_for_views,
            barchart_policy_table_name)
        barchart_policy_column_names = self.tidy_column_names(barchart_policy_column_names)

        #
        if barchart_value_max is None:
            barchart_ref_value_max = self.table_value_abs_max(
                mapview,
                barchart_ref_table_name,
                barchart_ref_column_names)

            barchart_policy_value_max = self.table_value_abs_max(
                mapview,
                barchart_policy_table_name,
                barchart_policy_column_names)

            barchart_value_max = max(
                (barchart_ref_value_max or 0.0),
                (barchart_policy_value_max or 0.0))
            barchart_value_max = round_pretty(
                barchart_value_max,
                mode=round_mode)

        print("barchart_value_max={}".format(
            barchart_value_max))

        # Squish our data into the file!

        # metadata
        qgsfile.update_metadata()
        qgsfile.update_db_info(db_info=mapview.db_connection_string)

        # @TODO: So how does QGIS line up the plotted barcharts?
        #        Should be be sticking in some blank columns,
        #        Or will QGIS match them up by name.

        ### barchart: ref
        qgsfile.update_layer(
            1,
            "{}.{}".format(
                mapview.schema_for_views,
                mapview.geom_view_name_ref))

        qgsfile.maplayer_update_histogram(
            1,
            bar_height=barchart_height,
            bar_value_max=barchart_value_max,
            bar_width=barchart_width,
            column_names=barchart_ref_column_names)

        ### barchart: policy
        qgsfile.update_layer(
            2,
            "{}.{}".format(
                mapview.schema_for_views,
                mapview.geom_view_name_policy))

        qgsfile.maplayer_update_histogram(
            2,
            bar_height=barchart_height,
            bar_value_max=barchart_value_max,
            bar_width=barchart_width,
            column_names=barchart_policy_column_names)

        ### colors: diff
        qgsfile.update_layer(
            3,
            "{}.{}".format(
                mapview.schema_for_views,
                mapview.geom_view_name))

        #
        minmax_table_name = mapview.crosstab_materialized_view_name
        minmax_column_names = mapview.list_column_names(
            mapview.schema_for_views,
            minmax_table_name)

        # remove unwanted colums
        minmax_column_names = lst_re_filter(
            minmax_column_names,
            remove_regexes=[
                "^geom",
                "^zonenumber",
                "luz",
                "taz"])

        minmax_column_name = minmax_column_names[-1]
        print(f"minmax_column_name={minmax_column_name}")

        # compute the range minmax if not given.
        if range_min is None or range_max is None:
            print("query for range_min range_max")
            sql = """
            select min({}),max({})
            from "{}"."{}"
            """.format(
                minmax_column_name,
                minmax_column_name,
                mapview.schema_for_views,
                minmax_table_name)

            # Make a connection to the DATA db...
            with mapview.get_data_conn() as data_conn:
                cursor = data_conn.execute(sql)
                row = cursor.first()
                (range_min, range_max) = row

            print("create_qgsfile_data_3: {} to {}".format(
                range_min, range_max))

            # We want the ranges to be symmetric.
            range_abs = max(abs(range_min or 0.0), abs(range_max or 0.0))
            # and we want them to be pretty.
            range_pretty = round_pretty(
                range_abs,
                mode=round_mode)
            # reset the ranges.
            range_min = -range_pretty
            range_max = +range_pretty

        #
        print(f"Using range: min/max = {range_min} {range_max}")

        if not (range_min <= range_max):
            raise ValueError("ranges flipped!")

        #
        range_lst = float_range(
            range_min,
            range_max,
            num_steps=range_divisions)

        # apply round_pretty mode=3 to middle values.
        # "mode=log" can change "range_lst[1]" to be rounded to "range_lst[0]"
        # dont change the start and end, the user might have set them.
        for idx in range(1, len(range_lst) - 1):
            range_lst[idx] = round_pretty(
                range_lst[idx],
                mode=3)

        print(range_lst)

        qgsfile.maplayer_set_ranges(
            3,
            range_lst,
            minmax_column_name)

        #
        self.file_data = qgsfile.as_xml_bytes()

        self.rqjob_update(
            status="done",
            message="Done")

        return True

    def create_qgsfile_data_fixed_comparison(self, form_data, ref_column_name):
        """Called to create the qgsfile data.  This is supposed to
        be generic.  It should be pushed into the MapView
        class.  As that would give the MapView subclasses a
        chance to provide their own method.

        ``form_data`` is a dict of values from the form.
        and we can find the mapview we are attached to.

        """

        print("==================== form_data")
        pprint.pprint(form_data)

        self.rqjob_update(
            status="running",
            message="Started")

        #
        mapview = self.mapview

        #
        qgsfile_path = self.find_template_path(
            form_data.get("qgs_template"),
            mapview.qgsfile_template())
        qgsfile = QgsFile(qgsfile_path)

        #
        range_min = float_safe(form_data.get("range_min"))
        range_max = float_safe(form_data.get("range_max"))
        range_divisions = int(form_data.get("range_divisions", 11))
        year_steps = int(form_data.get("year_steps", 1))

        #
        barchart_height = float_safe(form_data.get("barchart_height", 20.0))
        barchart_width = float_safe(form_data.get("barchart_width", 1.0))
        barchart_value_max = float_safe(form_data.get("barchart_value_max", 0.0))
        round_mode = form_data.get("round_mode", "log")

        # Not a crosstabbed table
        barchart_ref_table_name = mapview.data_view_name_ref

        # barchart_ref_column_names = mapview.list_column_names(
        #    mapview.schema_for_views,
        #    barchart_ref_table_name)
        #barchart_ref_column_names = self.tidy_column_names(barchart_ref_column_names)

        barchart_ref_column_names = [ref_column_name]

        barchart_policy_table_name = mapview.crosstab_materialized_view_name_policy
        barchart_policy_column_names = mapview.list_column_names(
            mapview.schema_for_views,
            barchart_policy_table_name)
        barchart_policy_column_names = self.tidy_column_names(barchart_policy_column_names)

        #
        if barchart_value_max is None:
            barchart_ref_value_max = self.table_value_abs_max(
                mapview,
                barchart_ref_table_name,
                barchart_ref_column_names)

            barchart_policy_value_max = self.table_value_abs_max(
                mapview,
                barchart_policy_table_name,
                barchart_policy_column_names)

            barchart_value_max = max(
                (barchart_ref_value_max or 0.0),
                (barchart_policy_value_max or 0.0))
            barchart_value_max = round_pretty(
                barchart_value_max,
                mode=round_mode)

        print("barchart_value_max={}".format(
            barchart_value_max))

        # Squish our data into the file!

        # metadata
        qgsfile.update_metadata()
        qgsfile.update_db_info(db_info=mapview.db_connection_string)

        # @TODO: So how does QGIS line up the plotted barcharts?
        #        Should be be sticking in some blank columns,
        #        Or will QGIS match them up by name.

        ### barchart: ref
        qgsfile.update_layer(
            1,
            "{}.{}".format(
                mapview.schema_for_views,
                mapview.geom_view_name_ref))

        qgsfile.maplayer_update_histogram(
            1,
            bar_height=barchart_height,
            bar_value_max=barchart_value_max,
            bar_width=barchart_width * 2,
            column_names=barchart_ref_column_names)

        ### barchart: policy
        qgsfile.update_layer(
            2,
            "{}.{}".format(
                mapview.schema_for_views,
                mapview.geom_view_name_policy))

        qgsfile.maplayer_update_histogram(
            2,
            bar_height=barchart_height,
            bar_value_max=barchart_value_max,
            bar_width=barchart_width,
            column_names=barchart_policy_column_names)

        ### colors: diff
        qgsfile.update_layer(
            3,
            "{}.{}".format(
                mapview.schema_for_views,
                mapview.geom_view_name))

        #
        minmax_table_name = mapview.crosstab_materialized_view_name
        minmax_column_names = mapview.list_column_names(
            mapview.schema_for_views,
            minmax_table_name)

        # remove unwanted colums
        minmax_column_names = lst_re_filter(
            minmax_column_names,
            remove_regexes=[
                "^geom",
                "^zonenumber",
                "luz",
                "taz"])

        # We assumed the last year is the comparison year
        # minmax_column_name = minmax_column_names[-1]
        # But we've figured out a different comparison year for this one
        minmax_column_name = 'total_{:04}'.format(mapview.year_comparison)

        print(f"minmax_column_name={minmax_column_name}")

        # compute the range minmax if not given.
        if range_min is None or range_max is None:
            print("query for range_min range_max")
            sql = """
            select min({}),max({})
            from "{}"."{}"
            """.format(
                minmax_column_name,
                minmax_column_name,
                mapview.schema_for_views,
                minmax_table_name)

            # Make a connection to the DATA db...
            with mapview.get_data_conn() as data_conn:
                cursor = data_conn.execute(sql)
                row = cursor.first()
                (range_min, range_max) = row

            print("create_qgsfile_data_3: {} to {}".format(
                range_min, range_max))

            # We want the ranges to be symmetric.
            range_abs = max(abs(range_min or 0.0), abs(range_max or 0.0))
            # and we want them to be pretty.
            range_pretty = round_pretty(
                range_abs,
                mode=round_mode)
            # reset the ranges.
            range_min = -range_pretty
            range_max = +range_pretty

        #
        print(f"Using range: min/max = {range_min} {range_max}")

        if not (range_min <= range_max):
            raise ValueError("ranges flipped!")

        #
        range_lst = float_range(
            range_min,
            range_max,
            num_steps=range_divisions)

        # apply round_pretty mode=3 to middle values.
        # "mode=log" can change "range_lst[1]" to be rounded to "range_lst[0]"
        # dont change the start and end, the user might have set them.
        for idx in range(1, len(range_lst) - 1):
            range_lst[idx] = round_pretty(
                range_lst[idx],
                mode=3)

        print(range_lst)

        qgsfile.maplayer_set_ranges(
            3,
            range_lst,
            minmax_column_name)

        # blue #4abcd0
        # yellow #ddf100

        # We also want the comparison year to be a different color in the policy layer
        selected_column_name = "total_{:04}".format(mapview.year_comparison)
        qgsfile.update_bar_colors(
            2,
            new_colors={
                selected_column_name: '#ddf100',
            }
        )

        self.file_data = qgsfile.as_xml_bytes()

        self.rqjob_update(
            status="done",
            message="Done")

        return True
