#
# mapit_2.0/mapit2/mapit2/models/mixins.py ---
#

import sqlalchemy

from ..env_vars import (
    MAPIT2_APP_SCHEMA,
)
from .sqlalchemy_model_base import (
    Mapit2ModelBase,
)

#####


class MixinId(object):
    """Numeric primary key.
    """

    __abstract__ = True

    __mapper_args__ = {
        # not needed/allowed after sqlalchemy==1.4.0
        # 'order_by': "id",
    }

    @sqlalchemy.ext.declarative.declared_attr
    #: generate a numeric id column for this row.
    def id(cls):
        # add a sequence into the meta.
        #
        # well darn - alembic doesnt support autogeneration of sequences.
        # it is listed in the todos on this page.
        # https://alembic.sqlalchemy.org/en/latest/autogenerate.html
        #
        # This works when creating directly, just not with alembic.
        # Add the sequence creation by hand to the script.
        id_seq = sqlalchemy.Sequence(
            "{}_id_seq".format(cls.__tablename__),
            start=1,
            increment=1,
            schema=f"{MAPIT2_APP_SCHEMA}",
            metadata=cls.metadata)

        col = sqlalchemy.Column(
            'id',
            sqlalchemy.Integer(),
            server_default=id_seq.next_value(),
            primary_key=True)

        # scoot this column to the leftmost.
        col._creation_order = 0
        return col

#####


class MixinRqjob(object):

    #: The status of the job.
    # The
    # values are:
    # - null
    # - "waiting"
    # - "running"
    # - "done" or "error"
    rqjob_status = sqlalchemy.Column(
        sqlalchemy.String(20))

    #: Messages as the job is processed.
    rqjob_message = sqlalchemy.Column(
        sqlalchemy.String(100))

    # scoot to the right.
    rqjob_status._creation_order = 201
    rqjob_message._creation_order = 202

    def rqjob_update(
            self,
            status=None,
            message=None):
        """Update the rqjob fields and commit.
        This is called as the job progresses, so we can see the job progress.

        We call this even if not using RQ, as this only updates
        the fields in the DB, it does not interact with the RQ system.
        """

        if False:
            print("rqjob_update: {} {} {}".format(
                self.id,
                status,
                message))

        if message is not None:
            if message == "":
                message = None
            self.rqjob_message = message
        if status:
            self.rqjob_status = status

        #
        this_session = sqlalchemy.orm.session.Session.object_session(self)
        this_session.commit()

        #
        return True
