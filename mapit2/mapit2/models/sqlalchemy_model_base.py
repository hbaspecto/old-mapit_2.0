
import sqlalchemy
import sqlalchemy.ext.declarative

from ..env_vars import *

#####

# John would like his app in its own schema, not "public."
# https://gist.github.com/h4/fc9b6d350544ff66491308b535762fee


@sqlalchemy.ext.declarative.as_declarative()
class Mapit2ModelBase(object):

    __abstract__ = True

    @sqlalchemy.ext.declarative.declared_attr
    def __table_args__(cls):
        return {
            "schema": f"{MAPIT2_APP_SCHEMA}",
        }
