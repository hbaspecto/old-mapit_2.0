#!/usr/bin/env python3

# See tutorial https://youtu.be/Z1RJmh_OqeA

import os
import pprint
import re
import uuid
from datetime import (
    datetime,
    timedelta,
)

import flask_table
import flask_wtf
import jinja2
import sqlalchemy as sq
import wtforms
from flask import (
    Flask,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_sqlalchemy import (
    SQLAlchemy,
)

from .env_vars import *
# When the other views import "MapView",
# they are added to MapView.metadata and dont need to be imported here.
from .models.mapview_superclass import (
    MapView,
)

#####

#: Turn on debugging.
DEBUG = True

#####

sqlenv = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(MAPIT2_DIR, "mapit2", "mapit2")),
    # we dont want to have any autoescaping.
    #autoescape=jinja2.select_autoescape(['sql', ])
    autoescape=False,
)

qlrenv = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(MAPIT2_DIR, "mapit2", "mapit2")),
    # we dont want to have any autoescaping.
    # autoescape=jinja2.select_autoescape(['qlr', ])
    autoescape=False,
)

#####

#: A list of pages to display.
#: [ {"func": function,
#      "title": "page title",}, ...]
#: imported pages should add themselves here.
DEFINE_PAGE_LST = []

#: A struct built with categories, then the pages.
DEFINED_PAGES_BY_CATEGORY = None


def define_page(func, title, category=None):
    """Call this to define (and thus add) a page to the list of mapit views.

    The categories start with 'NN:' so we can order them.
    The 'NN:' is stripped off before it is displayed.
    """
    global DEFINE_PAGE_LST
    DEFINE_PAGE_LST.append({
        "func": func,
        "category": category,
        "title": title,
    })


def define_page_by_category_build():
    """Sort the defined pages by their category and name.
    Build the data structure we will show.
    """
    global DEFINE_PAGE_LST

    # dont repeat if done.
    global DEFINED_PAGES_BY_CATEGORY
    if DEFINED_PAGES_BY_CATEGORY is not None:
        return True

    # (should only be printed once)
    # print("### define_page_struct: building the struct.")

    item_lst = sorted(DEFINE_PAGE_LST,
                      key=lambda x: (x.get("category"), x.get("title")))

    DEFINED_PAGES_BY_CATEGORY = []

    prior_cat_name = None
    for item in item_lst:
        item_cat_name = item.get("category", "Unategorized")
        if item_cat_name != prior_cat_name:
            prior_cat_name = item_cat_name
            cat_lst = []
            DEFINED_PAGES_BY_CATEGORY.append([item_cat_name, cat_lst])
        cat_lst.append(item)

    # Remove the 'NN:' at the start.
    for idx in range(len(DEFINED_PAGES_BY_CATEGORY)):
        cat_name = DEFINED_PAGES_BY_CATEGORY[idx][0]
        cat_name = re.sub(r"\d+: +", "", cat_name)
        DEFINED_PAGES_BY_CATEGORY[idx][0] = cat_name

    # debug our struct.
    # pprint.pprint(DEFINED_PAGES_BY_CATEGORY)
    return True

#####


app = Flask(__name__, template_folder=os.path.join(MAPIT2_DIR, "mapit2", "mapit2"))

app.config['SECRET_KEY'] = uuid.uuid4().hex

# No CSRF if debugging
if DEBUG:
    app.config['SECRET_KEY'] = "aaaaaaaaaa"

if not MAPIT2_DB_URI:
    raise ValueError("Please source the environment, so we know what $MAPIT2_DB_URI is")

# TODO: Parameterize the database name and schema
dbschema = f'{MAPIT2_APP_SCHEMA},public'
app.config['SQLALCHEMY_DATABASE_URI'] = MAPIT2_DB_URI
app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {
    "connect_args": {
        'options': f'-csearch_path={dbschema}',
    },
}
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {'options': '-csearch_path={}'.format(dbschema)}

db = SQLAlchemy(app)


#####

@app.route('/new_view_selector', methods=['POST', 'GET'])
def new_view_selector():
    global DEFINED_PAGES_BY_CATEGORY
    define_page_by_category_build()
    # @TODO get the view types from the database table?
    return render_template(
        'templates/new_view_selector.html',
        DEFINED_PAGES_BY_CATEGORY=DEFINED_PAGES_BY_CATEGORY)


@app.route('/delete/<int:id>')
def delete_mapview(id):
    mapview = db.session.query(MapView).get_or_404(id)

    try:
        mapview.drop_views()
    except (Exception,) as e:
        # TODO give the user some options
        return "There was a problem deleteing the associated views in the database." + \
            "Error:" + str(e)
    try:
        db.session.delete(mapview)
        db.session.commit()
        return redirect('/')
    except (BaseException,) as e:
        # TODO give the user some options
        return "There was a problem deleting that view from the app."
