import traceback

from ..common_vars import *
from .employment_comparison_by_taz import (
    EmploymentComparisonByTaz,
)

#####


class EmploymentComparisonByTazForm(MapViewCreatorForm):

    scenario_policy = wtforms.StringField(
        "Scenario Policy",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=3,
                max=20,
                message="Scenario name short and at least 3 characters"),
        ])

    scenario_ref = wtforms.StringField(
        "Scenario Ref",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=3,
                max=20,
                message="Scenario name short and at least 3 characters"),
        ])

    year_start = wtforms.IntegerField(
        "Year Start",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])

    year_end = wtforms.IntegerField(
        "Year End",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])

    naics_list = wtforms.StringField(
        "List of numeric NAICS, separated by commas.")

#####


@app.route('/employment_comparison_by_taz/create', methods=['POST', 'GET'])
def employment_comparison_by_taz_create():
    # TODO CSRF protection
    # TODO this default URI shouldn't be here, but at least it's from the environment
    item = EmploymentComparisonByTaz(
        db_connection_string=MAPITDB_DEFAULT_URI,
        # TODO put this in environment
        schema_for_views=MAPIT2_VIEWS_SCHEMA,
    )

    form = EmploymentComparisonByTazForm()
    if flask.request.method == 'GET':
        item.initializeForm(form)

    message_err = None
    message_ok = None

    # A function to process the form, so we can use "return"
    def process_form():
        nonlocal message_ok, message_err

        # dont do anything if button not clicked, e.g. if POST is from a API call.
        if not form.button.data:
            return
        if not form.validate():
            return

        item.copy_from_form(form)
        db.session.add(item)
        db.session.commit()

        try:
            if rqjob_enabled():
                rqjob_enqueue_call(
                    item,
                    "make_views",
                    method_args=[taz_table, taz_key])
            else:
                item.make_views(taz_table, taz_key)

        except Exception as e:
            traceback.print_exc()
            print('There was an issue creating your data views, ' + str(e))
            return 'There was an issue creating your data views, ' + str(e)

        return None

    if flask.request.method == 'POST':
        if form.validate():
            print("Posting view...")
            # TODO test for content type JSON because we want this to be an API as well.
            # Store it
            vals = process_form()
            if vals is None:
                return flask.redirect(flask.url_for('index'))
            else:
                message_err = vals
        else:
            message_err = form.errors

    return flask.render_template(
        './employment_comparison_by_taz/employment_comparison_by_taz.html',
        **locals())

#####


define_page(
    func='employment_comparison_by_taz_create',
    category="12: Employment",
    title="Employment comparison for two scenarios by TAZ (new!)")
