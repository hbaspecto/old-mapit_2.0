select
  taz,
  year_run,
  sum(amt) as employment
from
  output.all_employment
where
  scenario = '{{scenario}}' and
  {{item.year_start}} <= year_run and
  year_run <= {{item.year_end}}
  {% if item.naics_list %}
  and
  naics in ({{item.naics_list}})
  {% endif %}
group by
      taz,
      year_run
order by
      taz,
      year_run
