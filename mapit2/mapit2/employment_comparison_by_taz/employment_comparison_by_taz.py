#
# mapit_2.0/mapit2/mapit2/employment_comparison_by_taz/employment_comparison_by_taz.py ---
#
# Test values:
# Scenario: I159test
# years: 2012 2014
# naics_list: 11, 21, 23

from ..common_vars import *

#####


class EmploymentComparisonByTaz(MapView):

    __tablename__ = 'employment_comparison_by_taz'

    id = sq.Column(
        sq.Integer,
        sq.ForeignKey(
            f'{MAPIT2_APP_SCHEMA}.mapview.id',
            ondelete="CASCADE"),
        primary_key=True)

    #
    scenario_ref = sq.Column(sq.String(20))
    scenario_policy = sq.Column(sq.String(20))

    year_start = sq.Column(
        sq.Integer())
    year_end = sq.Column(
        sq.Integer())

    naics_list = sq.Column(
        sq.String(500))

    #: Extra crap
    data_view_name_ref = sq.Column(
        sq.String(200),
        default=None,
        nullable=True)
    #:
    crosstab_materialized_view_name_ref = sq.Column(
        sq.String(200),
        default=None,
        nullable=True)
    #:
    geom_view_name_ref = sq.Column(
        sq.String(200),
        default=None,
        nullable=True)
    #:
    data_view_name_policy = sq.Column(
        sq.String(200),
        default=None,
        nullable=True)
    #:
    crosstab_materialized_view_name_policy = sq.Column(
        sq.String(200),
        default=None,
        nullable=True)
    #:
    geom_view_name_policy = sq.Column(
        sq.String(200),
        default=None,
        nullable=True)

    __mapper_args__ = {
        'polymorphic_identity': 'employment_comparison_by_taz',
    }

    def view_name_policy(self, timestamp):
        # OOO How do you want the view named, after it is created
        return '{scenario_policy}_policy_employment_{timestamp}'.format(
            scenario_policy=self.scenario_policy,
            timestamp=timestamp)

    def view_name_ref(self, timestamp):
        # OOO How do you want the view named, after it is created
        return '{scenario_ref}_ref_employment_{timestamp}'.format(
            scenario_ref=self.scenario_ref,
            timestamp=timestamp)

    def view_name_diff(self, timestamp):
        # OOO How do you want the view named, after it is created
        return '{scenario_policy}_{scenario_ref}_diff_employment_{timestamp}'.format(
            scenario_policy=self.scenario_policy,
            scenario_ref=self.scenario_ref,
            timestamp=timestamp)

    @property
    def description(self):
        # OOO describe the view
        return "employment comparison by taz between {scenario_policy} and {scenario_ref} naics_list={naics_list}".format(
            scenario_policy=self.scenario_policy,
            scenario_ref=self.scenario_ref,
            naics_list=self.naics_list)

    def make_views(self, luz_table, luz_key):
        self.gen_timestamp()
        timestamp = self.cur_timestamp()

        view_base_name_ref = self.view_name_ref(timestamp)
        print("view name ref ", view_base_name_ref)
        self.make_views_ref(view_base_name_ref, luz_table, luz_key)

        view_base_name_policy = self.view_name_policy(timestamp)
        print("view name policy ", view_base_name_policy)
        self.make_views_policy(view_base_name_policy, luz_table, luz_key)

        view_base_name_diff = self.view_name_diff(timestamp)
        self.make_views_diff(view_base_name_diff, luz_table, luz_key)

        self.data_view_name = view_base_name_diff
        self.crosstab_materialized_view_name = f"{view_base_name_diff}_xtab"
        self.geom_view_name = f"{view_base_name_diff}_xtab_geom"

        self.data_view_name_ref = view_base_name_ref
        self.crosstab_materialized_view_name_ref = f"{view_base_name_ref}_xtab"
        self.geom_view_name_ref = f"{view_base_name_ref}_xtab_geom"

        self.data_view_name_policy = view_base_name_policy
        self.crosstab_materialized_view_name_policy = f"{view_base_name_policy}_xtab"
        self.geom_view_name_policy = f"{view_base_name_policy}_xtab_geom"

        self.description_field = self.description

    def make_views_diff(self, view_base_name, geom_table, geom_key):
        template = sqlenv.get_template(
            'employment_comparison_by_taz/employment_comparison_by_taz_comparison.sql')
        sql = template.render(
            item=self)
        self.make_views_mine(view_base_name, geom_table, geom_key, sql)

    def make_views_ref(self, view_base_name, geom_table, geom_key):
        template = sqlenv.get_template(
            'employment_comparison_by_taz/employment_comparison_by_taz.sql')
        sql = template.render(
            item=self,
            scenario=self.scenario_ref)
        self.make_views_mine(view_base_name, geom_table, geom_key, sql)

    def make_views_policy(self, view_base_name, geom_table, geom_key):
        template = sqlenv.get_template(
            'employment_comparison_by_taz/employment_comparison_by_taz.sql')
        sql = template.render(
            item=self,
            scenario=self.scenario_policy)
        self.make_views_mine(view_base_name, geom_table, geom_key, sql)

    def make_views_mine(self, view_base_name, geom_table, geom_key, sql):
        """This actaully makes the 2 standard views and one standard materialized view.
        """
        self.rqjob_update(message=view_base_name)

        #
        view_base = view_base_name
        engine = sq.create_engine(self.db_connection_string)
        with engine.connect() as connection:
            with connection.begin():
                sql_create = """
                CREATE VIEW "{schema}"."{view_base}" as
                {sql};
                """.format(
                    schema=self.schema_for_views,
                    view_base=view_base,
                    sql=sql)
                print("### employment_comparison_by_taz: SQL", sql_create)
                # TODO check to make sure there are only three columns?
                # TODO check to make sure there are at least one row
                connection.execute(sql_create)

            with connection.begin():
                (prefix, column_name, suffix) = self.get_view_metacolumns()
                sql_crosstab = """
                select output.generate_crosstab_from_3_col_query(
                '{schema}."{view_base}"',
                '{view_base}_xtab',
                '{column_name}',
                0,
                '{schema}',
                '{prefix}',
                '{suffix}',
                '{geom_table}',
                '{geom_key}');""".format(
                    schema=self.schema_for_views,
                    view_base=view_base,
                    geom_table=geom_table,
                    geom_key=geom_key,
                    prefix=prefix,
                    suffix=suffix,
                    column_name=column_name)
                print("### employment_comparison_by_taz: SQL:", sql_crosstab)
                connection.execute(sql_crosstab)

    def get_view_metacolumns(self):
        '''This is for a subclass to define the name of it's columns
        and how it wants to prefix and suffix them in the crosstab.
        '''
        # prefix, column_name, suffix
        return ('total_', 'year_run', '',)

    def drop_views(self):
        return self.drop_views_3()
