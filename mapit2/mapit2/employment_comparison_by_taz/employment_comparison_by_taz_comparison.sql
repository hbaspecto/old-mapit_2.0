select
        scenario_ref.taz,
        scenario_ref.year_run,
        scenario_policy.employment - scenario_ref.employment as employment_diff
from
(

select
  taz,
  year_run,
  sum(amt) as employment
from
  output.all_employment R
where
  scenario = '{{item.scenario_ref}}' and
  {{item.year_start}} <= R.year_run and
  R.year_run <= {{item.year_end}}
  {% if item.naics_list %}
  and
  naics in ({{item.naics_list}})
  {% endif %}
group by
      taz,
      year_run
order by
      taz,
      year_run
) scenario_ref

full join

(
select
  taz,
  year_run,
  sum(amt) as employment
from
  output.all_employment P
where
  scenario = '{{item.scenario_policy}}' and
  {{item.year_start}} <= P.year_run and
  P.year_run <= {{item.year_end}}
  {% if item.naics_list %}
  and
  naics in ({{item.naics_list}})
  {% endif %}
group by
      taz,
      year_run
order by
      taz,
      year_run

) scenario_policy

using (taz,year_run)

where
        scenario_ref.year_run is not null and
        scenario_policy.year_run is not null

order by
      taz,
      year_run
