create index deh_parcel on q241i.development_events_history  (new_pecas_parcel_num);

drop view if exists q241i.development_events_history_geom;
create view q241i.development_events_history_geom
as SELECT year_run, event_type, deh.parcel_id, original_pecas_parcel_num, new_pecas_parcel_num, available_services, old_space_type_id, new_space_type_id, old_space_quantity, new_space_quantity, old_year_built, new_year_built, deh.land_area, old_is_derelict, new_is_derelict, old_is_brownfield, new_is_brownfield, zoning_rules_code, deh.taz,
	pbg.geom
	FROM q241i.development_events_history deh
	join q241i.parcels_backup_with_geom pbg on (pbg.pecas_parcel_num = deh.new_pecas_parcel_num);