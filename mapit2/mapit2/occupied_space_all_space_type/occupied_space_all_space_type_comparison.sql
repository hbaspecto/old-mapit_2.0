select
  scenario_ref.zonenumber,
  scenario_ref.year_run,
  scenario_policy.quantity - scenario_ref.quantity

from

(
select
  zonenumber,
  year_run,
  sum(-amount) as quantity
from
  output.all_zonalmakeuse_act_com
where
  scenario = '{{item.scenario_ref}}' and
  {{item.year_start}} <= year_run and
  year_run <= {{item.year_end}} and
  commodity in ({{item.commodities}})
group by
  zonenumber
  year_run,
) scenario_ref

full join

(
select
  zonenumber,
  year_run,
  sum(-amount) as quantity
from
  output.all_zonalmakeuse_act_com
where
  scenario = '{{item.scenario_policy}}' and
  {{item.year_start}} <= year_run and
  year_run <= {{item.year_end}} and
  commodity in ({{item.commodities}})
group by
  zonenumber,
  year_run
) scenario_policy

using (zonenumber, year_run)

where
      scenario_ref.year_run is not null
      and
      scenario_policy.year_run is not null

order by
      zonenumber,
      year_run
