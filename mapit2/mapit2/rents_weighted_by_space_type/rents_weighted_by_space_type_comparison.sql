select
  coalesce(scenario_ref.zonenumber, scenario_policy.zonenumber) as zonenumber,
  coalesce(scenario_ref.year_run, scenario_policy.year_run) as year_run,
  coalesce(scenario_policy.weight_rent,0.0) - coalesce(scenario_ref.weight_rent,0.0) as weight_rent

from
(
SELECT
        zonenumber,
        year_run,
        sum(demand * price) / sum(demand) as weight_rent
FROM
        output.all_exchange_results
where
        scenario = '{{item.scenario_ref}}' and
        {{item.year_start}} <= year_run and
        year_run <= {{item.year_end}} and
        commodity in ( {{item.commodities }} ) and
        {{item.demand_min}} < demand
group by
      zonenumber,
      year_run
order by
      zonenumber,
      year_run
) scenario_ref

full join

(
SELECT
        zonenumber,
        year_run,
        sum(demand * price)/ sum(demand) as weight_rent
FROM
        output.all_exchange_results
where
        scenario = '{{item.scenario_policy}}' and
        {{item.year_start}} <= year_run and
        year_run <= {{item.year_end}} and
        commodity in ( {{item.commodities }} ) and
        {{item.demand_min}} < demand
group by
      zonenumber,
      year_run

order by
      zonenumber,
      year_run
) scenario_policy

on
   scenario_ref.zonenumber = scenario_policy.zonenumber
   and
   scenario_ref.year_run   = scenario_policy.year_run

order by
      zonenumber,
      year_run
