SELECT
        zonenumber,
        year_run,
        sum(demand * price) / sum(demand) as weight_rent
FROM
        output.all_exchange_results
where
        scenario = '{{scenario}}' and
        {{item.year_start}} <= year_run and
        year_run <= {{item.year_end}} and
        {{item.demand_min}} < demand and
        commodity in ( {{item.commodities|safe }} )
group by
      zonenumber,
      year_run

order by
      zonenumber,
      year_run
