#
# mapit_2.0/mapit2/mapit2/app_autoimport.py ---
#

import glob
import importlib
import os
import re

#####

DEBUG = False


def find_and_import_page_files():
    """Find and import the subpages.

    This imports them into python, not the namespace of the current module.
    """
    this_dir_path = os.path.dirname(__file__)
    page_py_lst = glob.glob(os.path.join(this_dir_path, "*/*_page.py"))
    for page_py in sorted(page_py_lst):
        # Convert them to a package
        page_py = page_py[len(this_dir_path) + 1:-3]
        mod_name = "." + re.sub("/", ".", page_py)
        if DEBUG:
            print(f"### import {mod_name}")
        importlib.import_module(mod_name, "mapit2")
