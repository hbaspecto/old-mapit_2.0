SELECT
  taz,
  taz/100 as luz,
  year_run,
  noc11 as nocs,
  sum(amt) as employment
FROM
  output.all_employment
WHERE
  scenario = '{{ item.scenario }}'  and
  year_run = '{{ item.year }}'
  -- FIXME: Blank is all, or a list.
  -- noc11 in ( 1, 2, ... )
GROUP BY
      taz,
      luz,
      year_run,
      noc11
ORDER BY
      taz,
      luz,
      year_run,
      noc11
