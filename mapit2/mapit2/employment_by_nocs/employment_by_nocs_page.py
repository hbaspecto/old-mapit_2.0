#
# mapit_2.0/mapit2/mapit2/employment_by_nocs/employment_by_nocs_page.py ---
#

from ..common_vars import *
from .employment_by_nocs import (
    EmploymentByNocs,
    EmploymentByNocsForm,
)

#####


@app.route('/employment_by_nocs/create', methods=['POST', 'GET'])
def employment_by_nocs():
    # TODO CSRF protection
    # TODO this default URI shouldn't be here, but at least it's from the environment
    item = EmploymentByNocs(
        db_connection_string=MAPITDB_DEFAULT_URI,
        # TODO put this in environment
        schema_for_views=MAPIT2_VIEWS_SCHEMA,
    )
    form = EmploymentByNocsForm()
    if flask.request.method == 'GET':
        item.initializeForm(form)

    message_err = None
    message_ok = None

    # A function to process the form, so we can use "return"
    def process_form():
        nonlocal message_ok, message_err

        # dont do anything if button not clicked, e.g. if POST is from a API call.
        if not form.button.data:
            return
        if not form.validate():
            return

        item.copy_from_form(form)
        db.session.add(item)
        db.session.commit()

        try:
            if rqjob_enabled():
                rqjob_enqueue_call(
                    item,
                    "make_views",
                    method_args=[luz_table, luz_key])
            else:
                item.make_views(luz_table, luz_key)

        except Exception as e:
            print('There was an issue creating your data views, ' + str(e))
            return 'There was an issue creating your data views, ' + str(e)

        return None

    if flask.request.method == 'POST':
        if form.validate():
            print("Posting NOCS view...")
            # TODO test for content type JSON because we want this to be an API as well.
            # Store it
            vals = process_form()
            if vals is None:
                return flask.redirect(flask.url_for('index'))
            else:
                message_err = vals
        else:
            message_err = form.errors

    return flask.render_template('employment_by_nocs/employment_by_nocs.html', **locals())


@app.route('/employment_by_nocs/view/<int:id>', methods=['GET', ])
def view_employment_by_nocs(id):
    item = db.session.query(EmploymentByNocs).get_or_404(id)
    response = flask.make_response(item.generate_layer_file(), 200)
    response.mimetype = "text/plain"
    response.headers.set(
        'Content-Disposition', 'attachment', filename='{view}.qlr'.format(view=item.data_view_name))
    return response

#####


# Add our function and description to the list of pages.
define_page(
    func='employment_by_nocs',
    category="12: Employment",
    title="Employment by NOCS")
