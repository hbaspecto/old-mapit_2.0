#
# mapit_2.0/mapit2/mapit2/employment_by_nocs/employment_by_nocs.py ---
#

from ..common_vars import *

#####


class EmploymentByNocs(MapView):

    __tablename__ = 'employment_by_nocs'

    id = sq.Column(
        sq.Integer,
        sq.ForeignKey(
            f'{MAPIT2_APP_SCHEMA}.mapview.id',
            ondelete="CASCADE"),
        primary_key=True)

    # OOO THESE ARE THE USER CHOSEN VALUES FOR THE {{}} IN THE QUERY
    year = sq.Column(sq.Integer)
    scenario = sq.Column(sq.String(20))
    # TODO TAZ or LUZ boolean

    __mapper_args__ = {
        'polymorphic_identity': 'employment_by_nocs',
    }

    def generate_view_SQL(self):
        template = sqlenv.get_template('employment_by_nocs/employment_by_nocs.sql')
        sql = template.render(item=self)
        return sql

    @property
    def view_name(self):
        # OOO How do you want the view named, after it is created
        return '{scenario}_empl_nocs_{year}_{timestamp}'.format(
            scenario=self.scenario,
            year=self.year,
            timestamp=self.cur_timestamp())

    @property
    def description(self):
        # OOO describe the view
        return "Employment for scenario {scenario} for year {year} by NOCS code".format(
            scenario=self.scenario,
            year=self.year)

    def generate_layer_file(self):
        # OOO the layer definition file the user should use to view it in QGIS
        template = qlrenv.get_template('employment_by_nocs/employment_by_nocs.qlr')
        a = parse_db_uri(self.db_connection_string)
        if not a:
            raise Exception("Couldn't parse connection string to parameterize layer file, {connstring}".format(
                connstring=self.deb_connection_string))
        print(a)
        # TODO get these from the connection string HARLEY will do it and not with this abomination
        #host = self.db_connection_string.split('@')[1].split('.')[0]

        host = a.get('host')
        port = a.get('port', 5432)
        user = a.get('user')
        password = a.get('password')
        database = a.get('database')
        schema = self.schema_for_views
        view_name = self.data_view_name
        layer_def_file = template.render(
            PGHOST=host,
            PGPORT=port,
            PGUSER=user,
            PGPASSWORD=password,
            PGDATABASE=database,
            PGSCHEMA=schema,
            #
            view_name=view_name,
            scenario=self.scenario,
            year=self.year)
        return layer_def_file

    def get_view_metacolumns(self):
        return ('total_', 'nocs', '_jobs',)


class EmploymentByNocsForm(MapViewCreatorForm):

    # OOO how you want the fields presented to the user in the form.
    year = wtforms.IntegerField(
        "Year",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])
    scenario = wtforms.StringField(
        "Scenario",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=3,
                max=20,
                message="Scenario name short and at least 3 characters"),
        ])
