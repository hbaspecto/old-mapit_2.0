import os
import traceback

import flask
import sqlalchemy as sq
import wtforms

from ..common_vars import *
from ..rqjob import (
    rqjob_enabled,
    rqjob_enqueue_call,
)
from .avg_size_occupied_units import (
    AvgSizeOccupiedUnits,
)

#####


class AvgSizeOccupiedUnitsForm(MapViewCreatorForm):

    scenario_policy = wtforms.StringField(
        "Scenario Policy",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=3,
                max=20,
                message="Scenario name short and at least 3 characters"),
        ])

    scenario_ref = wtforms.StringField(
        "Scenario Ref",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=3,
                max=20,
                message="Scenario name short and at least 3 characters"),
        ])

    year_start = wtforms.IntegerField(
        "Year Start",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])
    year_end = wtforms.IntegerField(
        "Year End",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])

    commodities = wtforms.StringField(
        "Commodities single quotes separated by commas",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=5,
                max=600,
                message="Please enter a list of commodities separated by commas."),
        ])

#####


@app.route('/avg_size_occupied_units/create', methods=['POST', 'GET'])
def avg_size_occupied_units():
    # TODO CSRF protection
    # TODO this default URI shouldn't be here, but at least it's from the environment
    item = AvgSizeOccupiedUnits(
        db_connection_string=MAPITDB_DEFAULT_URI,
        # TODO put this in environment
        schema_for_views=MAPIT2_VIEWS_SCHEMA,
    )

    form = AvgSizeOccupiedUnitsForm()
    if flask.request.method == 'GET':
        item.initializeForm(form)

    message_err = None
    message_ok = None

    # A function to process the form, so we can use "return"
    def process_form():
        nonlocal message_ok, message_err

        # dont do anything if button not clicked, e.g. if POST is from a API call.
        if not form.button.data:
            return
        if not form.validate():
            return
        # print("process_form")

        item.copy_from_form(form)
        db.session.add(item)
        db.session.commit()

        try:
            if rqjob_enabled():
                rqjob_enqueue_call(
                    item,
                    "make_views",
                    method_args=[luz_table, luz_key])
            else:
                item.make_views(luz_table, luz_key)

        except Exception as e:
            traceback.print_exc()
            print('There was an issue creating your data views, ' + str(e))
            # return 'There was an issue creating your data views, ' + str(e)
            raise

        return None

    if flask.request.method == 'POST':
        if form.validate():
            print("Posting view...")
            # TODO test for content type JSON because we want this to be an API as well.
            # Store it
            vals = process_form()
            if vals is None:
                return flask.redirect(flask.url_for('index'))
            else:
                message_err = vals
        else:
            message_err = form.errors

    return flask.render_template(
        'avg_size_occupied_units/avg_size_occupied_units.html',
        **locals())


@app.route(
    '/avg_size_occupied_units/view/<int:id>',
    methods=['GET', ])
def view_avg_size_occupied_units(id):
    item = db.session.query(AvgSizeOccupiedUnits).get_or_404(id)
    response = flask.make_response(item.generate_layer_file(), 200)
    response.mimetype = "text/plain"
    response.headers.set(
        'Content-Disposition', 'attachment', filename='{view}.qlr'.format(view=item.data_view_name))
    return response

#####


# Geraldine had this in red in the email.
# Will ask about removal.
define_page(
    func='avg_size_occupied_units',
    category="14: Units",
    title="Avg Size of Occupied Units")
