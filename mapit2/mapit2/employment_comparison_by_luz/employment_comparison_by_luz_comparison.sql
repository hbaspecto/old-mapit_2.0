select
  coalesce(scenario_ref.luz,scenario_policy.luz) as luz,
  coalesce(scenario_ref.year_run, scenario_policy.year_run) as year_run,
  coalesce(scenario_policy.employment,0.0) - coalesce(scenario_ref.employment,0.0) as employment_diff
from
(

select
  taz/100 as luz,
  year_run,
  sum(amt) as employment
from
  output.all_employment R
where
  scenario = '{{item.scenario_ref}}' and
  {{item.year_start}} <= R.year_run and
  R.year_run <= {{item.year_end}}
  {% if item.naics_list %}
  and
  naics in ({{item.naics_list}})
  {% endif %}
group by
      taz/100,
      year_run
order by
      taz/100,
      year_run
) scenario_ref

full join

(
select
  taz/100 as luz,
  year_run,
  sum(amt) as employment
from
  output.all_employment P
where
  scenario = '{{item.scenario_policy}}' and
  {{item.year_start}} <= P.year_run and
  P.year_run <= {{item.year_end}}
  {% if item.naics_list %}
  and
  naics in ({{item.naics_list}})
  {% endif %}
group by
      taz/100,
      year_run
order by
      taz/100,
      year_run

) scenario_policy

using (luz,year_run)

where
        scenario_ref.year_run is not null and
        scenario_policy.year_run is not null

order by
      coalesce(scenario_ref.luz, scenario_policy.luz),
      coalesce(scenario_ref.year_run, scenario_policy.year_run)
