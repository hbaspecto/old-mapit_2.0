SELECT
--Population by LUZ, TAZ
scenario, year_run, zonenumber as taz, b.luz_jdh as luz, population_at_avg_size as pop
FROM manual_views.taz_population a
join public.taz5k_v4 b on a.zonenumber = b.taz5kv4
where scenario in ('I247', 'P248a')
and year_run in (2019, 2021, 2023, 2025, 2027, 2029, 2031, 2033, 2035, 2037, 2039)


SELECT
--Employment by LUZ, TAZ
scenario, year_run, taz, b.luz_jdh as luz, sum(amt) as emp
FROM output.all_employment a
join public.taz5k_v4 b on a.taz = b.taz5kv4
where scenario in ('I247', 'P248a')
and year_run in (2019, 2021, 2023, 2025, 2027, 2029, 2031, 2033, 2035, 2037, 2039)
group by year_run, scenario, taz, b.luz_jdh;
