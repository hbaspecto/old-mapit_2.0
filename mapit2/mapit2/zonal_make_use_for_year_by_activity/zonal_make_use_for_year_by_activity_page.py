import html

from ..common_vars import *
from .zonal_make_use_for_year_by_activity import (
    ZonalMakeUseForYearByActivity,
    ZonalMakeUseForYearByActivityForm,
)

# TODO can we override this as a combo update/create (i.e. if id is null then create, otherwise update on POST
# and just review on GET)


@app.route('/zonal_make_use_for_year_by_activity/create', methods=['POST', 'GET'])
def zonal_make_use_for_year_by_activity():
    # TODO CSRF protection
    # TODO this default URI shouldn't be here, but at least it's from the environment
    item = ZonalMakeUseForYearByActivity(
        db_connection_string=MAPITDB_DEFAULT_URI,
        # TODO put this in environment
        schema_for_views=MAPIT2_VIEWS_SCHEMA,
    )
    form = ZonalMakeUseForYearByActivityForm()
    if flask.request.method == 'GET':
        item.initializeForm(form)

    message_err = None
    message_ok = None

    # A function to process the form, so we can use "return"
    def process_form():
        nonlocal message_ok, message_err

        # dont do anything if button not clicked, e.g. if POST is from a API call.
        if not form.button.data:
            return
        if not form.validate():
            return
        # print("process_form")
        item.year = form.year.data
        item.scenario1 = form.scenario1.data
        item.scenario2 = form.scenario2.data
        item.activity_list = html.unescape(form.activity_list.data)
        item.commodity_list = html.unescape(form.commodity_list.data)
        item.moru = form.moru.data
        item.typename = form.typename.data

        # These four could be in a superclass method
        item.date_to_expire = form.date_to_expire.data
        item.created_by = form.created_by.data
        item.db_connection_string = form.db_connection_string.data
        item.schema_for_views = form.schema_for_views.data

        print(str(item.generate_view_SQL()))
        try:
            item.make_views(luz_table, luz_key)
        except Exception as e:
            print('There was an issue creating your data views, ' + str(e))
            return 'There was an issue creating your data views, ' + str(e)
        # These things we will create, so we shoulnd't get them from the form.
        #item.data_view_name = form.data_view_name.data
        #item.crosstab_materialized_view_name = form.crosstab_materialized_view_name.data
        #item.geom_view_name = form.geom_view_name.data
        # TODO create the new views in the database, (call another method to keep the business logic separate)
        try:
            db.session.add(item)
            db.session.commit()
            # TODO should take the user to the page to download the layer definiton file
        except Exception as e:
            return 'There was an issue adding your view, ' + str(e)

    if flask.request.method == 'POST':
        if form.validate():
            print("Posting ZonalMakeUse view...")
            # TODO test for content type JSON because we want this to be an API as well.
            # Store it
            vals = process_form()
            if vals is None:
                return flask.redirect(flask.url_for('index'))
            else:
                message_err = vals
                # return
                # flask.render_template('zonal_make_use_for_year_by_activity/zonal_make_use_for_year_by_activity.html',
                # **locals())
        else:
            message_err = form.errors

    return flask.render_template(
        'zonal_make_use_for_year_by_activity/zonal_make_use_for_year_by_activity.html', **locals())


@app.route('/zonal_make_use_for_year_by_activity/view/<int:id>', methods=['GET', ])
def view_zonal_make_use_for_year_by_activity(id):
    item = db.session.query(ZonalMakeUseForYearByActivity).get_or_404(id)
    response = flask.make_response(item.generate_layer_file(), 200)
    response.mimetype = "text/plain"
    response.headers.set(
        'Content-Disposition', 'attachment', filename='{view}.qlr'.format(view=item.data_view_name))
    return response

#####


define_page(
    func='zonal_make_use_for_year_by_activity',
    category="16: Production or consumption of Puts by activities by zone",
    title="ZonalMakeUse for a year grouped by activity, comparison")
