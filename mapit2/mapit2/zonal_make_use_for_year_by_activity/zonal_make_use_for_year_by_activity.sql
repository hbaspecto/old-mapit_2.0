select
        zonenumber,
        -- this will generate a category for each p243 + activity (e.g. p243gt_401_Book)
        '{{item.scenario2 }}gt_'||activity as activity_compare,
        greatest({{item.scenario2 }}.{{item.typename}} - {{item.scenario1}}.{{item.typename}},0.0) as {{item.typename}}{{item.demand_or_supply}}diff
from
(
-- note the three columns, 1) geography, 2) cateogorization, 3) values
SELECT zonenumber, activity, sum(amount) as {{item.typename}}
	FROM output.all_zonalmakeuse_act_com
	where scenario = '{{item.scenario2}}'
	and year_run = {{item.year}}
	and activity in ( {{activity_list_str|safe }})
	and commodity in ( {{item.commodity_list|safe }} )
 	and moru = '{{item.moru }}'
	group by zonenumber, activity) {{item.scenario2}}
join
(SELECT zonenumber, activity, sum(amount) as {{item.typename}}
	FROM output.all_zonalmakeuse_act_com
	where scenario = '{{item.scenario1}}'
	and year_run = {{item.year}}
 	and activity in ({{activity_list_str|safe }})
	and commodity in ( {{item.commodity_list|safe }} )
 	and moru = '{{item.moru }}'
	group by zonenumber, activity) {{item.scenario1}}
using (zonenumber, activity)
union all -- combines the p243gt and q241igt differences into one view
-- Then, we subtract p243 labour from q241i labour to generate differences where q241i labour is
-- greater than (gt) p243 labour
select
        zonenumber,
        -- this will generate a category for each q241i + activity combination (e.g. q241igt_401_Book)
        '{{item.scenario1 }}gt_'||activity as activity_compare,
        greatest({{item.scenario1 }}.{{item.typename}} - {{item.scenario2}}.{{item.typename}},0.0) as {{item.typename}}{{item.demand_or_supply}}diff
from
(SELECT zonenumber, activity, sum(amount) as {{item.typename}}
	FROM output.all_zonalmakeuse_act_com
	where scenario = '{{item.scenario2}}'
	and year_run = {{item.year}}
 	and activity in ({{activity_list_str|safe }})
	and commodity in ( {{item.commodity_list|safe }} )
 	and moru = '{{item.moru}}'
	group by zonenumber, activity) {{item.scenario2}}
join
(SELECT zonenumber, activity, sum(amount) as {{item.typename}}
	FROM output.all_zonalmakeuse_act_com
	where scenario = '{{item.scenario1}}'
	and year_run = {{item.year}}
 	and activity in ({{activity_list_str|safe }})
	and commodity in ({{item.commodity_list|safe }})
 	and moru = '{{item.moru}}'
	group by zonenumber, activity) {{item.scenario1}}
using (zonenumber,activity)
