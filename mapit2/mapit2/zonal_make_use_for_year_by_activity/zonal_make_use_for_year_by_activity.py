from ..common_vars import *

#####


class ZonalMakeUseForYearByActivity(MapView):

    __tablename__ = 'zonal_make_use_for_year_by_activity'

    id = sq.Column(
        sq.Integer,
        sq.ForeignKey(
            f'{MAPIT2_APP_SCHEMA}.mapview.id',
            ondelete="CASCADE"),
        primary_key=True)

    # OOO THESE ARE THE USER CHOSEN VALUES FOR THE {{}} IN THE QUERY
    year = sq.Column(sq.Integer)
    scenario1 = sq.Column(sq.String(20))
    scenario2 = sq.Column(sq.String(20))
    activity_list = sq.Column(sq.Text)
    commodity_list = sq.Column(sq.Text)
    moru = sq.Column(sq.String(1))
    typename = sq.Column(sq.String(20))

    demand_or_supply = "demand"

    __mapper_args__ = {
        'polymorphic_identity': 'zonal_make_use_for_year_by_activity',
    }

    def generate_view_SQL(self):
        if self.moru == 'U':
            self.demand_or_supply = "demand"
        else:
            self.demand_or_supply = "supply"
        template = sqlenv.get_template('zonal_make_use_for_year_by_activity/zonal_make_use_for_year_by_activity.sql')
        activity_list_str = str(self.activity_list)
        print("Activity list is " + activity_list_str + " and is of type " + str(type(activity_list_str)))
        sql = template.render(item=self, activity_list_str=activity_list_str)
        # print(sql)
        return sql

    @property
    def view_name(self):
        # OOO How do you want the view named, after it is created
        return '{scenario1}_{scenario2}_zonalmakeuse_{year}_{timestamp}'.format(
            scenario1=self.scenario1,
            scenario2=self.scenario2,
            year=self.year,
            timestamp=self.cur_timestamp())

    @property
    def description(self):
        # OOO describe the view
        return "This is a comparison summary file of ZonalMakeUse output file. It compares scenarios {scenario2} and "
        " {scenario1} for year {year}.  It is for the following activities and commodities/puts, and "
        " is grouped by activity.  It is also broken out into two sets, one where {scenario1} is greater, "
        " and one where {scenario2} is greater. There are no negative numbers, so that QGIS can plot charts. \n"
        "Activity list: {activity_list}  \n"
        "Commodity list: {commodity_list}".format(
            scenario1=self.scenario1,
            year=self.year,
            activity_list=self.activity_list,
            commodity_list=self.commodity_list)

    def get_view_metacolumns(self):
        ''' This is for a subclass to define the name of it's columns and how it wants to prefix
        and suffix them in the crosstab
        '''
        return ('total_', 'activity_compare', '',)

    def generate_layer_file(self):
        # OOO the layer definition file the user should use to view it in QGIS
        template = qlrenv.get_template('zonal_make_use_for_year_by_activity/zonal_make_use_for_year_by_activity.qlr')
        a = parse_db_uri(self.db_connection_string)
        if not a:
            raise Exception("Couldn't parse connection string to parameterize layer file, {connstring}".format(
                connstring=self.deb_connection_string))
        print(a)
        # TODO get these from the connection string HARLEY will do it and not with this abomination
        #host = self.db_connection_string.split('@')[1].split('.')[0]

        host = a.get('host')
        port = a.get('port', 5432)
        user = a.get('user')
        password = a.get('password')
        database = a.get('database')
        schema = self.schema_for_views
        view_name = self.data_view_name

        layer_def_file = template.render(
            PGPORT=port,
            PGSCHEMA=schema,
            PGHOST=host,
            PGUSER=user,
            PGPASSWORD=password,
            PGDATABASE=database,
            #
            activity_list=self.activity_list,
            commodity_list=self.commodity_list,
            scenario1=self.scenario1,
            scenario2=self.scenario2,
            view_name=view_name,
            year=self.year)
        return layer_def_file


class ZonalMakeUseForYearByActivityForm(MapViewCreatorForm):

    # OOO how you want the fields presented to the user in the form.
    year = wtforms.IntegerField("Year", validators=[wtforms.validators.InputRequired(),
                                                    wtforms.validators.NumberRange(
        min=1990, max=2150, message="Year between should be in this century"),
        #wtforms_validators.Integer(message = "Year must be integer")
    ])
    scenario1 = wtforms.StringField("Scenario1", validators=[wtforms.validators.InputRequired(),
                                                             wtforms.validators.Length(
        min=3, max=20, message="Scenario 1 (base) name short and at least 3 characters"),
        #wtforms_validators.AlphaNumeric(message = "Scenarios are Tnnnpppp with T representing type, nnn the number, and pppp purpose" )
    ])
    moru = wtforms.StringField("MorU", validators=[wtforms.validators.InputRequired(),
                                                   wtforms.validators.Length(
        min=1, max=1, message='"M" or "U" depending on whether you want Make or Use'),
        #wtforms_validators.AlphaNumeric(message = "Scenarios are Tnnnpppp with T representing type, nnn the number, and pppp purpose" )
    ])
    scenario2 = wtforms.StringField("Scenario2", validators=[wtforms.validators.InputRequired(),
                                                             wtforms.validators.Length(
        min=3, max=20, message="Scenario 2 (alternate) name short and at least 3 characters"),
        #wtforms_validators.AlphaNumeric(message = "Scenarios are Tnnnpppp with T representing type, nnn the number, and pppp purpose" )
    ])
    activity_list = wtforms.StringField("activity_list", validators=[wtforms.validators.InputRequired(),
                                                                     wtforms.validators.Length(
        min=3, max=1500, message="List of activities, single quoted and separated by commas"),
        #wtforms_validators.AlphaNumeric(message = "Scenarios are Tnnnpppp with T representing type, nnn the number, and pppp purpose" )
    ])
    commodity_list = wtforms.StringField("commodity_list", validators=[wtforms.validators.InputRequired(),
                                                                       wtforms.validators.Length(
        min=3, max=1500, message="List of commodities/puts, single quoted and separated by commas"),
        #wtforms_validators.AlphaNumeric(message = "Scenarios are Tnnnpppp with T representing type, nnn the number, and pppp purpose" )
    ])
    typename = wtforms.StringField("typename", default='labour', validators=[wtforms.validators.InputRequired(),
                                                                             wtforms.validators.Length(
                                                                                 min=3, max=15, message="Just a short label "),
                                                                             #wtforms_validators.AlphaNumeric(message = "Scenarios are Tnnnpppp with T representing type, nnn the number, and pppp purpose" )
                                                                             ])
