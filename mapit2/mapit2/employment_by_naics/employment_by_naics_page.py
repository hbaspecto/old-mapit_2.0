from ..common_vars import *
from .employment_by_naics import (
    EmploymentByNaics,
)

#####


class EmploymentByNaicsForm(MapViewCreatorForm):

    # OOO how you want the fields presented to the user in the form.
    year = wtforms.IntegerField(
        "Year",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(
                min=1990,
                max=2150,
                message="Year between should be in this century"),
        ])
    scenario = wtforms.StringField(
        "Scenario",
        validators=[
            wtforms.validators.InputRequired(),
            wtforms.validators.Length(
                min=3,
                max=20,
                message="Scenario name short and at least 3 characters"),
        ])

#####


@app.route(
    '/employment_by_naics/create',
    methods=['POST', 'GET'])
def employment_by_naics_create():
    item = EmploymentByNaics(
        db_connection_string=MAPITDB_DEFAULT_URI,
        schema_for_views=MAPIT2_VIEWS_SCHEMA,
    )

    form = EmploymentByNaicsForm()

    if flask.request.method == 'GET':
        item.initializeForm(form)

    message_err = None
    message_ok = None

    # A function to process the form, so we can use "return"
    def process_form():
        nonlocal message_ok, message_err

        # dont do anything if button not clicked, e.g. if POST is from a API call.
        if not form.button.data:
            return
        if not form.validate():
            return

        item.copy_from_form(form)
        db.session.add(item)
        db.session.commit()

        try:
            if rqjob_enabled():
                rqjob_enqueue_call(
                    item,
                    "make_views",
                    method_args=[luz_table, luz_key])
            else:
                item.make_views(luz_table, luz_key)

        except Exception as e:
            print('There was an issue creating your data views, ' + str(e))
            return 'There was an issue creating your data views, ' + str(e)

        return None

    if flask.request.method == 'POST':
        if form.validate():
            print("Posting NAICS view...")
            # TODO test for content type JSON because we want this to be an API as well.
            # Store it
            vals = process_form()
            if vals is None:
                return flask.redirect(flask.url_for('index'))
            else:
                message_err = vals
                # return flask.render_template('employment_by_naics/employment_by_naics.html',  **locals())
        else:
            message_err = form.errors

    return flask.render_template(
        'employment_by_naics/employment_by_naics.html',
        **locals())

#####


# Add our function and description to the list of pages.
define_page(
    func='employment_by_naics_create',
    category="12: Employment",
    title="Employment by NAICS")
