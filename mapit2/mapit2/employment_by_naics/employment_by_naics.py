from ..common_vars import *

#####


class EmploymentByNaics(MapView):

    __tablename__ = 'employment_by_naics'

    id = sq.Column(
        sq.Integer,
        sq.ForeignKey(
            f'{MAPIT2_APP_SCHEMA}.mapview.id',
            ondelete="CASCADE"),
        primary_key=True)

    # OOO THESE ARE THE USER CHOSEN VALUES FOR THE {{}} IN THE QUERY
    year = sq.Column(
        sq.Integer())
    scenario = sq.Column(
        sq.String(20))

    __mapper_args__ = {
        'polymorphic_identity': 'employment_by_naics',
    }

    def generate_view_SQL(self):
        template = sqlenv.get_template(
            'employment_by_naics/employment_by_naics.sql')
        sql = template.render(item=self)
        return sql

    @property
    def view_name(self):
        # OOO How do you want the view named, after it is created
        return '{scenario}_empl_naics_{year}_{timestamp}'.format(
            scenario=self.scenario,
            year=self.year,
            timestamp=self.cur_timestamp())

    @property
    def description(self):
        # OOO describe the view
        return "Employment for scenario {scenario} for year {year} by NAICS code".format(
            scenario=self.scenario,
            year=self.year)

    def generate_layer_file(self):
        # OOO the layer definition file the user should use to view it in QGIS
        template = qlrenv.get_template(
            'employment_by_naics/employment_by_naics.qlr')
        a = parse_db_uri(self.db_connection_string)
        if not a:
            raise Exception("Couldn't parse connection string to parameterize layer file, {connstring}".format(
                connstring=self.deb_connection_string))

        host = a.get('host')
        port = a.get('port', 5432)
        user = a.get('user')
        password = a.get('password')
        pgdatabase = a.get('database')
        pgschema = self.schema_for_views
        view_name = self.data_view_name
        layer_def_file = template.render(
            PGHOST=host,
            PGPORT=port,
            PGSCHEMA=schema,
            PGUSER=user,
            PGPASSWORD=password,
            PGDATABASE=database,
            #
            view_name=view_name,
            scenario=self.scenario,
            year=self.year)
        return layer_def_file

    def get_view_metacolumns(self):
        return ('total_', 'naics', '_jobs',)
