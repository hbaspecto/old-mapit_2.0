SELECT
  -- taz,
  taz/100 as luz,
  -- year_run,
  naics,
  sum(amt) as employment
FROM
  output.all_employment
where
  scenario = '{{ item.scenario }}'  and
  year_run = '{{ item.year }}'
  -- FIXME:
  -- naics in ( 11, 21, ...)
group by
      -- taz,
      luz,
      -- year_run,
      naics
order by
      -- taz,
      luz,
      -- year_run,
      naics
