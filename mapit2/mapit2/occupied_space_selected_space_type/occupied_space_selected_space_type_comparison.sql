select
  coalesce(scenario_ref.zonenumber, scenario_policy.zonenumber) as zonenumber,
  coalesce(scenario_ref.year_run, scenario_policy.year_run) as year_run,
  coalesce(scenario_policy.quantity,0.0) - coalesce(scenario_ref.quantity,0.0) as quantity

from

(
select
  zonenumber,
  year_run,
  sum(-amount) as quantity
from
  output.all_zonalmakeuse_act_com
where
  scenario = '{{item.scenario_ref}}' and
  {{item.year_start}} <= year_run and
  year_run <= {{item.year_end}} and
  commodity in ({{item.commodities}})
group by
  zonenumber,
  year_run
) scenario_ref

full join

(
select
  zonenumber,
  year_run,
  sum(-amount) as quantity
from
  output.all_zonalmakeuse_act_com
where
  scenario = '{{item.scenario_policy}}' and
  {{item.year_start}} <= year_run and
  year_run <= {{item.year_end}} and
  commodity in ({{item.commodities}})
group by
  zonenumber,
  year_run
) scenario_policy

on
        scenario_policy.zonenumber = scenario_ref.zonenumber
        and
        scenario_ref.year_run = scenario_policy.year_run

where
        scenario_ref.year_run is not null
        and
        scenario_policy.year_run is not null

order by
      zonenumber,
      year_run
