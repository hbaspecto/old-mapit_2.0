mapit_2.0/mapit2/mapit2/occupied_space_selected_space_type/README
======================================================================

This used to be called ``occupied_space_all_space_type``,
but was renamed to ``occupied_space_selected_space_type``.

The old one was left in place as we might want to see its data.
When we are done with it it can be removed.
(and clean up the old views as well.)
