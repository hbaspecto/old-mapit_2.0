select
  zonenumber,
  year_run,
  sum(-amount) as quantity
from
  output.all_zonalmakeuse_act_com
where
  scenario = '{{scenario}}' and
  {{item.year_start}} <= year_run and
  year_run <= {{item.year_end}} and
  commodity in ({{item.commodities}})
group by
  zonenumber,
  year_run
order by
  zonenumber,
  year_run
