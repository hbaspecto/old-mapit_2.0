#
# mrsgui/setup.py ---
#

from __future__ import (
    absolute_import,
)

import os
from distutils.core import (
    setup,
)

#####

setup(
    name="mapit2",
    version="0.0.1",
    description="Some python to build a QGIS layer file of PECAS outputs",
    author="HBA Specto Incorporated",
    url="www.hbaspecto.com",
    install_requires=[
        "flask",
        "flask-sqlalchemy",
        "flask_table",
        "flask_wtf",
        "jinja2",
        "psycopg2-binary",
        "pyyaml",
        "wtforms",
        "wtforms-validators",
        # "git://bitbucket.org/hbaspecto/hba-pg-csv",
    ],
    packages=[
        "mapit2",
        "qgs_util",
    ],
    package_dir={
        'mapit2': 'mapit2',
        "qgs_util": "qgs_util",
    },
    entry_points={
        'console_scripts': [
            # The server.
            "mapit2-server = mapit2.app:server_run",
            # The rqjob worker. (used for views and qgis)
            "mapit2-rq-worker = mapit2.rqjob.worker_main:worker_main",
            # working with "*.qgs" files.
            "qgs-util = qgs_util.qgs_util_main:main",
            # Used to run tests
            "mapit2-debug = mapit2.mapit2_debug_main:main_entry",
        ],
    },
)
