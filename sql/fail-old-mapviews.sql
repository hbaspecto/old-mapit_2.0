--
-- mapit_2.0/sql/fail-old-mapviews.sql ---
--
-- Fail old mapviews;
-- if they have been around more than a day and are unprocessed, they are dead.
--
-- \i ./sql/fail-old-mapviews.sql

select
        *
from
        mapit2_app.mapview
where
        date_created < (current_date -1 ) and
        ( rqjob_status not in ('done', 'error') or rqjob_status is null )
order by
      id desc
limit 10
;

update
        mapit2_app.mapview
set
        rqjob_status = 'error',
        rqjob_message = 'failed for being too old'
where
        date_created < (current_date -1 ) and
	( rqjob_status not in ('done', 'error' ) or rqjob_status is null )
returning
        id
;
