--
-- mapit_2.0/sql/alter-search-path.sql ---
--
-- set the default search_path so we can see our tables.

alter role :PGUSER in database :PGDATABASE set search_path = mapit2_app, mapit2_views, public;
