#!/bin/bash
#
# mapit_2.0/docker/docker-entrypoint.sh ---
#
# Docker runs this script when the container starts.
# This should be used/run inside the container.
#

# A subset of "./hba-setup.env"
# Some env vars are set in "./Dockerfile" (ENV ABC=...)
# others are passed in to this container. (--env ABC=...)

#
export ALEMBIC_CONFIG="${MAPIT2_DIR}/migrations/alembic.ini"
export MAPIT2_DB_URI="postgresql://${PGUSER}@${PGHOST}:${PGPORT}/${PGDATABASE}"

#####

#
arg="${1:-}"

case "${arg}" in
  --help|-h)
    shift
    exec cat ${MAPIT2_DIR}/sphinx/source/DOCKER_HELP.rst
    ;;
  --bash)
    shift
    exec bash
    ;;
  --migrate|-m)
    shift
    (cd migrations ; alembic upgrade head )
    ;;
  *)
    ;;
esac

# Now run the server.
exec ./ve/bin/mapit2-server
