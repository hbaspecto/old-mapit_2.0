mapit_2.0/migrations/README
==================================================

The alembic migrations for the server database.

The mapit data is in another database and requires a bit of
fussing to get it exported and loaded.
That process is documented here:

- https://bitbucket.org/hbaspecto/mapit-server/src/master/db/atlanta_mapit_brooks/

Workflow
--------------------------------------------------

The migrations workflow is:

- ``cd migrations``

- Run ``make _alembic_upgrade_head`` so you have the latest schema.

- Make changes to your models using the python code (subclasses of Mapit2ModelBase)
  Add/rename columns, tables, etc...

- Run ``make _alembic_revision_autogenerate`` to create a
  migration script.

- Edit the file in ``scripts/versions``, change the ``fixme`` suffix to something
  more descriptive, and the fixme comment in the file itself (at the top).

- Run ``make _alembic_upgrade_head`` to apply the changes.
  (Also checks the migration works.)

- Reformat the source code with ``make _black_scripts_versions``.

- Commit the migration file and your changes.

NOTE:

- Currently the "db/crosstab_function.sql" function is outside of the migrations.
  If you change it, you should run ``make _postgres_install_functions``
  as well.

  (We could have the migration load different versions,
  but that would be a bigger project.)


Inital setup
--------------------------------------------------

Copied the setup from ``mrsgui``, then modfied stuff for ``mapit2``.

::

    # Create the DB.
    # This was changed not to create the tables, as we will do it.
    (cd ../db ; make _postgres_recreate_database)

    # Make the inital script.
    make message=initial _alembic_revision_autogenerate

    # Now create the tables with the first migration.
    make _alembic_upgrade_head
