
drop table employment_by_naics cascade;
drop table mapview cascade ;
drop table occupied_units cascade ;
drop table zonal_make_use_for_year_by_activity cascade ;

drop schema mapit2_app cascade ;
create schema mapit2_app ;
