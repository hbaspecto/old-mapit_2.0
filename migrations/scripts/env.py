import os
from logging.config import fileConfig

from alembic import context
from sqlalchemy import (
    engine_from_config,
    pool,
)

from mapit2.env_vars import (
    MAPIT2_APP_SCHEMA,
)

# Now import the app, so we have all the tables defined in the metadata
import mapit2.app

# Import this to get a reference to the base metadata
import mapit2.models.sqlalchemy_model_base

#####

target_metadata = mapit2.models.sqlalchemy_model_base.Mapit2ModelBase.metadata

#####

#: will be filled in by exclude_tables_from_config below.
EXCLUDE_TABLES = None

# https://gist.github.com/utek/6163250


def exclude_tables_from_config(config_):
    tables_ = config_.get("tables", None)
    if tables_ is not None:
        tables = tables_.split(",")
        tables = [t.strip() for t in tables]
    return tables


# a selector func used below.
def include_object(object, name, type_, reflected, compare_to):
    """Need to add this to both online and offline migrations."""
    global EXCLUDE_TABLES
    if type_ == "table" and name in EXCLUDE_TABLES:
        # print("include_object: IGNORE: ", name, type_)
        return False
    else:
        # print("include_object: PROCESS: ", name, type_)
        return True


#####


def generate_db_url():
    # url = config.get_main_option("sqlalchemy.url")
    # postgresql://scott:tiger@localhost:5432/mydatabase
    url = "{}://{}:{}@{}:{}/{}".format(
        "postgresql",
        os.environ.get("PGUSER"),
        os.environ.get("PGPASSWORD"),
        os.environ.get("PGHOST"),
        os.environ.get("PGPORT"),
        os.environ.get("PGDATABASE"),
    )
    print("generate_db_url: {!r}".format(url))
    return url


#####

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# Build our list.
EXCLUDE_TABLES = exclude_tables_from_config(config.get_section("alembic:exclude"))

config.set_main_option("sqlalchemy.url", generate_db_url())

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")

    context.configure(
        dialect_opts={
            "paramstyle": "named",
        },
        include_object=include_object,
        literal_binds=True,
        target_metadata=target_metadata,
        url=url,
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            include_object=include_object,
            target_metadata=target_metadata,
            version_table_schema=f"{MAPIT2_APP_SCHEMA}",
            include_schemas=True,
        )

        with context.begin_transaction():
            context.execute(f"SET search_path TO {MAPIT2_APP_SCHEMA},public")
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
