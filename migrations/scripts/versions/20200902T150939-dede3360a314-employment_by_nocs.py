"""Add 'employment_by_nocs'.

Revision ID: dede3360a314
Revises: 691387fb8783
Create Date: 2020-09-02 15:09:39.209696

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "dede3360a314"
down_revision = "691387fb8783"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "employment_by_nocs",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("year", sa.Integer(), nullable=True),
        sa.Column("scenario", sa.String(length=20), nullable=True),
        sa.ForeignKeyConstraint(
            ["id"],
            ["mapit2_app.mapview.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
        schema="mapit2_app",
    )


def downgrade():
    op.drop_table("employment_by_nocs", schema="mapit2_app")
