"""Fix occupied_space tables.

Revision ID: b8174a96705e
Revises: 75bb881dbea9
Create Date: 2020-07-09 11:49:07.678739

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "b8174a96705e"
down_revision = "75bb881dbea9"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "occupied_space_all_residential",
        sa.Column(
            "crosstab_materialized_view_name_policy",
            sa.String(length=200),
            nullable=True,
        ),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column(
            "crosstab_materialized_view_name_ref", sa.String(length=200), nullable=True
        ),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column("data_view_name_policy", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column("data_view_name_ref", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column("geom_view_name_policy", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column("geom_view_name_ref", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column("scenario_policy", sa.String(length=20), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column("scenario_ref", sa.String(length=20), nullable=True),
        schema="mapit2_app",
    )
    op.drop_column("occupied_space_all_residential", "scenario", schema="mapit2_app")
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column(
            "crosstab_materialized_view_name_policy",
            sa.String(length=200),
            nullable=True,
        ),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column(
            "crosstab_materialized_view_name_ref", sa.String(length=200), nullable=True
        ),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column("data_view_name_policy", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column("data_view_name_ref", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column("geom_view_name_policy", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column("geom_view_name_ref", sa.String(length=200), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column("scenario_policy", sa.String(length=20), nullable=True),
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column("scenario_ref", sa.String(length=20), nullable=True),
        schema="mapit2_app",
    )
    op.drop_column("occupied_space_all_space_type", "scenario", schema="mapit2_app")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "occupied_space_all_space_type",
        sa.Column(
            "scenario", sa.VARCHAR(length=20), autoincrement=False, nullable=True
        ),
        schema="mapit2_app",
    )
    op.drop_column("occupied_space_all_space_type", "scenario_ref", schema="mapit2_app")
    op.drop_column(
        "occupied_space_all_space_type", "scenario_policy", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_space_type", "geom_view_name_ref", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_space_type", "geom_view_name_policy", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_space_type", "data_view_name_ref", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_space_type", "data_view_name_policy", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_space_type",
        "crosstab_materialized_view_name_ref",
        schema="mapit2_app",
    )
    op.drop_column(
        "occupied_space_all_space_type",
        "crosstab_materialized_view_name_policy",
        schema="mapit2_app",
    )
    op.add_column(
        "occupied_space_all_residential",
        sa.Column(
            "scenario", sa.VARCHAR(length=20), autoincrement=False, nullable=True
        ),
        schema="mapit2_app",
    )
    op.drop_column(
        "occupied_space_all_residential", "scenario_ref", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_residential", "scenario_policy", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_residential", "geom_view_name_ref", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_residential", "geom_view_name_policy", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_residential", "data_view_name_ref", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_residential", "data_view_name_policy", schema="mapit2_app"
    )
    op.drop_column(
        "occupied_space_all_residential",
        "crosstab_materialized_view_name_ref",
        schema="mapit2_app",
    )
    op.drop_column(
        "occupied_space_all_residential",
        "crosstab_materialized_view_name_policy",
        schema="mapit2_app",
    )
    # ### end Alembic commands ###
