mapit_2.0/sphinx/source/DOCKER_HELP
==================================================

How to run ``mapit2`` via docker.

Examples
--------------------------------------------------

Run the server:

::

    docker run --detach --name mapit2_server \
      gitlab-reg-1.hbaspecto.com/docker/images/mapit2_hba_dev

Run a bash shell:

::

    docker run --rm -it XXXXX --bash \
      gitlab-reg-1.hbaspecto.com/docker/images/mapit2_hba_dev

More examples in ``docker/Makefile``.


Options
--------------------------------------------------

- ``--help|-h`` = this help

- ``--migrate|-m`` = run migrations

- ``--bash`` = run bash.
  (for interactive use also add ``-it``)
