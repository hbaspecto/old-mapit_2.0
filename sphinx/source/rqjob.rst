Queuing and rqjob
==================================================

The DB takes a long time to make the crosstabs and
materialized views.  This can cause browser timeouts and
stop the SQL from completing.

To avoid that, the function call which does the long running
sql is put into a queue and the worker pulls it out from the
queue and runs it.

The running of the job also updates the DB as it runs with
status messages. The colors indicate the status of the job.
(Legend at the bottom of the page.)

This also helps out the DB server - it can devote resources
to finishing one query at a time, and not be overloaded
trying to process a bunch of maps at the same time.

If our DB server was faster and had more IO, we could have
more than one worker per queue.

Queue env vars:

::

    MAPIT2_RQ_QUEUE_QGS = makes the qgs files
    MAPIT2_RQ_QUEUE_VIEW = makes the views

The other queue is the ``qgs queue`` for making qgs files.
These dont take as much IO to do, so we can run them at the
same time as a view is being made.



Programmer Notes:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Once the job is complete, the job recored is removed from
  the ``rqjob`` queue; The ``mapit2`` rows remain with the
  users results.

- The ``RQ`` system wants to pickle the objects it sends
  over the queue. We dont want to attempt the pickle of an
  sqlalchemy object - It is a huge object collection which
  wouldnt deal well. (Cause it also has references to
  unpickable objects.)

  Instead, we pickle the ``id`` of the object and the worker
  pulls it from the DB.

- The deadline for the jobs is 1 hour per job.

- view the logs on ``leduc`` with:

::

    docker logs mapit2_hba_prd_work_qgs
    docker logs mapit2_hba_prd_work_view


- Run the worker with:

::

    mapit2-rq-worker --work-qgs --work-view
