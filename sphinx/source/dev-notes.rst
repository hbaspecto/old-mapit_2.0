dev-notes
==================================================

Hints for devs:

- Dont put anything other than SQL in the ``*.sql`` files.
  ``output.generate_crosstab_from_3_col_query`` chokes on it,
  and the error message isnt helpful.

- If you are missing data
  ``output.generate_crosstab_from_3_col_query`` barfs.
  If the input tables are empty, you get an error.

- If you select a range of years with no data, the crosstab fails.
  (cause of empty data.)

- Inputs to the crosstab function must be sorted.
  If not, then you get output which is all ``chopped up``
  as the function assumes it is ordered.

- When doing the ``_comparison.sql``, keep in mind that
  not all scenarios have all years.
  use ``coalesce`` to account for missing values.

- Order your data by (taz,luz) then year

Adding a new query:
--------------------------------------------------

- Pick an existing query which is kinda like the one you
  want to do.

- copy it with ``cp -r existing_query new_query``.

- Change all the names and sql.

- Generate the new table migration and apply it.

::

    cd migrations
    make _alembic_revision_autogenerate
    make _alembic_upgrade_head

- Test it locally

::

    make _server_run
    make _worker_run

- Commit your changes.

::

    git commit -m "new_query: blah blah blah."

- Use the jenkins job which builds the docker image and
  deploys it to PRD.

  `http://jenkins-2.office.hbaspecto.com/job/mapit_2.0/ <http://jenkins-2.office.hbaspecto.com/job/mapit_2.0/>`_
