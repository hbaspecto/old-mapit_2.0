CLI Commands
==================================================

qgs-util help
--------------------------------------------------

.. literalinclude:: qgs-util-help.txt
   :language: text

mapit2-debug help
--------------------------------------------------

.. literalinclude:: mapit2-debug-help.txt
   :language: text
