qgis intergration
==================================================

``mapit2`` can generate maps for the views it creates.


QGIS
--------------------------------------------------

The maps generated are ``.gqs`` maps.
To view these maps you will need to install:

- Python 3.6 (BEFORE installing QGIS)
  `python 3.6.8 <https://www.python.org/ftp/python/3.6.8/>`_

- `QGIS <https://qgis.org/en/site/forusers/download.html>`_

After installing, start it up, and ensure you have the ``DB
Manager`` plugin.



Viewing the maps
--------------------------------------------------

- Generate a ``mapit2`` view, and then wait for it to complete.

- Visit the ``info`` page of the view.

- Click on the ``download QGIS`` 

- After the file downloads, open it.




QGIS admin
==================================================

For programmers and sysadmins.



Adding a new QGIS template
--------------------------------------------------

When a new mapit2 view is added, or you want to update the
QGIS file for a view:

- Run the new mapit2 view to create some data to make the
  initial QGIS file.

- Create a new QGIS project.

- Add three layers which reference the DB to the project, in
  this order:

  - policy
  - reference
  - diff

- Save the QGIS project and copy it to the mapit2 view.

- Commit it to git and push.

- Deploy to PRD.


How it works
--------------------------------------------------

Mapit2 uses an xml parser to replace some of the info in the
XML project (dbinfo and view names) in the template you created.

You do not have to edit the XML to add template information.
This means you can just replace the file.
