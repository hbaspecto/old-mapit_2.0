DEPLOYMENT
==================================================

How mapit-2.0 is deployed to our users.

The suggested ports are:

- PRD: 5631
- STG: 5632
- DEV: 5633

(The nginx config is below.)

There are three processes running:

- mapit2_hba_prd_svr = main web server
- mapit2_hba_prd_work_view = Creates the DB views. (can take awhile...)
- mapit2_hba_prd_work_qgs = Generates the QGS files. (1-2 min per.)



Jenkins
--------------------------------------------------

Jenkins has a couple of jobs which will do the (more common)
last two steps of the deployment, building the image and
starting the server.

Be sure your changes are pushed to the `master` branch on
BitBucket, as Jenkins will pull the changes from
BitBucket. Also in site-templates make sure your templates
are in the hba-prd directory if you are hoping they'll show
up in production.

- `mapit2--deploy-hba-STG <http://jenkins-2.office.hbaspecto.com/job/mapit_2.0/job/mapit2--deploy-hba-STG/>`_
  = deploys the staging server.

- `mapit2--deploy-hba-PRD <http://jenkins-2.office.hbaspecto.com/job/mapit_2.0/job/mapit2--deploy-hba-PRD/>`_
  = deploys the production server.

Other jobs at: http://jenkins-2.office.hbaspecto.com/job/mapit_2.0/


Viewing logs
--------------------------------------------------

You can run the jenkins job which will show you the last ``30min`` of logs.
(`mapit2--PRD-logs <http://jenkins-2.office.hbaspecto.com/job/mapit_2.0/job/mapit2--PRD-logs/>`_)

For more control, your can login to ``leduc`` and run the command:

::

    docker logs --since 30m mapit2_hba_prd_svr

(Remember, there are three docker processes running, you may
have to look at the other two as well to see your error.
You can also use different args if you need more or less data.)


Deployment Details
------------------

Deployment is several steps:

- The nginx web proxy:

  This is configured in
  `mapit2-proxy-play.yml <https://bitbucket.org/hbaspecto/systems-hbaspecto-com/src/master/ansible/plays/mapit2-proxy-play.yml>`_.
  Once set up, it shouldnt need to be updated.

- Creation of the database:

::

    # do this on leduc
    ssh leduc

    cd mapit_2.0

    # load the VE
    source ./hba-setup.env
    # load the HBA-STG values
    source ./hba-setup/setup-hba-stg.env

    # create the db
    # Careful! dont remove an active db!
    cd ${MAPIT2_DIR}/db
    make _postgres_create_user
    make _postgres_make_database

    # build the docker image.
    cd ${MAPIT2_DIR}/docker
    make _docker_build

    # start it running on leduc
    cd ${MAPIT2_DIR}/ansible
    make _deploy_server

- Creating the docker image:

::

    cd docker
    make _mapit2_build

- Starting the container

::

    cd docker
    make _run_server




Redis Server
--------------------------------------------------

The redis server was created with
`redis-server-play.yml <https://bitbucket.org/hbaspecto/systems-hbaspecto-com/src/master/ansible/plays/redis-server-play.yml>`_.


Nginx proxy
--------------------------------------------------

The proxy was created with
`mapit2-prd-proxy-play.yml <https://bitbucket.org/hbaspecto/systems-hbaspecto-com/src/master/ansible/plays/mrsgui2-proxy-play.yml>`_.



