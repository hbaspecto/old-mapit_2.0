mapit_2.0/sphinx/source/DEBUG
==================================================

Notes from debugging problems.  Saved here as it might be
helpful for debugging other problems too.


Debugging job for jea
--------------------------------------------------

One of the scenarios was missing data, so the crosstab
failed.  While it would be nice to have the crosstab give a
better message, we should check that all the data is there
before calling the crosstab func.


Debugging job 416
--------------------------------------------------

Running under the debugger we see::

  sqlalchemy.exc.ProgrammingError:
  (psycopg2.errors.DuplicateColumn) column "coalesce"
  specified more than once

Maybe this is due to sqlalchemy versions?

::

    Flask-SQLAlchemy==2.5.1
    SQLAlchemy==1.4.22
