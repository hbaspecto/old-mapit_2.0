mapit_2.0
==================================================

Notes:

- If your view turns red, then the job to create your view
  failed.  This could be for a couple of reasons -- A common
  one is there was no data in the year range selected.
  Check the years were loaded. (like with psql)

  If the data is there, then ask a dev to take a look.
  Could be an error in the code or SQL.


Contents
==================================================

.. toctree::
   :maxdepth: 2

   top-readme
   dev-notes
   qgis
   qgs-db-change
   commands
   rqjob
   DEPLOYMENT
   DOCKER_HELP
   DEBUG


Modules
==================================================

.. autosummary::
   :toctree: modules

   mapit2
   mapit2.app
   mapit2.appbase
   mapit2.avg_size_occupied_units.avg_size_occupied_units
   mapit2.avg_size_occupied_units.avg_size_occupied_units_page
   mapit2.avg_size_residential_units.avg_size_residential_units
   mapit2.avg_size_residential_units.avg_size_residential_units_page
   mapit2.employment_by_naics
   mapit2.employment_by_naics.employment_by_naics
   mapit2.employment_by_naics.employment_by_naics_page
   mapit2.employment_by_nocs.employment_by_nocs
   mapit2.employment_by_nocs.employment_by_nocs_page
   mapit2.employment_comparison_by_luz.employment_comparison_by_luz
   mapit2.employment_comparison_by_luz.employment_comparison_by_luz_page
   mapit2.employment_comparison_by_taz.employment_comparison_by_taz
   mapit2.employment_comparison_by_taz.employment_comparison_by_taz_page
   mapit2.models.mapqgsfile
   mapit2.models.mapview_superclass
   mapit2.models.mixins
   mapit2.models.sqlalchemy_model_base
   mapit2.occupied_space_all_residential.occupied_space_all_residential
   mapit2.occupied_space_all_residential.occupied_space_all_residential_page
   mapit2.occupied_space_all_space_type.occupied_space_all_space_type
   mapit2.occupied_space_all_space_type.occupied_space_all_space_type_page
   mapit2.occupied_space_selected_space_type.occupied_space_selected_space_type
   mapit2.occupied_space_selected_space_type.occupied_space_selected_space_type_page
   mapit2.occupied_units.occupied_units
   mapit2.occupied_units.occupied_units_page
   mapit2.population_comparison_by_luz.population_comparison_by_luz
   mapit2.population_comparison_by_luz.population_comparison_by_luz_page
   mapit2.population_comparison_by_taz.population_comparison_by_taz
   mapit2.population_comparison_by_taz.population_comparison_by_taz_page
   mapit2.rents_weighted_by_space_type.rents_weighted_by_space_type
   mapit2.rents_weighted_by_space_type.rents_weighted_by_space_type_page
   mapit2.rqjob
   mapit2.rqjob.base
   mapit2.rqjob.worker_main
   mapit2.util
   mapit2.zonal_make_use_for_year_by_activity.zonal_make_use_for_year_by_activity
   mapit2.zonal_make_use_for_year_by_activity.zonal_make_use_for_year_by_activity_page
   qgs_util
   qgs_util.qgs_util_main
   qgs_util.qgsfile


Indices and tables
==================================================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
