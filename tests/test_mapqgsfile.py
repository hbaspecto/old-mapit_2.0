#
# mapit_2.0/tests/test_mapqgsfile.py ---
#

import os
import pprint
import unittest

from mapit2.models.mapqgsfile import (
    MapQgsFile,
    build_site_templates_qgs_files,
)

#####


class TestMapQgsFile(unittest.TestCase):

    def test_build_site_templates_qgs_files(self):
        """Show
        """
        data=build_site_templates_qgs_files()

        if True:
            print("build_site_templates_qgs_files:")
            pprint.pprint(data)

        self.assertTrue(isinstance(data,(dict,)))

        for (key,val) in data.items():
            self.assertTrue(isinstance(key,(int,)))
            self.assertTrue(isinstance(val,(list,)))
            self.assertTrue(val)
            # They should all be simple names (not paths.)
            for name in val:
                self.assertEqual(name,os.path.basename(name))

    def test_find_template_path_1(self):
        """Test the good cases.
        """
        mapqgsfile=MapQgsFile()

        for item in [
                "edmonton_bap_comparison.qgs",
                "ref-policy-diff.qgs",
                ]:
            path=mapqgsfile.find_template_path(item)
            if True:
                print("find_template_path: {!r} = {!r}".format(
                    item,path))
            self.assertTrue(path)

            path_2=mapqgsfile.find_template_path(None,None,item)
            self.assertEqual(path,path_2)

    def test_find_template_path_2(self):
        """Test the bad cases.
        """
        mapqgsfile=MapQgsFile()

        self.assertRaises(
            ValueError,
            mapqgsfile.find_template_path,None)

        self.assertRaises(
            ValueError,
            mapqgsfile.find_template_path,"this does not exist.qgs")
