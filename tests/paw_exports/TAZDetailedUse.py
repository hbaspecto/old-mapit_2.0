# Install the Python Requests library:
# `pip install requests`

import requests


def send_request():
    # TAZDetailedUse
    # POST http://brooks.office.hbaspecto.com:5081/mapit/queryBuilderYr/

    try:
        response = requests.post(
            url="http://brooks.office.hbaspecto.com:5081/mapit/queryBuilderYr/",
            headers={
                "Authorization": "Basic bXJzZ3VpOm1yc2d1aS0wNmZi",
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
            },
            data={
                "commodityCondition": "CL05WhiteCollar",
                "selected_file": "40",
                "startyear": "2020",
                "attribute": "amount",
                "num_scen": "one",
                "aggFunc": "sum",
                "scenario1": "W116",
                "endyear": "2030",
                "activityCondition": "AI54ProfTech",
            },
        )
        print('Response HTTP Status Code: {status_code}'.format(
            status_code=response.status_code))
        print('Response HTTP Response Body: {content}'.format(
            content=response.content))
    except requests.exceptions.RequestException:
        print('HTTP Request failed')
