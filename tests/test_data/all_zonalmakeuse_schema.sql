CREATE TABLE output.all_zonalmakeuse
(
    scenario character varying(250) COLLATE pg_catalog."default" NOT NULL,
    year_run integer NOT NULL,
    zonenumber integer NOT NULL,
    activity integer NOT NULL,
    commodity integer NOT NULL,
    moru character varying(250) COLLATE pg_catalog."default" NOT NULL,
    coefficient double precision NOT NULL,
    utility double precision NOT NULL,
    amount double precision NOT NULL
)
