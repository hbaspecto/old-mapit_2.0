--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.2

-- Started on 2020-04-22 17:36:26 MDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10248 (class 1259 OID 35702885)
-- Name: employment_temp; Type: TABLE; Schema: output; Owner: -
--

CREATE TABLE output.employment_temp (
    taz integer,
    naics integer,
    noc11 integer,
    amt double precision
);


-- Completed on 2020-04-22 17:36:43 MDT

--
-- PostgreSQL database dump complete
--

