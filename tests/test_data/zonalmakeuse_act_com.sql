
CREATE OR REPLACE VIEW output.all_zonalmakeuse_act_com
 AS
 SELECT zmu.scenario,
    zmu.year_run,
    zmu.zonenumber,
    an.activitynumber,
    an.activity,
    cn.commoditynumber,
    cn.commodity,
    zmu.moru,
    zmu.coefficient,
    zmu.utility,
    zmu.amount
   FROM output.all_zonalmakeuse zmu
     JOIN output.activity_numbers an ON zmu.activity = an.activitynumber
     JOIN output.commodity_numbers cn ON zmu.commodity = cn.commoditynumber;

