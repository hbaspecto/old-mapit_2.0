
--
-- TOC entry 3283 (class 1259 OID 11934776)
-- Name: commodity_numbers; Type: TABLE; Schema: output; Owner: -
--

CREATE TABLE output.commodity_numbers (
    commoditynumber integer NOT NULL,
    commodity character varying(250) NOT NULL,
    commodity_type_id integer,
    commodity_name_nospace text
);


--
-- TOC entry 66259 (class 0 OID 11934776)
-- Dependencies: 3283
-- Data for Name: commodity_numbers; Type: TABLE DATA; Schema: output; Owner: -
--

INSERT INTO output.commodity_numbers VALUES (268, 'G268 - Entertainment Recorded', 32, 'G268_-_Entertainment_Recorded');
INSERT INTO output.commodity_numbers VALUES (512, 'S512 - Trans Services Freight Rail', 32, 'S512_-_Trans_Services_Freight_Rail');
INSERT INTO output.commodity_numbers VALUES (513, 'S513 - Trans Services Freight Water', 32, 'S513_-_Trans_Services_Freight_Water');
INSERT INTO output.commodity_numbers VALUES (514, 'S514 - Trans Services Moving', 32, 'S514_-_Trans_Services_Moving');
INSERT INTO output.commodity_numbers VALUES (515, 'S515 - Trans Services General Freight Truck', 32, 'S515_-_Trans_Services_General_Freight_Truck');
INSERT INTO output.commodity_numbers VALUES (516, 'S516 - Trans Services Special Freight Truck', 32, 'S516_-_Trans_Services_Special_Freight_Truck');
INSERT INTO output.commodity_numbers VALUES (519, 'S519 - Sightseeing Services', 32, 'S519_-_Sightseeing_Services');
INSERT INTO output.commodity_numbers VALUES (521, 'S521 - Trans Services Natural Gas Pipeline', 32, 'S521_-_Trans_Services_Natural_Gas_Pipeline');
INSERT INTO output.commodity_numbers VALUES (522, 'S522 - Trans Services Oil Pipeline', 32, 'S522_-_Trans_Services_Oil_Pipeline');
INSERT INTO output.commodity_numbers VALUES (531, 'S531 - Trans Support Services Air', 32, 'S531_-_Trans_Support_Services_Air');
INSERT INTO output.commodity_numbers VALUES (532, 'S532 - Trans Support Services Rail', 32, 'S532_-_Trans_Support_Services_Rail');
INSERT INTO output.commodity_numbers VALUES (533, 'S533 - Trans Support Services Water', 32, 'S533_-_Trans_Support_Services_Water');
INSERT INTO output.commodity_numbers VALUES (535, 'S535 - Trans Support Services Road', 32, 'S535_-_Trans_Support_Services_Road');
INSERT INTO output.commodity_numbers VALUES (539, 'S539 - Trans Support Services Other', 32, 'S539_-_Trans_Support_Services_Other');
INSERT INTO output.commodity_numbers VALUES (631, 'S631 - Entertainment Live', 32, 'S631_-_Entertainment_Live');
INSERT INTO output.commodity_numbers VALUES (632, 'S632 - Representative Agent Services', 32, 'S632_-_Representative_Agent_Services');
INSERT INTO output.commodity_numbers VALUES (633, 'S633 - Heritage Cultural Services', 32, 'S633_-_Heritage_Cultural_Services');
INSERT INTO output.commodity_numbers VALUES (591, 'S591 - Purchase of Education K12', 32, 'S591_-_Purchase_of_Education_K12');
INSERT INTO output.commodity_numbers VALUES (592, 'S592 - Students K12', 32, 'S592_-_Students_K12');
INSERT INTO output.commodity_numbers VALUES (593, 'S593 - Government Education Grant PSE', 32, 'S593_-_Government_Education_Grant_PSE');
INSERT INTO output.commodity_numbers VALUES (621, 'S621 - Health Care Services', 32, 'S621_-_Health_Care_Services');
INSERT INTO output.commodity_numbers VALUES (950, 'F950 - Household Shopping', 31, 'F950_-_Household_Shopping');
INSERT INTO output.commodity_numbers VALUES (622, 'S622 - Residential Care Services', 32, 'S622_-_Residential_Care_Services');
INSERT INTO output.commodity_numbers VALUES (623, 'S623 - Social Assistance Services', 32, 'S623_-_Social_Assistance_Services');
INSERT INTO output.commodity_numbers VALUES (626, 'S626 - Daycare Services', 32, 'S626_-_Daycare_Services');
INSERT INTO output.commodity_numbers VALUES (541, 'S541 - Postal Services', 32, 'S541_-_Postal_Services');
INSERT INTO output.commodity_numbers VALUES (542, 'S542 - Courier Services', 32, 'S542_-_Courier_Services');
INSERT INTO output.commodity_numbers VALUES (544, 'S544 - Warehousing Services', 32, 'S544_-_Warehousing_Services');
INSERT INTO output.commodity_numbers VALUES (545, 'S545 - Advertising', 32, 'S545_-_Advertising');
INSERT INTO output.commodity_numbers VALUES (546, 'S546 - Information Handling Services', 32, 'S546_-_Information_Handling_Services');
INSERT INTO output.commodity_numbers VALUES (551, 'S551 - Residential Space Rental', 32, 'S551_-_Residential_Space_Rental');
INSERT INTO output.commodity_numbers VALUES (552, 'S552 - NonResidential Space Rental', 32, 'S552_-_NonResidential_Space_Rental');
INSERT INTO output.commodity_numbers VALUES (555, 'S555 - Real Estate Support Services', 32, 'S555_-_Real_Estate_Support_Services');
INSERT INTO output.commodity_numbers VALUES (561, 'S561 - Motor Vehicle Rental', 32, 'S561_-_Motor_Vehicle_Rental');
INSERT INTO output.commodity_numbers VALUES (562, 'S562 - Furnishings Household Rental', 32, 'S562_-_Furnishings_Household_Rental');
INSERT INTO output.commodity_numbers VALUES (563, 'S563 - Furnishings Other Rental', 32, 'S563_-_Furnishings_Other_Rental');
INSERT INTO output.commodity_numbers VALUES (565, 'S565 - Commercial Machinery Rental', 32, 'S565_-_Commercial_Machinery_Rental');
INSERT INTO output.commodity_numbers VALUES (568, 'S568 - Brand Rental', 32, 'S568_-_Brand_Rental');
INSERT INTO output.commodity_numbers VALUES (919, 'S919 - Repair construction services', 32, 'S919_-_Repair_construction_services');
INSERT INTO output.commodity_numbers VALUES (956, 'F956 - Financial Services', 32, 'F956_-_Financial_Services');
INSERT INTO output.commodity_numbers VALUES (581, 'S581 - Brand Rental', 32, 'S581_-_Brand_Rental');
INSERT INTO output.commodity_numbers VALUES (711, 'S711 - Repair Services Motor Vehicles', 32, 'S711_-_Repair_Services_Motor_Vehicles');
INSERT INTO output.commodity_numbers VALUES (712, 'S712 - Repair Services Other Than Motor Vehicles', 32, 'S712_-_Repair_Services_Other_Than_Motor_Vehicles');
INSERT INTO output.commodity_numbers VALUES (752, 'S752 - Funeral Services', 32, 'S752_-_Funeral_Services');
INSERT INTO output.commodity_numbers VALUES (811, 'S811 - Personal Care Services', 32, 'S811_-_Personal_Care_Services');
INSERT INTO output.commodity_numbers VALUES (812, 'S812 - Motor Vehicle Parking Services', 32, 'S812_-_Motor_Vehicle_Parking_Services');
INSERT INTO output.commodity_numbers VALUES (813, 'S813 - Membership Services', 32, 'S813_-_Membership_Services');
INSERT INTO output.commodity_numbers VALUES (814, 'S814 - Babysitting Services', 32, 'S814_-_Babysitting_Services');
INSERT INTO output.commodity_numbers VALUES (815, 'S815 - Religious Services', 32, 'S815_-_Religious_Services');
INSERT INTO output.commodity_numbers VALUES (590, 'S590 - Government Education Grant K12', 33, 'S590_-_Government_Education_Grant_K12');
INSERT INTO output.commodity_numbers VALUES (594, 'S594 - Purchase of Education PSE', 33, 'S594_-_Purchase_of_Education_PSE');
INSERT INTO output.commodity_numbers VALUES (596, 'S596 - Government Education Grant Other', 33, 'S596_-_Government_Education_Grant_Other');
INSERT INTO output.commodity_numbers VALUES (597, 'S597 - Education Other', 33, 'S597_-_Education_Other');
INSERT INTO output.commodity_numbers VALUES (595, 'S595 - Students PSE', 36, 'S595_-_Students_PSE');
INSERT INTO output.commodity_numbers VALUES (598, 'S598 - Students Other', 36, 'S598_-_Students_Other');
INSERT INTO output.commodity_numbers VALUES (816, 'S624 - RC Patients', 36, 'S624_-_RC_Patients');
INSERT INTO output.commodity_numbers VALUES (202, 'G202 - Rubber', 26, 'G202_-_Rubber');
INSERT INTO output.commodity_numbers VALUES (206, 'G206 - Hoses Belts Rubber Plastic', 26, 'G206_-_Hoses_Belts_Rubber_Plastic');
INSERT INTO output.commodity_numbers VALUES (207, 'G207 - Tires', 26, 'G207_-_Tires');
INSERT INTO output.commodity_numbers VALUES (208, 'G208 - Rubber Products', 26, 'G208_-_Rubber_Products');
INSERT INTO output.commodity_numbers VALUES (221, 'G221 - Plastic Primary', 26, 'G221_-_Plastic_Primary');
INSERT INTO output.commodity_numbers VALUES (222, 'G222 - Synthetic Fibres', 26, 'G222_-_Synthetic_Fibres');
INSERT INTO output.commodity_numbers VALUES (241, 'C241 - Lumber', 26, 'C241_-_Lumber');
INSERT INTO output.commodity_numbers VALUES (501, 'S501 - Trans Services Passenger Air', 32, 'S501_-_Trans_Services_Passenger_Air');
INSERT INTO output.commodity_numbers VALUES (491, 'S491 - Water Supply Services', 32, 'S491_-_Water_Supply_Services');
INSERT INTO output.commodity_numbers VALUES (492, 'S492 - HVAC Supply Services', 32, 'S492_-_HVAC_Supply_Services');
INSERT INTO output.commodity_numbers VALUES (495, 'S495 - Metal Processing Services', 32, 'S495_-_Metal_Processing_Services');
INSERT INTO output.commodity_numbers VALUES (496, 'S496 - Typeset Printing', 32, 'S496_-_Typeset_Printing');
INSERT INTO output.commodity_numbers VALUES (900, 'B900 - Residential buildings', 33, 'B900_-_Residential_buildings');
INSERT INTO output.commodity_numbers VALUES (901, 'B901 - Industrial buildings', 33, 'B901_-_Industrial_buildings');
INSERT INTO output.commodity_numbers VALUES (227, 'G227 - Plastic Products', 26, 'G227_-_Plastic_Products');
INSERT INTO output.commodity_numbers VALUES (902, 'B902 - Office buildings', 33, 'B902_-_Office_buildings');
INSERT INTO output.commodity_numbers VALUES (903, 'B903 - Shopping centers', 33, 'B903_-_Shopping_centers');
INSERT INTO output.commodity_numbers VALUES (904, 'B904 - Other commercial buildings', 33, 'B904_-_Other_commercial_buildings');
INSERT INTO output.commodity_numbers VALUES (5002, 'XCropProd_Buildings', 33, 'XCropProd_Buildings');
INSERT INTO output.commodity_numbers VALUES (5003, 'XCropProd_ME', 33, 'XCropProd_ME');
INSERT INTO output.commodity_numbers VALUES (5004, 'XConvExt_Active', 33, 'XConvExt_Active');
INSERT INTO output.commodity_numbers VALUES (5005, 'XConvExt_Infrastructure', 33, 'XConvExt_Infrastructure');
INSERT INTO output.commodity_numbers VALUES (5006, 'XConvExt_ME', 33, 'XConvExt_ME');
INSERT INTO output.commodity_numbers VALUES (5007, 'XNonConvExt_Active', 33, 'XNonConvExt_Active');
INSERT INTO output.commodity_numbers VALUES (5008, 'XNonConvExt_Infrastructure', 33, 'XNonConvExt_Infrastructure');
INSERT INTO output.commodity_numbers VALUES (5009, 'XNonConvExt_ME', 33, 'XNonConvExt_ME');
INSERT INTO output.commodity_numbers VALUES (5010, 'XChemMan_Active', 33, 'XChemMan_Active');
INSERT INTO output.commodity_numbers VALUES (5011, 'XChemMan_Infrastructure', 33, 'XChemMan_Infrastructure');
INSERT INTO output.commodity_numbers VALUES (5012, 'XChemMan_ME', 33, 'XChemMan_ME');
INSERT INTO output.commodity_numbers VALUES (905, 'B905 - Schools universities', 33, 'B905_-_Schools_universities');
INSERT INTO output.commodity_numbers VALUES (951, 'F951 - Retail margins', 33, 'F951_-_Retail_margins');
INSERT INTO output.commodity_numbers VALUES (952, 'F952 - Retail trade commissions', 33, 'F952_-_Retail_trade_commissions');
INSERT INTO output.commodity_numbers VALUES (953, 'F953 - Transportation margins', 33, 'F953_-_Transportation_margins');
INSERT INTO output.commodity_numbers VALUES (954, 'F954 - Wholesale margins', 33, 'F954_-_Wholesale_margins');
INSERT INTO output.commodity_numbers VALUES (955, 'F955 - Wholesale trade commissions', 33, 'F955_-_Wholesale_trade_commissions');
INSERT INTO output.commodity_numbers VALUES (2001, 'D01 - Agriculture and Forestry Office Actions', 33, 'D01_-_Agriculture_and_Forestry_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (2002, 'D02 - Oil and Gas Office Actions', 33, 'D02_-_Oil_and_Gas_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (2003, 'D03 - Mining and Extraction Office Actions', 33, 'D03_-_Mining_and_Extraction_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (2004, 'D04 - Utilities Office Actions', 33, 'D04_-_Utilities_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (2005, 'D05 - Construction Office Actions', 33, 'D05_-_Construction_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (2006, 'D06 - Food Manufacturing Office Actions', 33, 'D06_-_Food_Manufacturing_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (2007, 'D07 - Fabrics and Paper Office Actions', 33, 'D07_-_Fabrics_and_Paper_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (2008, 'D08 - Energy Manufacturing Office Actions', 33, 'D08_-_Energy_Manufacturing_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (1001, 'L01 - ManagerSenior', 34, 'L01_-_ManagerSenior');
INSERT INTO output.commodity_numbers VALUES (1002, 'L02 - ManagerMiddle', 34, 'L02_-_ManagerMiddle');
INSERT INTO output.commodity_numbers VALUES (1003, 'L03 - AnalystProfessional', 34, 'L03_-_AnalystProfessional');
INSERT INTO output.commodity_numbers VALUES (1004, 'L04 - AnalystTech', 34, 'L04_-_AnalystTech');
INSERT INTO output.commodity_numbers VALUES (1005, 'L05 - Administrator', 34, 'L05_-_Administrator');
INSERT INTO output.commodity_numbers VALUES (1006, 'L06 - Clerical', 34, 'L06_-_Clerical');
INSERT INTO output.commodity_numbers VALUES (1007, 'L07 - HealthProfessional', 34, 'L07_-_HealthProfessional');
INSERT INTO output.commodity_numbers VALUES (1008, 'L08 - HealthTech', 34, 'L08_-_HealthTech');
INSERT INTO output.commodity_numbers VALUES (1009, 'L09 - SocialProfessional', 34, 'L09_-_SocialProfessional');
INSERT INTO output.commodity_numbers VALUES (1010, 'L10 - SocialTech', 34, 'L10_-_SocialTech');
INSERT INTO output.commodity_numbers VALUES (1011, 'L11 - Instructor', 34, 'L11_-_Instructor');
INSERT INTO output.commodity_numbers VALUES (1012, 'L12 - Performer', 34, 'L12_-_Performer');
INSERT INTO output.commodity_numbers VALUES (1013, 'L13 - RetailSalesService', 34, 'L13_-_RetailSalesService');
INSERT INTO output.commodity_numbers VALUES (1014, 'L14 - Trades', 34, 'L14_-_Trades');
INSERT INTO output.commodity_numbers VALUES (1015, 'L15 - Driver', 34, 'L15_-_Driver');
INSERT INTO output.commodity_numbers VALUES (1016, 'L16 - Labourer', 34, 'L16_-_Labourer');
INSERT INTO output.commodity_numbers VALUES (1017, 'L17 - NatResTech', 34, 'L17_-_NatResTech');
INSERT INTO output.commodity_numbers VALUES (1018, 'L18 - ProcessingTech', 34, 'L18_-_ProcessingTech');
INSERT INTO output.commodity_numbers VALUES (231, 'C231 - Logs Pulpwood', 26, 'C231_-_Logs_Pulpwood');
INSERT INTO output.commodity_numbers VALUES (232, 'C232 - Logs Lumber', 26, 'C232_-_Logs_Lumber');
INSERT INTO output.commodity_numbers VALUES (233, 'C233 - Fuel Wood', 26, 'C233_-_Fuel_Wood');
INSERT INTO output.commodity_numbers VALUES (235, 'C235 - Wood Untreated', 26, 'C235_-_Wood_Untreated');
INSERT INTO output.commodity_numbers VALUES (236, 'C236 - Wood Chips', 26, 'C236_-_Wood_Chips');
INSERT INTO output.commodity_numbers VALUES (242, 'C242 - Wood Products Treated', 26, 'C242_-_Wood_Products_Treated');
INSERT INTO output.commodity_numbers VALUES (243, 'C243 - Wood Window Door', 26, 'C243_-_Wood_Window_Door');
INSERT INTO output.commodity_numbers VALUES (245, 'C245 - Plywood', 26, 'C245_-_Plywood');
INSERT INTO output.commodity_numbers VALUES (101, 'C101 - Stone', 15, 'C101_-_Stone');
INSERT INTO output.commodity_numbers VALUES (102, 'C102 - Sand Gravel', 15, 'C102_-_Sand_Gravel');
INSERT INTO output.commodity_numbers VALUES (103, 'C103 - NonMetallic Minerals', 15, 'C103_-_NonMetallic_Minerals');
INSERT INTO output.commodity_numbers VALUES (107, 'C107 - Basic Inorganic Chemicals', 16, 'C107_-_Basic_Inorganic_Chemicals');
INSERT INTO output.commodity_numbers VALUES (141, 'C141 - Dyes and Pigments', 16, 'C141_-_Dyes_and_Pigments');
INSERT INTO output.commodity_numbers VALUES (152, 'C152 - Ammonia and Chemical Fertilizer', 16, 'C152_-_Ammonia_and_Chemical_Fertilizer');
INSERT INTO output.commodity_numbers VALUES (167, 'C167 - Basic Organic Chemicals', 16, 'C167_-_Basic_Organic_Chemicals');
INSERT INTO output.commodity_numbers VALUES (169, 'G169 - Other Chemical Products', 16, 'G169_-_Other_Chemical_Products');
INSERT INTO output.commodity_numbers VALUES (171, 'G171 - Perfumery', 16, 'G171_-_Perfumery');
INSERT INTO output.commodity_numbers VALUES (172, 'G172 - Soaps', 16, 'G172_-_Soaps');
INSERT INTO output.commodity_numbers VALUES (173, 'G173 - Insecticides', 16, 'G173_-_Insecticides');
INSERT INTO output.commodity_numbers VALUES (51, 'C051 - Frozen Meat', 18, 'C051_-_Frozen_Meat');
INSERT INTO output.commodity_numbers VALUES (52, 'C052 - Frozen Poultry', 18, 'C052_-_Frozen_Poultry');
INSERT INTO output.commodity_numbers VALUES (53, 'C053 - Fish', 18, 'C053_-_Fish');
INSERT INTO output.commodity_numbers VALUES (54, 'C054 - Processed Meat', 18, 'C054_-_Processed_Meat');
INSERT INTO output.commodity_numbers VALUES (55, 'C055 - Seafood', 18, 'C055_-_Seafood');
INSERT INTO output.commodity_numbers VALUES (61, 'C061 - Flour', 18, 'C061_-_Flour');
INSERT INTO output.commodity_numbers VALUES (62, 'C062 - Margarine and Cooking Oils', 18, 'C062_-_Margarine_and_Cooking_Oils');
INSERT INTO output.commodity_numbers VALUES (63, 'C063 - Other Grain and Oilseed Products', 18, 'C063_-_Other_Grain_and_Oilseed_Products');
INSERT INTO output.commodity_numbers VALUES (64, 'C064 - Baked Products', 18, 'C064_-_Baked_Products');
INSERT INTO output.commodity_numbers VALUES (65, 'C065 - Snack Foods', 18, 'C065_-_Snack_Foods');
INSERT INTO output.commodity_numbers VALUES (66, 'C066 - Other Food Products', 18, 'C066_-_Other_Food_Products');
INSERT INTO output.commodity_numbers VALUES (108, 'C108 - Industrial Gases', 23, 'C108_-_Industrial_Gases');
INSERT INTO output.commodity_numbers VALUES (111, 'C111 - Coal', 23, 'C111_-_Coal');
INSERT INTO output.commodity_numbers VALUES (112, 'C112 - Conventional and Synthetic Crude Oil and Bitumen', 23, 'C112_-_Conventional_and_Synthetic_Crude_Oil_and_Bitumen');
INSERT INTO output.commodity_numbers VALUES (119, 'C119 - Electricity', 23, 'C119_-_Electricity');
INSERT INTO output.commodity_numbers VALUES (121, 'C121 - Gasoline', 23, 'C121_-_Gasoline');
INSERT INTO output.commodity_numbers VALUES (122, 'C122 - Fuel Oils Diesel Biodiesel', 23, 'C122_-_Fuel_Oils_Diesel_Biodiesel');
INSERT INTO output.commodity_numbers VALUES (123, 'C123 - Jet Fuel', 23, 'C123_-_Jet_Fuel');
INSERT INTO output.commodity_numbers VALUES (124, 'C124 - Petrochemicals', 23, 'C124_-_Petrochemicals');
INSERT INTO output.commodity_numbers VALUES (131, 'C131 - Natural Gas', 23, 'C131_-_Natural_Gas');
INSERT INTO output.commodity_numbers VALUES (132, 'C132 - Natural Gas Liquids', 23, 'C132_-_Natural_Gas_Liquids');
INSERT INTO output.commodity_numbers VALUES (135, 'C135 - Coke', 23, 'C135_-_Coke');
INSERT INTO output.commodity_numbers VALUES (137, 'C137 - Asphalt', 23, 'C137_-_Asphalt');
INSERT INTO output.commodity_numbers VALUES (267, 'G267 - Software', 1, 'G267_-_Software');
INSERT INTO output.commodity_numbers VALUES (252, 'C252 - Newsprint', 25, 'C252_-_Newsprint');
INSERT INTO output.commodity_numbers VALUES (253, 'C253 - Paper', 25, 'C253_-_Paper');
INSERT INTO output.commodity_numbers VALUES (254, 'C254 - Paperboard', 25, 'C254_-_Paperboard');
INSERT INTO output.commodity_numbers VALUES (255, 'G255 - Paperboard Containers', 25, 'G255_-_Paperboard_Containers');
INSERT INTO output.commodity_numbers VALUES (244, 'C244 - Wood Products Millwork', 26, 'C244_-_Wood_Products_Millwork');
INSERT INTO output.commodity_numbers VALUES (246, 'C246 - Wood Products Reconstituted', 26, 'C246_-_Wood_Products_Reconstituted');
INSERT INTO output.commodity_numbers VALUES (247, 'C247 - Wood Crates and Pallets', 26, 'C247_-_Wood_Crates_and_Pallets');
INSERT INTO output.commodity_numbers VALUES (248, 'C248 - Other Engineered Wood Products', 26, 'C248_-_Other_Engineered_Wood_Products');
INSERT INTO output.commodity_numbers VALUES (251, 'C251 - Wood Pulp', 26, 'C251_-_Wood_Pulp');
INSERT INTO output.commodity_numbers VALUES (275, 'C275 - Fibre Yarn Thread', 25, 'C275_-_Fibre_Yarn_Thread');
INSERT INTO output.commodity_numbers VALUES (48, 'C048 - Fruit and Vegetables Processed', 18, 'C048_-_Fruit_and_Vegetables_Processed');
INSERT INTO output.commodity_numbers VALUES (49, 'C049 - Other Crop Products', 18, 'C049_-_Other_Crop_Products');
INSERT INTO output.commodity_numbers VALUES (11, 'C011 - Bovine', 19, 'C011_-_Bovine');
INSERT INTO output.commodity_numbers VALUES (12, 'C012 - Swine', 19, 'C012_-_Swine');
INSERT INTO output.commodity_numbers VALUES (13, 'C013 - Poultry', 19, 'C013_-_Poultry');
INSERT INTO output.commodity_numbers VALUES (15, 'C015 - Other Live Animals', 19, 'C015_-_Other_Live_Animals');
INSERT INTO output.commodity_numbers VALUES (21, 'C021 - Wheat', 19, 'C021_-_Wheat');
INSERT INTO output.commodity_numbers VALUES (22, 'C022 - Grains', 19, 'C022_-_Grains');
INSERT INTO output.commodity_numbers VALUES (31, 'C031 - Potatoes', 19, 'C031_-_Potatoes');
INSERT INTO output.commodity_numbers VALUES (32, 'C032 - Vegetables', 19, 'C032_-_Vegetables');
INSERT INTO output.commodity_numbers VALUES (33, 'C033 - Fruits and Nuts', 19, 'C033_-_Fruits_and_Nuts');
INSERT INTO output.commodity_numbers VALUES (35, 'C035 - Oilseeds', 19, 'C035_-_Oilseeds');
INSERT INTO output.commodity_numbers VALUES (36, 'C036 - Floriculture', 19, 'C036_-_Floriculture');
INSERT INTO output.commodity_numbers VALUES (41, 'C041 - Eggs', 19, 'C041_-_Eggs');
INSERT INTO output.commodity_numbers VALUES (333, 'E333 - Commercial Machinery', 20, 'E333_-_Commercial_Machinery');
INSERT INTO output.commodity_numbers VALUES (336, 'E336 - Household Appliances', 20, 'E336_-_Household_Appliances');
INSERT INTO output.commodity_numbers VALUES (339, 'E339 - Prefabricated Buildings', 20, 'E339_-_Prefabricated_Buildings');
INSERT INTO output.commodity_numbers VALUES (341, 'E341 - Electric Motors', 20, 'E341_-_Electric_Motors');
INSERT INTO output.commodity_numbers VALUES (3511, 'R011 - Detached Economy', 9, 'R011_-_Detached_Economy');
INSERT INTO output.commodity_numbers VALUES (3512, 'R012 - Detached Luxury', 9, 'R012_-_Detached_Luxury');
INSERT INTO output.commodity_numbers VALUES (3520, 'R020 - Attached', 9, 'R020_-_Attached');
INSERT INTO output.commodity_numbers VALUES (3531, 'R031 - Multifamily Economy', 9, 'R031_-_Multifamily_Economy');
INSERT INTO output.commodity_numbers VALUES (3532, 'R032 - Multifamily Luxury', 9, 'R032_-_Multifamily_Luxury');
INSERT INTO output.commodity_numbers VALUES (3040, 'R040 - Mobile', 9, 'R040_-_Mobile');
INSERT INTO output.commodity_numbers VALUES (256, 'G256 - Paper Converted Products', 25, 'G256_-_Paper_Converted_Products');
INSERT INTO output.commodity_numbers VALUES (257, 'G257 - Stationery Products', 25, 'G257_-_Stationery_Products');
INSERT INTO output.commodity_numbers VALUES (258, 'G258 - Sanitary Paper', 25, 'G258_-_Sanitary_Paper');
INSERT INTO output.commodity_numbers VALUES (259, 'G259 - Paper Printed', 25, 'G259_-_Paper_Printed');
INSERT INTO output.commodity_numbers VALUES (261, 'G261 - Newspapers', 25, 'G261_-_Newspapers');
INSERT INTO output.commodity_numbers VALUES (262, 'G262 - Books', 25, 'G262_-_Books');
INSERT INTO output.commodity_numbers VALUES (263, 'G263 - Periodicals', 25, 'G263_-_Periodicals');
INSERT INTO output.commodity_numbers VALUES (264, 'G264 - Flyers Catalogues', 25, 'G264_-_Flyers_Catalogues');
INSERT INTO output.commodity_numbers VALUES (269, 'G269 - Stationery Supplies', 25, 'G269_-_Stationery_Supplies');
INSERT INTO output.commodity_numbers VALUES (271, 'G271 - Fabrics', 25, 'G271_-_Fabrics');
INSERT INTO output.commodity_numbers VALUES (342, 'E342 - Electrical Equipment', 20, 'E342_-_Electrical_Equipment');
INSERT INTO output.commodity_numbers VALUES (343, 'E343 - Batteries', 20, 'E343_-_Batteries');
INSERT INTO output.commodity_numbers VALUES (345, 'E345 - Light Bulbs', 20, 'E345_-_Light_Bulbs');
INSERT INTO output.commodity_numbers VALUES (346, 'E346 - Circuit Components', 20, 'E346_-_Circuit_Components');
INSERT INTO output.commodity_numbers VALUES (347, 'E347 - Computers', 20, 'E347_-_Computers');
INSERT INTO output.commodity_numbers VALUES (348, 'E348 - Consumer Electronics', 20, 'E348_-_Consumer_Electronics');
INSERT INTO output.commodity_numbers VALUES (349, 'E349 - Blank Recording Media', 20, 'E349_-_Blank_Recording_Media');
INSERT INTO output.commodity_numbers VALUES (351, 'V351 - Motor Vehicle Parts', 20, 'V351_-_Motor_Vehicle_Parts');
INSERT INTO output.commodity_numbers VALUES (352, 'V352 - Trucks', 20, 'V352_-_Trucks');
INSERT INTO output.commodity_numbers VALUES (42, 'C042 - Raw Hides Silk', 24, 'C042_-_Raw_Hides_Silk');
INSERT INTO output.commodity_numbers VALUES (43, 'C043 - Pet food', 24, 'C043_-_Pet_food');
INSERT INTO output.commodity_numbers VALUES (44, 'C044 - Other Animal Feed', 24, 'C044_-_Other_Animal_Feed');
INSERT INTO output.commodity_numbers VALUES (454, 'W454 - Scrap Paper', 1, 'W454_-_Scrap_Paper');
INSERT INTO output.commodity_numbers VALUES (455, 'W455 - Scrap Glass', 1, 'W455_-_Scrap_Glass');
INSERT INTO output.commodity_numbers VALUES (467, 'G467 - Other Miscellaneous Goods', 1, 'G467_-_Other_Miscellaneous_Goods');
INSERT INTO output.commodity_numbers VALUES (272, 'G272 - Clothing', 25, 'G272_-_Clothing');
INSERT INTO output.commodity_numbers VALUES (273, 'G273 - Carpets Rugs', 25, 'G273_-_Carpets_Rugs');
INSERT INTO output.commodity_numbers VALUES (274, 'G274 - Other Textile Furnishings', 25, 'G274_-_Other_Textile_Furnishings');
INSERT INTO output.commodity_numbers VALUES (276, 'G276 - Footwear', 25, 'G276_-_Footwear');
INSERT INTO output.commodity_numbers VALUES (277, 'G277 - Leather and Furs', 25, 'G277_-_Leather_and_Furs');
INSERT INTO output.commodity_numbers VALUES (278, 'G278 - Leather Products', 25, 'G278_-_Leather_Products');
INSERT INTO output.commodity_numbers VALUES (279, 'G279 - Other Textile Products', 25, 'G279_-_Other_Textile_Products');
INSERT INTO output.commodity_numbers VALUES (81, 'C081 - Beer', 13, 'C081_-_Beer');
INSERT INTO output.commodity_numbers VALUES (82, 'C082 - Wine', 13, 'C082_-_Wine');
INSERT INTO output.commodity_numbers VALUES (83, 'C083 - Distilled Liquor', 13, 'C083_-_Distilled_Liquor');
INSERT INTO output.commodity_numbers VALUES (91, 'G091 - Tobacco Products', 13, 'G091_-_Tobacco_Products');
INSERT INTO output.commodity_numbers VALUES (105, 'C105 - Other Metal Ores', 15, 'C105_-_Other_Metal_Ores');
INSERT INTO output.commodity_numbers VALUES (4001, 'N001 - Crop Space', 37, 'N001_-_Crop_Space');
INSERT INTO output.commodity_numbers VALUES (4002, 'N002 - Range Space', 37, 'N002_-_Range_Space');
INSERT INTO output.commodity_numbers VALUES (4003, 'N003 - Extraction Space', 38, 'N003_-_Extraction_Space');
INSERT INTO output.commodity_numbers VALUES (4004, 'N004 - Heavy Industry Space', 38, 'N004_-_Heavy_Industry_Space');
INSERT INTO output.commodity_numbers VALUES (4005, 'N005 - Light Industry Space', 38, 'N005_-_Light_Industry_Space');
INSERT INTO output.commodity_numbers VALUES (4006, 'N006 - Warehouse and Depot Space', 38, 'N006_-_Warehouse_and_Depot_Space');
INSERT INTO output.commodity_numbers VALUES (4007, 'N007 - Office Space', 39, 'N007_-_Office_Space');
INSERT INTO output.commodity_numbers VALUES (4008, 'N008 - Retail Space', 39, 'N008_-_Retail_Space');
INSERT INTO output.commodity_numbers VALUES (4009, 'N009 - Hotel Space', 39, 'N009_-_Hotel_Space');
INSERT INTO output.commodity_numbers VALUES (4010, 'N010 - Primary School Space', 40, 'N010_-_Primary_School_Space');
INSERT INTO output.commodity_numbers VALUES (636, 'S636 - Hotel Services', 32, 'S636_-_Hotel_Services');
INSERT INTO output.commodity_numbers VALUES (637, 'S637 - Recreational Camp Services', 32, 'S637_-_Recreational_Camp_Services');
INSERT INTO output.commodity_numbers VALUES (638, 'S638 - Food and Drink Services', 32, 'S638_-_Food_and_Drink_Services');
INSERT INTO output.commodity_numbers VALUES (634, 'S634 - Gambling', 32, 'S634_-_Gambling');
INSERT INTO output.commodity_numbers VALUES (635, 'S635 - Amusement and Recreation Services', 32, 'S635_-_Amusement_and_Recreation_Services');
INSERT INTO output.commodity_numbers VALUES (571, 'S571 - Professional Technical Services', 32, 'S571_-_Professional_Technical_Services');
INSERT INTO output.commodity_numbers VALUES (572, 'S572 - Travel Agent Services', 32, 'S572_-_Travel_Agent_Services');
INSERT INTO output.commodity_numbers VALUES (573, 'S573 - Office Administration Services', 32, 'S573_-_Office_Administration_Services');
INSERT INTO output.commodity_numbers VALUES (574, 'S574 - Employment Services', 32, 'S574_-_Employment_Services');
INSERT INTO output.commodity_numbers VALUES (575, 'S575 - Business Support Services', 32, 'S575_-_Business_Support_Services');
INSERT INTO output.commodity_numbers VALUES (576, 'S576 - Building Support Services', 32, 'S576_-_Building_Support_Services');
INSERT INTO output.commodity_numbers VALUES (577, 'S577 - Security Services', 32, 'S577_-_Security_Services');
INSERT INTO output.commodity_numbers VALUES (578, 'S578 - Waste Management Services', 32, 'S578_-_Waste_Management_Services');
INSERT INTO output.commodity_numbers VALUES (906, 'B906 - Health care buildings', 33, 'B906_-_Health_care_buildings');
INSERT INTO output.commodity_numbers VALUES (907, 'B907 - Other institutional buildings', 33, 'B907_-_Other_institutional_buildings');
INSERT INTO output.commodity_numbers VALUES (908, 'B908 - Highways roads', 33, 'B908_-_Highways_roads');
INSERT INTO output.commodity_numbers VALUES (909, 'B909 - Other transportation construction', 33, 'B909_-_Other_transportation_construction');
INSERT INTO output.commodity_numbers VALUES (910, 'B910 - Production facilities in oil and gas extraction', 33, 'B910_-_Production_facilities_in_oil_and_gas_extraction');
INSERT INTO output.commodity_numbers VALUES (911, 'B911 - Other oil and gas engineering construction', 33, 'B911_-_Other_oil_and_gas_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (912, 'B912 - Electric power engineering construction', 33, 'B912_-_Electric_power_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (913, 'B913 - Communication engineering construction', 33, 'B913_-_Communication_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (914, 'B914 - Marine engineering construction', 33, 'B914_-_Marine_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (915, 'B915 - Waterworks engineering construction', 33, 'B915_-_Waterworks_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (916, 'B916 - Sewage engineering construction', 33, 'B916_-_Sewage_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (917, 'B917 - Mining engineering construction', 33, 'B917_-_Mining_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (918, 'B918 - Other engineering construction', 33, 'B918_-_Other_engineering_construction');
INSERT INTO output.commodity_numbers VALUES (2009, 'D09 - Chemical Manufacturing Office Actions', 33, 'D09_-_Chemical_Manufacturing_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3010, 'D10 - Machinery and Equipment Manufacturing Office Actions', 33, 'D10_-_Machinery_and_Equipment_Manufacturing_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3011, 'D11 - Wholesale Office Actions', 33, 'D11_-_Wholesale_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3012, 'D12 - Retail Office Actions', 33, 'D12_-_Retail_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3013, 'D13 - Transportation Office Actions', 33, 'D13_-_Transportation_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3014, 'D14 - Pipeline Office Actions', 33, 'D14_-_Pipeline_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3015, 'D15 - Entertainment Office Actions', 33, 'D15_-_Entertainment_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3016, 'D16 - Information and Publishing Office Actions', 33, 'D16_-_Information_and_Publishing_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (3017, 'D17 - Facilities Management Office Actions', 33, 'D17_-_Facilities_Management_Office_Actions');
INSERT INTO output.commodity_numbers VALUES (4011, 'N011 - Post Secondary Space', 40, 'N011_-_Post_Secondary_Space');
INSERT INTO output.commodity_numbers VALUES (4012, 'N012 - Health Space', 40, 'N012_-_Health_Space');
INSERT INTO output.commodity_numbers VALUES (106, 'C106 - Bauxite and Aluminum Oxide', 15, 'C106_-_Bauxite_and_Aluminum_Oxide');
INSERT INTO output.commodity_numbers VALUES (143, 'C143 - Earth metals', 15, 'C143_-_Earth_metals');
INSERT INTO output.commodity_numbers VALUES (151, 'C151 - Potash', 15, 'C151_-_Potash');
INSERT INTO output.commodity_numbers VALUES (316, 'E316 - Metal Containers Less300L', 15, 'E316_-_Metal_Containers_Less300L');
INSERT INTO output.commodity_numbers VALUES (321, 'E321 - Turbines', 15, 'E321_-_Turbines');
INSERT INTO output.commodity_numbers VALUES (281, 'C281 - Cement', 15, 'C281_-_Cement');
INSERT INTO output.commodity_numbers VALUES (282, 'G282 - Clay Products and Refractories', 15, 'G282_-_Clay_Products_and_Refractories');
INSERT INTO output.commodity_numbers VALUES (283, 'G283 - Lime Gypsum Products', 15, 'G283_-_Lime_Gypsum_Products');
INSERT INTO output.commodity_numbers VALUES (284, 'G284 - Concrete Products', 15, 'G284_-_Concrete_Products');
INSERT INTO output.commodity_numbers VALUES (285, 'G285 - Glass and Glass Products', 15, 'G285_-_Glass_and_Glass_Products');
INSERT INTO output.commodity_numbers VALUES (286, 'G286 - NonMetallic Mineral Products', 15, 'G286_-_NonMetallic_Mineral_Products');
INSERT INTO output.commodity_numbers VALUES (287, 'G287 - ReadyMixed Concrete', 15, 'G287_-_ReadyMixed_Concrete');
INSERT INTO output.commodity_numbers VALUES (291, 'G291 - Iron Steel Basic Shapes', 15, 'G291_-_Iron_Steel_Basic_Shapes');
INSERT INTO output.commodity_numbers VALUES (292, 'G292 - Steel Rolled Drawn', 15, 'G292_-_Steel_Rolled_Drawn');
INSERT INTO output.commodity_numbers VALUES (293, 'G293 - Iron Steel Pipes Tubes', 15, 'G293_-_Iron_Steel_Pipes_Tubes');
INSERT INTO output.commodity_numbers VALUES (301, 'G301 - Copper Unshaped', 15, 'G301_-_Copper_Unshaped');
INSERT INTO output.commodity_numbers VALUES (302, 'G302 - Nickel Shaped and Unshaped', 15, 'G302_-_Nickel_Shaped_and_Unshaped');
INSERT INTO output.commodity_numbers VALUES (303, 'G303 - Aluminum Unshaped', 15, 'G303_-_Aluminum_Unshaped');
INSERT INTO output.commodity_numbers VALUES (304, 'G304 - Aluminum Bars', 15, 'G304_-_Aluminum_Bars');
INSERT INTO output.commodity_numbers VALUES (305, 'G305 - Other NonFerrous', 15, 'G305_-_Other_NonFerrous');
INSERT INTO output.commodity_numbers VALUES (311, 'G311 - Metal Fittings', 15, 'G311_-_Metal_Fittings');
INSERT INTO output.commodity_numbers VALUES (312, 'G312 - Metal Structural Parts', 15, 'G312_-_Metal_Structural_Parts');
INSERT INTO output.commodity_numbers VALUES (313, 'E313 - Metal Doors Windows', 15, 'E313_-_Metal_Doors_Windows');
INSERT INTO output.commodity_numbers VALUES (314, 'E314 - Metal Springs', 15, 'E314_-_Metal_Springs');
INSERT INTO output.commodity_numbers VALUES (315, 'E315 - Hardware', 15, 'E315_-_Hardware');
INSERT INTO output.commodity_numbers VALUES (317, 'E317 - Metal Containers More300L', 15, 'E317_-_Metal_Containers_More300L');
INSERT INTO output.commodity_numbers VALUES (318, 'E318 - Ball and Roller Bearings', 15, 'E318_-_Ball_and_Roller_Bearings');
INSERT INTO output.commodity_numbers VALUES (319, 'G319 - Worked Metal Products', 15, 'G319_-_Worked_Metal_Products');
INSERT INTO output.commodity_numbers VALUES (322, 'E322 - Power Generation', 15, 'E322_-_Power_Generation');
INSERT INTO output.commodity_numbers VALUES (331, 'E331 - Metal Hand Tools', 15, 'E331_-_Metal_Hand_Tools');
INSERT INTO output.commodity_numbers VALUES (174, 'G174 - Paints Varnishes', 16, 'G174_-_Paints_Varnishes');
INSERT INTO output.commodity_numbers VALUES (181, 'G181 - Pharmaceutical Products', 16, 'G181_-_Pharmaceutical_Products');
INSERT INTO output.commodity_numbers VALUES (71, 'C071 - Milk and Milk Products', 18, 'C071_-_Milk_and_Milk_Products');
INSERT INTO output.commodity_numbers VALUES (73, 'C073 - Cheese', 18, 'C073_-_Cheese');
INSERT INTO output.commodity_numbers VALUES (74, 'C074 - Ice Cream', 18, 'C074_-_Ice_Cream');
INSERT INTO output.commodity_numbers VALUES (75, 'C075 - Coffee and Tea', 18, 'C075_-_Coffee_and_Tea');
INSERT INTO output.commodity_numbers VALUES (76, 'C076 - Water and Soft Drinks', 18, 'C076_-_Water_and_Soft_Drinks');
INSERT INTO output.commodity_numbers VALUES (78, 'C078 - Sugar', 18, 'C078_-_Sugar');
INSERT INTO output.commodity_numbers VALUES (354, 'V354 - Motor Homes', 20, 'V354_-_Motor_Homes');
INSERT INTO output.commodity_numbers VALUES (356, 'V356 - Buses', 20, 'V356_-_Buses');
INSERT INTO output.commodity_numbers VALUES (357, 'V357 - Vehicle Bodies', 20, 'V357_-_Vehicle_Bodies');
INSERT INTO output.commodity_numbers VALUES (358, 'V358 - Trailers', 20, 'V358_-_Trailers');
INSERT INTO output.commodity_numbers VALUES (359, 'V359 - Other Transportation Equipment', 20, 'V359_-_Other_Transportation_Equipment');
INSERT INTO output.commodity_numbers VALUES (361, 'V361 - Railroad Rolling Stock and Parts', 20, 'V361_-_Railroad_Rolling_Stock_and_Parts');
INSERT INTO output.commodity_numbers VALUES (371, 'V371 - Aircraft and Aircraft Parts', 20, 'V371_-_Aircraft_and_Aircraft_Parts');
INSERT INTO output.commodity_numbers VALUES (372, 'V372 - Boats', 20, 'V372_-_Boats');
INSERT INTO output.commodity_numbers VALUES (373, 'V373 - Ships', 20, 'V373_-_Ships');
INSERT INTO output.commodity_numbers VALUES (392, 'E392 - Furnishings Household', 1, 'E392_-_Furnishings_Household');
INSERT INTO output.commodity_numbers VALUES (393, 'E393 - Furnishings Other', 1, 'E393_-_Furnishings_Other');
INSERT INTO output.commodity_numbers VALUES (401, 'G401 - Arms and Ammunition', 1, 'G401_-_Arms_and_Ammunition');
INSERT INTO output.commodity_numbers VALUES (402, 'G402 - Sporting Goods', 1, 'G402_-_Sporting_Goods');
INSERT INTO output.commodity_numbers VALUES (404, 'G404 - Toys and Games', 1, 'G404_-_Toys_and_Games');
INSERT INTO output.commodity_numbers VALUES (411, 'G411 - Jewellery Precious Metal', 1, 'G411_-_Jewellery_Precious_Metal');
INSERT INTO output.commodity_numbers VALUES (412, 'G412 - Jewellery and Silverware', 1, 'G412_-_Jewellery_and_Silverware');
INSERT INTO output.commodity_numbers VALUES (451, 'W451 - Scrap Metals Ferrous', 1, 'W451_-_Scrap_Metals_Ferrous');
INSERT INTO output.commodity_numbers VALUES (452, 'W452 - Scrap Metals NonFerrous', 1, 'W452_-_Scrap_Metals_NonFerrous');
INSERT INTO output.commodity_numbers VALUES (502, 'S502 - Trans Services Passenger Rail', 32, 'S502_-_Trans_Services_Passenger_Rail');
INSERT INTO output.commodity_numbers VALUES (503, 'S503 - Trans Services Passenger Water', 32, 'S503_-_Trans_Services_Passenger_Water');
INSERT INTO output.commodity_numbers VALUES (505, 'S505 - Trans Services Taxi Limo', 32, 'S505_-_Trans_Services_Taxi_Limo');
INSERT INTO output.commodity_numbers VALUES (506, 'S506 - Trans Services Interurban Coach', 32, 'S506_-_Trans_Services_Interurban_Coach');
INSERT INTO output.commodity_numbers VALUES (507, 'S507 - Trans Services Urban Transit', 32, 'S507_-_Trans_Services_Urban_Transit');
INSERT INTO output.commodity_numbers VALUES (508, 'S508 - Trans Services School Bus', 32, 'S508_-_Trans_Services_School_Bus');
INSERT INTO output.commodity_numbers VALUES (509, 'S509 - Trans Services Passenger Other Ground', 32, 'S509_-_Trans_Services_Passenger_Other_Ground');
INSERT INTO output.commodity_numbers VALUES (511, 'S511 - Trans Services Freight Air', 32, 'S511_-_Trans_Services_Freight_Air');
INSERT INTO output.commodity_numbers VALUES (453, 'W453 - Scrap Wood', 1, 'W453_-_Scrap_Wood');
INSERT INTO output.commodity_numbers VALUES (353, 'V353 - Cars', 20, 'V353_-_Cars');
INSERT INTO output.commodity_numbers VALUES (381, 'E381 - Fine Instruments', 20, 'E381_-_Fine_Instruments');
INSERT INTO output.commodity_numbers VALUES (391, 'E391 - Lighting Fixtures', 20, 'E391_-_Lighting_Fixtures');


--
-- TOC entry 55742 (class 2606 OID 11949832)
-- Name: commodity_numbers commodity_numbers_pkey; Type: CONSTRAINT; Schema: output; Owner: -
--

ALTER TABLE ONLY output.commodity_numbers
    ADD CONSTRAINT commodity_numbers_pkey PRIMARY KEY (commoditynumber);


--
-- TOC entry 55740 (class 1259 OID 11949833)
-- Name: commodity_numbers_commodity_idx; Type: INDEX; Schema: output; Owner: -
--

CREATE INDEX commodity_numbers_commodity_idx ON output.commodity_numbers USING btree (commodity);


--
-- TOC entry 55743 (class 2606 OID 38421347)
-- Name: commodity_numbers commodity_numbers_commodity_type_id_fkey; Type: FK CONSTRAINT; Schema: output; Owner: -
