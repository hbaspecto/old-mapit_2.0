CREATE TABLE output.zonalmakeuse_temp
(
    activity integer NOT NULL,
    zonenumber integer NOT NULL,
    commodity integer NOT NULL,
    moru character varying COLLATE pg_catalog."default" NOT NULL,
    coefficient double precision,
    utility double precision,
    amount double precision,
    CONSTRAINT zonalmakeuse_temp_pkey PRIMARY KEY (commodity, zonenumber, activity, moru)
)
