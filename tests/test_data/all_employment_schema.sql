--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.2

-- Started on 2020-04-22 15:11:20 MDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10247 (class 1259 OID 35699555)
-- Name: all_employment; Type: TABLE; Schema: output; Owner: -
--

CREATE TABLE output.all_employment (
    scenario character varying NOT NULL,
    year_run integer NOT NULL,
    taz integer NOT NULL,
    naics integer NOT NULL,
    noc11 integer NOT NULL,
    amt double precision
);


--
-- TOC entry 65284 (class 0 OID 0)
-- Dependencies: 10247
-- Name: TABLE all_employment; Type: COMMENT; Schema: output; Owner: -
--

COMMENT ON TABLE output.all_employment IS 'This column stores the employment output data for each year for each scenario.  The employment is generated based on the use of labour in each LUZ (stored in all_zonalmakeuse), but these labor amounts are scaled into employees and interpreted.  The activity of the labor use is used to inform the NAICS industry codes, whereas the put that is used by the activity informs the NOCS occupation codes.  The labor amounts are also disaggregated from LUZs to TAZs, and used to increase, or decrease, the base year labor amounts by TAZ.  The result is a table of occupation (NOCS) and industry (NAICS) by TAZ, counting employees.  Employment numbers are not rounded to integers, this can be done by the user if desired. ';


--
-- TOC entry 65285 (class 0 OID 0)
-- Dependencies: 10247
-- Name: COLUMN all_employment.scenario; Type: COMMENT; Schema: output; Owner: -
--

COMMENT ON COLUMN output.all_employment.scenario IS 'This is the PECAS Spatial Economic Model scenario name.';


--
-- TOC entry 65286 (class 0 OID 0)
-- Dependencies: 10247
-- Name: COLUMN all_employment.year_run; Type: COMMENT; Schema: output; Owner: -
--

COMMENT ON COLUMN output.all_employment.year_run IS 'This is the PECAS Spatial Economic Model year of output (results year)';


--
-- TOC entry 65287 (class 0 OID 0)
-- Dependencies: 10247
-- Name: COLUMN all_employment.taz; Type: COMMENT; Schema: output; Owner: -
--

COMMENT ON COLUMN output.all_employment.taz IS 'This is the integer TAZ number for the employment counts';


--
-- TOC entry 65288 (class 0 OID 0)
-- Dependencies: 10247
-- Name: COLUMN all_employment.naics; Type: COMMENT; Schema: output; Owner: -
--

COMMENT ON COLUMN output.all_employment.naics IS 'This is the 2-digit NAICS industry code';


--
-- TOC entry 65289 (class 0 OID 0)
-- Dependencies: 10247
-- Name: COLUMN all_employment.noc11; Type: COMMENT; Schema: output; Owner: -
--

COMMENT ON COLUMN output.all_employment.noc11 IS 'This is the census 2011 NOC code, note that Statistics Canada codes can be confusing year-to-year, some are zero based and some are one based. Be sure to interpret this as the NOC11 code and not some other occupation code. ';


--
-- TOC entry 65290 (class 0 OID 0)
-- Dependencies: 10247
-- Name: COLUMN all_employment.amt; Type: COMMENT; Schema: output; Owner: -
--

COMMENT ON COLUMN output.all_employment.amt IS 'This is the number of employees, note it is not rounded to integer because it is based on a continuous model of labour use. ';


--
-- TOC entry 54930 (class 2606 OID 35699562)
-- Name: all_employment all_employment_pkey; Type: CONSTRAINT; Schema: output; Owner: -
--

ALTER TABLE ONLY output.all_employment
    ADD CONSTRAINT all_employment_pkey PRIMARY KEY (scenario, year_run, taz, naics, noc11);


--
-- TOC entry 54931 (class 1259 OID 35699563)
-- Name: ix_scen_employment; Type: INDEX; Schema: output; Owner: -
--

CREATE INDEX ix_scen_employment ON output.all_employment USING btree (scenario);


--
-- TOC entry 54932 (class 1259 OID 35699564)
-- Name: ix_year_employment; Type: INDEX; Schema: output; Owner: -
--

CREATE INDEX ix_year_employment ON output.all_employment USING btree (year_run);


-- Completed on 2020-04-22 15:11:37 MDT

--
-- PostgreSQL database dump complete
--

