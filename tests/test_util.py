#
# mapit_2.0/tests/test_util.py ---
#

import os
import pprint
import unittest

from mapit2.util import (
    parse_db_uri,
    lst_re_filter,
    round_pretty,
    float_range,
)

#####

MAPIT2_DB_URI = os.environ.get("MAPIT2_DB_URI", "://")
MAPITDB_DEFAULT_URI = os.environ.get("MAPITDB_DEFAULT_URI", "://")

#####


class TestUtilParseDbUri(unittest.TestCase):

    def test_parse_db_uri(self):

        for uri in [
                ":///",
                ":///database",
                "://host/database",
                "://:password@/database",
                "postgresql://user:password@host:1234/database",
                MAPIT2_DB_URI,
                MAPITDB_DEFAULT_URI,
        ]:
            rv = parse_db_uri(uri)
            pprint.pprint(rv)
            self.assertTrue(rv)

        for uri in [
                "nope",
                "not a uri",
        ]:
            rv = parse_db_uri(uri)
            self.assertFalse(rv)

    def test_parse_db_uri_1(self):
        rv=parse_db_uri("postgresql://user:password@host.domain:1234/mydatabase")

        self.assertEqual("postgresql",rv["scheme"])
        self.assertEqual("user",rv["user"])
        self.assertEqual("password",rv["password"])
        self.assertEqual("host.domain",rv["host"])
        self.assertEqual("1234",rv["port"])
        self.assertEqual("mydatabase",rv["database"])

class TestLstReFilter(unittest.TestCase):

    def test_1(self):
        self.assertEqual(
            [],
            lst_re_filter([]))

    def test_2(self):
        names=["name_1","name_2","name_3","foo"]

        #
        self.assertEqual(
            names,
            lst_re_filter(names))

        self.assertEqual(
            ["foo"],
            lst_re_filter(
                names,
                keep_regexes=["^foo"]))

        self.assertEqual(
            ["name_1","name_2","name_3"],
            lst_re_filter(
                names,
                remove_regexes=["^foo"]))

        self.assertEqual(
            ["name_1","name_2","name_3"],
            lst_re_filter(
                names,
                keep_regexes=["^name"]))

        self.assertEqual(
            ["name_1","name_2","name_3"],
            lst_re_filter(
                names,
                remove_regexes=["^foo"]))

class TestRoundPretty(unittest.TestCase):

    # python3 -m unittest test_util.TestRoundPretty

    def test_round_pretty_1(self):
        self.assertEqual(
            150.0,
            round_pretty(143.456,mode=2))

        self.assertEqual(
            15100.0,
            round_pretty(15099.456,digits=3))

        self.assertEqual(
            15099.456,
            round_pretty(15099.456,digits="8"))

        self.assertEqual(
            -150.0,
            round_pretty(-143.456,mode=2))

        self.assertEqual(
            0.050,
            round_pretty(.0456,mode=1))

        self.assertEqual(
            20.0,
            round_pretty(11,digits=1))

        ###

        self.assertEqual(
            1.2,
            round_pretty(1.1,mode="log"))

        self.assertEqual(
            15.0,
            round_pretty(13.0,mode="log"))

        self.assertEqual(
            200.0,
            round_pretty(190.0))

        self.assertEqual(
            8000.0,
            round_pretty(6001.0))

        self.assertEqual(
            0.025,
            round_pretty(0.021))

        self.assertEqual(
            0.025,
            round_pretty(0.025))

        self.assertEqual(
            0.080,
            round_pretty(0.070))

class TestFloatRange(unittest.TestCase):

    def test_float_range_1(self):
        lst=float_range(0.0,10.0,10)
        print("float_range:",lst)
        self.assertEqual(
            lst,
            [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])

        lst=float_range(0.0,10.0,f_step=1.0)
        print("float_range:",lst)
        self.assertEqual(
            lst,
            [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])

        lst=float_range(-10.0,10.0,10)
        print("float_range:",lst)
        self.assertEqual(
            lst,
            [-10.0, -8.0, -6.0, -4.0, -2.0, 0.0, 2.0, 4.0, 6.0, 8.0, 10.0])

#####

if __name__=="__main__":
    unittest.main()
