''' This was copied from https://bitbucket.org/hbaspecto/hba-python/src/master/PECAS%20run%20scripts/pecas_routines.py
'''
import os

import psycopg2
import sqlutil


def load_outputs_for_year_sql(year, ps, filesrows):
    """No longer in-line for deprecation; to remain in the codebase as an alternative
       option for load_outputs_for_year_http()."""

    print("Loading data")

    #excluded_files = [fn if fn.endswith('.csv') else (fn + '.csv') for fn in (excl or [])]

    folder_path = 'test_data'

    def connect():
        return psycopg2.connect(
            database=os.environ.get('PGDATABASE'),
            host=os.environ.get('PGHOST'),
            port=os.environ.get('PGPORT'),
            user=os.environ.get('PGUSER'),
            password=os.environ.get('PGPASSWORD'))

    q = sqlutil.Querier(connect)

    with q.transaction(sch='output') as tr:
        for row in filesrows:  # for each output file
            csv_file_name = row.get('csv_file_name')

            print("Loading file " + csv_file_name)
            all_table_name = row.get('all_table_name')
            temp_table_name = row.get('temp_table_name')

            if hasattr(row, "partitioned"):
                partition = row.partitioned
            else:
                partition = False
            tr.query('TRUNCATE {sch}.{tmp}', tmp=temp_table_name)  # empty the temporary table
            csv_file = os.path.join(folder_path, csv_file_name)
            try:
                tr.load_from_csv("{sch}.{tmp}", csv_file, tmp=temp_table_name)

                counter = tr.query('SELECT count(*) FROM {sch}.{tmp};', tmp=temp_table_name)[0][0]

                # now insert the records from the temporary table into the full table which contains data for each
                # year/scenario. This needs to happen before the scenario runs
                if partition:
                    logging.info("Trying to partition " + all_table_name)
                    tr.query(
                        "select * from {sch}.{all}__create_partition(%(scen)s)",
                        all=all_table_name, scen=ps.scenario
                    )
                temptablecolnames = tr.query(
                    "SELECT column_name from INFORMATION_SCHEMA.COLUMNS "
                    "where TABLE_NAME=%(tmp)s AND TABLE_SCHEMA = %(sch)s;",
                    tmp=temp_table_name
                )
                print(temptablecolnames)
                temptablecolnames = [col[0] for col in temptablecolnames]
                # TODO Need to pass in a cursor factory that gives named tuples instead of tuples
                #temptablecolnames = [col.column_name for col in temptablecolnames]
                cols = ", ".join(temptablecolnames)
                tr.query(
                    "INSERT INTO {sch}.{all} (scenario, year_run, {cols}) "
                    "SELECT %(scen)s, %(year)s, {cols} from {sch}.{tmp}",
                    cols=cols, scen=ps.get('scenario'), year=year, tmp=temp_table_name, all=all_table_name
                )

                print("%d record(s) added to %s" % (counter, all_table_name))
            except IOError:  # if file does not exist. ex. ActivityLocations2_6k.csv not created every year.
                print("NOTICE: No such file or directory:" + csv_file)

        #tr.query('SET search_path = {sch}')
        #tr.callproc('output.update_loaded_scenarios', [ps.get('scenario'), year, year])
        print("Finished loading test data for year:" + str(year))


def called_from_make():
    filesrows = [{
        'csv_file_name': 'ZonalMakeUse_Q241i_2020.csv',
        'all_table_name': 'all_zonalmakeuse',
        'temp_table_name': 'zonalmakeuse_temp'}]

    ps = {
        'scenario': 'Q241i'
    }

    load_outputs_for_year_sql(2020, ps=ps, filesrows=filesrows)

    filesrows = [{
        'csv_file_name': 'ZonalMakeUse_P243_2020.csv',
        'all_table_name': 'all_zonalmakeuse',
        'temp_table_name': 'zonalmakeuse_temp'}]

    ps = {
        'scenario': 'P243'
    }

    load_outputs_for_year_sql(2020, ps=ps, filesrows=filesrows)


def main():
    ps = {
        'scenario': 'I240'
    }

    # TODO put these as test data
    filesrows = [{
        'csv_file_name': 'Employment_I240_2059.csv',
        'all_table_name': 'all_employment',
        'temp_table_name': 'employment_temp'}]

    load_outputs_for_year_sql(2059, ps=ps, filesrows=filesrows)


if __name__ == "__main__":
    # execute only if run as a script
    main()
