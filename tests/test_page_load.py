#
# mapit_2.0/tests/test_page_load.py ---
#

import os
import pprint
import unittest

import mapit2.app

#####


class TestUtilParseDbUri(unittest.TestCase):


    TEST_URLS=[
        "/",
        "/avg_size_occupied_units/create",
        "/avg_size_residential_units/create",
        # @todo: "/employment_by_luz_taz/create",
        "/employment_by_naics/create",
        "/occupied_space_all_residential/create",
        "/occupied_space_all_space_type/create",
        "/occupied_space_selected_space_type/create",
        "/occupied_units/create",
        # @todo: "/population_by_luz_taz/create",
    ]


    def get_client(self):
        """Get a client to test with."""
        return mapit2.app.app.test_client()

    def test_load(self):
        """Just try and load the pages as a smoke test.

        We dont even submit the forms.
        This should be enough to smoke out syntax errors.
        """

        client=self.get_client()
        for url in self.TEST_URLS:
            rv=client.get(url)

            print("test_load: {!r}  {}".format(
                url,
                rv.status_code))
            self.assertTrue(rv.status_code==200)
