-- FUNCTION: output.generate_crosstab_from_3_col_query(text, text, text, text, text, text, text, double precision)

-- DROP FUNCTION output.generate_crosstab_from_3_col_query(text, text, text, text, text, text, text, double precision);

CREATE OR REPLACE FUNCTION output.generate_crosstab_from_3_col_query(
	src_query text,
	output_qry_name text,
	category_name text,
	default_value double precision DEFAULT NULL::double precision,
	schema text DEFAULT 'analysis'::text,
	prefix_col text DEFAULT 'sum'::text,
	suffix_col text DEFAULT 'quantity'::text,
	geom_table text DEFAULT NULL::text,
	geom_zone_column text DEFAULT 'luz'::text)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
    sql_src_query text;  -- add 'select * from' to the beginning of the query, if it isn't already there
    sql_col_query text; -- query to find the distinctive column names in order
    sql_colnames  text; -- output of query to find the distinctive column names
    col_names     text[]; -- the actual distinctive column names
    sql_generate  text[]; -- this gets built up as the command to generate the materialized view
    record_sel    text[];
    record_types  text[];
    geom_create_string text;
    default_value_string text;
BEGIN
	-- First make sure it's a Select query, e.g. the user may have given a table name instead of a SELECT * FROM query.
    IF upper( left( ltrim( src_query ), 6 ) ) = 'SELECT' THEN
        sql_src_query := src_query;
        src_query := '(' || src_query || ') srcq';
    ELSE
        sql_src_query := 'SELECT * FROM ' || src_query;
    END IF;

    -- This is to get our column names
    sql_col_query :=
        'SELECT DISTINCT "' || category_name ||
        '"  FROM ' || src_query ||
        ' ORDER BY 1 ';
    sql_colnames :=
        'SELECT array_agg( "' || category_name || '" )' ||
        '  FROM (' || sql_col_query || ') cols;';
    EXECUTE sql_colnames INTO col_names;
    raise notice 'Colnames: %', col_names;

	-- now we start building up our big ugly crosstab query.  First we set up the "create materialized view" part and specify the first column name
    sql_generate := ARRAY[
        'CREATE materialized VIEW ' || schema || '."' || output_qry_name || '" AS ',
        '    SELECT zonenumber ,'
    ];
	raise notice 'SQL generate part 1: %', sql_generate;

	-- Now we have to remorgify our column names in the right syntax for the dumb crosstab function, but we get
	-- to add a prefix and a suffix to them conveniently if we want, so the user knows its the "sum_2011_demand" or whatever.
	default_value_string := case when default_value is null then 'NULL' else default_value::text end;
    record_sel := (
        SELECT array_agg( concat( '           ',
                                   'coalesce( ct."', col, '", ', default_value_string, ' ) AS "',
                                   prefix_col, col, suffix_col, '"' ) )
          FROM unnest( col_names ) AS col
    );

	raise notice 'Record selection: %', record_sel;

	-- Now we build up most of the dumb crosstab syntax that PostgreSQL tablefunc actually needs
    sql_generate := sql_generate || ARRAY[
        array_to_string( record_sel, E',\n' ),
        '      FROM crosstab(',
        '          ' || quote_literal( sql_src_query ) || '::text,',
        '          ' || quote_literal( sql_col_query ) || '::text',
        '      ) as ct(',
        '          zonenumber integer,'
    ];

	raise notice 'SQL generate part 2: %', sql_generate;

    -- but we also need to specify the output types of the crosstab amounts, assume they are double precision
    record_types := (
        SELECT array_agg( concat( '          "',  col, '" double precision' ) )
          FROM unnest( col_names ) AS col
    );
 	raise notice 'Record types: %', record_types;

 	-- Add the output types to the query, and sort it as well.
    sql_generate := sql_generate || ARRAY[
        array_to_string( record_types, E',\n' ),
        '      )',
        '  ORDER BY 1;'
    ];
 	raise notice 'SQL generate part 3 (final): %', sql_generate;

   	-- Now we finally execute the SQL, which generates the crosstab materialized view so we can use it.
    EXECUTE array_to_string( sql_generate, E'\n' );

    if geom_table is not null then
    	geom_create_string := 'create view ' || schema || '."' || output_qry_name ||'_geom" '
			'as select a.*, b.geom from ' || schema || '."'|| output_qry_name ||'" a join "' || geom_table || '" b on (a.zonenumber = b."' || geom_zone_column ||'");';
		raise notice 'geom view query: %', geom_create_string;
		execute geom_create_string;
    end if;

END;
$BODY$;



CREATE FUNCTION output.update_all_technology_choice_widx_table_from_view()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

	DECLARE
		option_id_int integer;
BEGIN
    IF TG_OP = 'INSERT' THEN
		select id into option_id_int from output.technology_options where option_name = NEW.option_name;
		if option_id_int is null then 
			insert into output.technology_options (option_name) values (NEW.option_name);
			select id into option_id_int from output.technology_options where option_name = NEW.option_name;
		end if;
        INSERT INTO output.all_technology_choice_widx (
            year_run, scenario, activity, zonenumber, option_id, utility, constant,
            baseutility, sizeutility, size,
            probability
        ) VALUES (
            NEW.year_run, NEW.scenario, NEW.activity, NEW.zonenumber,
            option_id_int, NEW.utility, NEW.constant, NEW.baseutility, NEW.sizeutility,
            NEW.size,
            NEW.probability
        );
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
		select id into option_id_int from output.technology_options where option_name = OLD.option_name;
		if option_id_int is not null then 
        UPDATE output.all_technology_choice_widx SET
            year_run = NEW.year_run,
            scenario = NEW.scenario,
            activity = NEW.activity,
            zonenumber = NEW.zonenumber,
            option_id = option_id_int,
			utility = NEW.utility,
            constant = NEW.constant,
            baseutility = NEW.baseutility,
            sizeutility = NEW.sizeutility,
            size = NEW.size,
            probability = NEW.probability
        WHERE
            year_run = OLD.year_run
			and scenario = OLD.scenario
			and activity = OLD.activity
			and zonenumber = OLD.zonenumber
			and option_id = option_id_int;
		end if;
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
		select id into option_id_int from output.technology_options where option_name = OLD.option_name;
		if option_id_int is not null then 
			DELETE FROM output.all_technology_choice_widx
			WHERE
            year_run = OLD.year_run
			and scenario = OLD.scenario
			and activity = OLD.activity
			and zonenumber = OLD.zonenumber
			and option_id = option_id_int;
		end if;
        RETURN NULL;
    END IF;
    RETURN NEW;
END;

$BODY$;



CREATE OR REPLACE FUNCTION output.all_activity_locations__create_partition(
	scenario text)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
declare
  scenario_lc text;
  stmt text;
begin

  raise notice 'all_activity_locations__create_partition: %',scenario;

  scenario_lc:=lower(scenario);
  stmt:='create table if not exists ' ||
        'partitioned_tables.all_activity_locations__part_' || scenario_lc || ' ' ||
        'partition of output.all_activity_locations ' ||
        '( ' ||

        'unique (scenario,year_run,zonenumber,activity ) ' ||

        ') ' ||
        'for values in ('''||scenario||''')' ||
        ' with ( ' ||

        ' fillfactor = 95 ' ||

        ' ) ' ||

        ' tablespace space_2 ' ||

        ';';

  raise notice 'all_activity_locations__create_partition: %',stmt;

  execute stmt;

  -- if you dont like looking at the logs.
  return stmt;

  -- the name as the return value.
  -- return scenario;

end;
$BODY$;


CREATE OR REPLACE FUNCTION output.all_technology_choice__create_partition(
	scenario text)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
  scenario_lc text;
  stmt text;
begin

  raise notice 'all_technology_choice_widx__create_partition: %',scenario;

  scenario_lc:=lower(scenario);
  stmt:='create table if not exists ' ||
        'partitioned_tables.all_technology_choice_widx__part_' || scenario_lc || ' ' ||
        'partition of output.all_technology_choice_widx ' ||
        '( ' ||

        'unique (scenario,year_run,zonenumber,activity,option_id ) ' ||

        ') ' ||
        'for values in ('''||scenario||''')' ||
        ' with ( ' ||

        ' fillfactor = 95 ' ||

        ' ) ' ||

        ' tablespace space_2 ' ||

        ';';

  raise notice 'all_technology_choice_widx__create_partition: %',stmt;

  execute stmt;

  -- if you dont like looking at the logs.
  return stmt;

  -- the name as the return value.
  -- return scenario;

end;

$BODY$;


create schema partitioned_tables;

CREATE TABLE output.loaded_scenarios
(
    scen_name character varying COLLATE pg_catalog."default" NOT NULL,
    start_year integer,
    end_year integer,
    CONSTRAINT scen_name_pk PRIMARY KEY (scen_name)
)
WITH (
    OIDS = FALSE
);

CREATE OR REPLACE FUNCTION output.update_loaded_scenarios(
	scenario_name text,
	start_yr integer,
	end_yr integer)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$
DECLARE
	arow output.loaded_scenarios%ROWTYPE;
	v_start_year integer = start_yr;
	v_end_year   integer = end_yr;
BEGIN	
	IF v_end_year IS NULL THEN
		v_end_year = v_start_year;
	END IF;

	IF v_end_year < v_start_year THEN
		-- SWAP 2 variables with no third temp variable :)
		v_end_year = v_end_year + v_start_year;
		v_start_year = v_end_year - v_start_year;
		v_end_year = v_end_year - v_start_year;
	END IF;

	RAISE NOTICE 'Updating Scenario % with % and %', scenario_name, v_start_year, v_end_year;
	SELECT scen_name, start_year, end_year INTO STRICT arow FROM output.loaded_scenarios WHERE scen_name = scenario_name;
		IF arow.start_year > v_start_year THEN
			UPDATE output.loaded_scenarios SET start_year = v_start_year
			WHERE scen_name = scenario_name;
		ELSIF arow.end_year < v_end_year THEN
			UPDATE output.loaded_scenarios SET end_year = v_end_year
			WHERE scen_name = scenario_name;
		END IF;
	EXCEPTION 
		WHEN NO_DATA_FOUND THEN
			INSERT INTO output.loaded_scenarios VALUES (scenario_name, v_start_year, v_end_year);
END;
$BODY$;

create extension tablefunc;
create schema mapit2_views;