--
-- mapit_2.0/src/sql/edmonton_funcs.sql ---
--
-- Define some functions to clean up the SQL edmonton uses.
-- This way we should reduce the chance for a cut-n-paste error;
-- Plus if we change the zones, we dont have to change it everywhere.

-----

-- Turns a TAZ into a LUZ.
-- While simple, I think it makes the SQL more readable.
create or replace function public.edmonton_taz_to_luz(
  taz integer)
returns integer
immutable
strict
language 'plpgsql'
as $$
begin
   return floor(taz/100);
end;
$$;

-----

-- Convert a TAZ to a string like 'Edmonton City'
create or replace function public.edmonton_taz_to_region_group(
   taz integer)
returns text
immutable
strict
language 'plpgsql'
as
$$
declare
  zz integer;
begin

  -- turn taz into zz
  zz:=floor(taz/10000);

  -- Returns the first matching region_group.

  --
  if zz in (22) or
     taz in (
       112508, 112509, 112510, 112513, 112514,
       112515, 112516, 112517, 112610, 112611,
       112612, 112613, 112614, 112701, 112702,
       112703, 112704, 112705, 112706, 112707,
       112708)
   then
     return 'Edmonton City';
   end if;

  -- The "not in" is redundant, as it should have been matched above.
  if zz in (11) and
     taz not in (
       112508, 112509, 112510, 112513, 112514,
       112515, 112516, 112517, 112610, 112611,
       112612, 112613, 112614, 112701, 112702,
       112703, 112704, 112705, 112706, 112707,
       112708)
  then
    return 'Edmonton Region';
  end if;

  -- This shouldnt match as the prior two should match.
  if zz in (11,22)
  then
    return 'Edmonton Metro';
  end if;

  --
  if zz in (6,21)
  then
    return 'Calgary Metro';
  end if;

  --
  if zz in (16)
  then
    return 'Wood Buffalo';
  end if;

  --
  if zz in (19)
  then
    return 'Grande Prairie';
  end if;

  --
  if  zz in (8)
  then
    return 'Red Deer';
  end if;

  --
  return 'Rest of Province';

end;
$$;
