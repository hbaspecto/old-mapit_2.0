--
-- mapit_2.0/src/sql/edmonton_funcs_example.sql ---
--
-- examples of using the edmonton functions.

----------

-- put the output here.
\o edmonton-output-pop-by-region-group.txt

--Population by region group
SELECT year_run, scenario, LUZ, sum_pop FROM (
  SELECT year_run, scenario,
         edmonton_zone_to_luz(zonenumber) AS LUZ,
        floor(sum(population_at_avg_size)) AS sum_pop,
        edmonton_taz_to_region_group(zonenumber) as region_group
  FROM manual_views.taz_population
  --specify scenario here:
  where scenario in ('I222', 'I225', 'P226')
  --If you want to query a single year, e.g.:
  --and year_run = 2021
  --If you want to query a set of years, e.g.:
  --and year_run in (2021, 2037)
  --If you want all years, you don't need to specify this
  group by year_run, scenario, LUZ, region_group) tbl
WHERE region_group = 'Edmonton City' AND
      year_run in (2049, 2079)
--WHERE statement filters the region_group to be 'Edmonton City' only and include only odd years and 2016.
order by year_run, scenario, LUZ;

----------

-- put the output here.
\o edmonton-output-employment-by-region-group.txt

--Employment by region group
SELECT year_run, scenario, LUZ, sum_emp FROM (
  SELECT year_run, scenario,
         edmonton_zone_to_luz(taz) AS LUZ,
         floor(sum(amt)) AS sum_emp,
         edmonton_taz_to_region_group(taz) as region_group
  FROM output.all_employment
  --specify scenario here:
  where scenario in ('I222', 'I225', 'P226')
  --If you want to query a single year, e.g.:
  --and year_run = 2021
  --If you want to query a set of years, e.g.:
  --and year_run in (2021, 2037)
  --If you want all years, you don't need to specify this
  group by year_run, scenario, LUZ, region_group) TBL
WHERE region_group = 'Edmonton City' AND
      year_run in (2049, 2079)
ORDER BY year_run, scenario, LUZ;
