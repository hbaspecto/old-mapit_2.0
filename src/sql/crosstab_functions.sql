

-- FUNCTION: output.generate_crosstab_from_query(text, text, text, text, text)

-- TODO: Would be nice to use RowType or RowToJson or some other function to determine the name of the
-- first column (almost always zonenumber) and second column (usually commodity or activity)
-- so that we don't have to assume the first column name and have the user specify the second.

-- TODO: Analysis schema shouldn't be hardcoded, can't we just specify it in output_qry_name ???  analysis.mycrosstabview

DROP FUNCTION if exists output.generate_crosstab_from_3_col_query(text, text, text, text, text, text, text);
DROP FUNCTION if exists output.generate_crosstab_from_3_col_query(text, text, text, text, text, text, text, double precision);

CREATE OR REPLACE FUNCTION output.generate_crosstab_from_3_col_query(
	src_query text, -- the query that we are crosstabbing, 3 columns zonenumber, category, quantity, note first column needs to be zonenumber
	output_qry_name text, -- the output materialized view to be created
	category_name text, -- the name of the category column, the 2nd column, e.g. 'commodity'
	prefix_col text DEFAULT 'sum'::text, -- the prefix for the column rows, e.g. "sum" or "average" depending on how the summary query was created
	suffix_col text DEFAULT 'quantity'::text, -- the suffix for the column rows (generally name of the 3rd column)
	geom_table text DEFAULT null, -- The name of the geom table to join the materialized view, in a view named the same but with the _geom suffix, use null to not generate a geom view.
	geom_zone_column text DEFAULT 'luz'::text, -- the name of the zone number column in the table specified above that has the zone geometry.
	default_value double precision default null -- crosstabs have nulls for missing values, if null means zero you can put zero here to replace the nulls with zero
	)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
    sql_src_query text;  -- add 'select * from' to the beginning of the query, if it isn't already there
    sql_col_query text; -- query to find the distinctive column names in order
    sql_colnames  text; -- output of query to find the distinctive column names
    col_names     text[]; -- the actual distinctive column names
    sql_generate  text[]; -- this gets built up as the command to generate the materialized view
    record_sel    text[];
    record_types  text[];
    geom_create_string text;
    default_value_string text;
BEGIN
	-- First make sure it's a Select query, e.g. the user may have given a table name instead of a SELECT * FROM query.
    IF upper( left( ltrim( src_query ), 6 ) ) = 'SELECT' THEN
        sql_src_query := src_query;
        src_query := '(' || src_query || ') srcq';
    ELSE
        sql_src_query := 'SELECT * FROM ' || src_query;
    END IF;

    -- This is to get our column names
    sql_col_query :=
        'SELECT DISTINCT ' || category_name ||
        '  FROM ' || src_query ||
        ' ORDER BY 1 ';
    sql_colnames :=
        'SELECT array_agg( ' || category_name || ' )' ||
        '  FROM (' || sql_col_query || ') cols;';
    EXECUTE sql_colnames INTO col_names;
    raise notice 'Colnames: %', col_names;

	-- now we start building up our big ugly crosstab query.  First we set up the "create materialized view" part and specify the first column name
    sql_generate := ARRAY[
        'CREATE materialized VIEW analysis."' || output_qry_name || '" AS ',
        '    SELECT zonenumber ,'
    ];
	raise notice 'SQL generate part 1: %', sql_generate;

	-- Now we have to remorgify our column names in the right syntax for the dumb crosstab function, but we get
	-- to add a prefix and a suffix to them conveniently if we want, so the user knows its the "sum_2011_demand" or whatever.
	default_value_string := case when default_value is null then 'NULL' else default_value::text end;
    record_sel := (
        SELECT array_agg( concat( '           ',
                                   'coalesce( ct."', col, '", ', default_value_string, ' ) AS "',
                                   prefix_col, col, suffix_col, '"' ) )
          FROM unnest( col_names ) AS col
    );

	raise notice 'Record selection: %', record_sel;

	-- Now we build up most of the dumb crosstab syntax that PostgreSQL tablefunc actually needs
    sql_generate := sql_generate || ARRAY[
        array_to_string( record_sel, E',\n' ),
        '      FROM output.crosstab(',
        '          ' || quote_literal( sql_src_query ) || '::text,',
        '          ' || quote_literal( sql_col_query ) || '::text',
        '      ) as ct(',
        '          zonenumber integer,'
    ];

	raise notice 'SQL generate part 2: %', sql_generate;

    -- but we also need to specify the output types of the crosstab amounts, assume they are double precision
    record_types := (
        SELECT array_agg( concat( '          "',  col, '" double precision' ) )
          FROM unnest( col_names ) AS col
    );
 	raise notice 'Record types: %', record_types;

 	-- Add the output types to the query, and sort it as well.
    sql_generate := sql_generate || ARRAY[
        array_to_string( record_types, E',\n' ),
        '      )',
        '  ORDER BY 1;'
    ];
 	raise notice 'SQL generate part 3 (final): %', sql_generate;

   	-- Now we finally execute the SQL, which generates the crosstab materialized view so we can use it.
    EXECUTE array_to_string( sql_generate, E'\n' );

    if geom_table is not null then
    	geom_create_string := 'create view analysis.' || output_qry_name ||'_geom '
			'as select a.*, b.geom from analysis.'|| output_qry_name ||' a join ' || geom_table || ' b on (a.zonenumber = b.' || geom_zone_column ||');';
		raise notice 'geom view query: %', geom_create_string;
		execute geom_create_string;
    end if;

END;
$BODY$;

