
-- test case
-- the data we want to crosstab
select zonenumber, --activity,
replace( commodity, ' ', '_' )as commodity, sum(amount) as quantity from output.all_zonalmakeuse_act_com
where year_run = 2019 and scenario = 'I236b' and
activity = '501_GovK12' and commodity in ('L11 - Instructor', 'L16 - Labourer')
group by zonenumber, activity, commodity limit 20;

-- Note there are three columns, the first must be "zonenumber", the second is the categorization you want to turn into columns
-- for GIS viewing (e.g. years, commoditieis), and the third are the values.

-- test using the function
-- these are the views that should be created

-- cleanup before tests
drop view if exists analysis.johns_test_query_geom;
drop materialized view if exists analysis.johns_test_query;

-- this is how we call the function when we aren't creating a geom
select output.generate_crosstab_from_3_col_query(
	'select zonenumber, replace( commodity, '' '', ''_'') as commodity, sum(amount) as quantity from output.all_zonalmakeuse_act_com
where year_run = 2019 and scenario = ''I236b'' and
activity = ''501_GovK12'' and commodity in (''L11 - Instructor'', ''L16 - Labourer'')
group by zonenumber, activity, commodity
	',
	'johns_test_query', -- it will create a materialized view called this
	'commodity', -- this is just the name of the second column
	'total_', -- prefix to the generated column names
	'_consumption' -- suffix to the generated column names
);

-- check to see if it worked

select * from analysis.johns_test_query limit 20;

-- clear it out
drop materialized view if exists analysis.johns_test_query;

-- this is how we call the function when we are creating a geom

select output.generate_crosstab_from_3_col_query(
	'select zonenumber, replace( commodity, '' '', ''_'') as commodity, sum(amount) as quantity from output.all_zonalmakeuse_act_com
where year_run = 2019 and scenario = ''I236b'' and
activity = ''501_GovK12'' and commodity in (''L11 - Instructor'', ''L16 - Labourer'')
group by zonenumber, activity, commodity
	',
	'johns_test_query', -- it will create a materialized view called this
	'commodity', -- this is just the name of the second column
	'total_', -- prefix to the generated column names
	'_consumption', -- suffix to the generated column names
	'public.luz_v4', -- our zone table
	'luz' -- the name of the zone index column in the zone table
);


select * from analysis.johns_test_query_geom limit 1;

-- MANUALTEST: Load analysis.johns_test_query_geom into QGIS and make sure it can be viewed.
