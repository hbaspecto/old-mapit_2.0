CREATE OR REPLACE VIEW {{schema}}.parcels_{{year}}_view
 AS
 SELECT a.*,
    b.geom
   FROM {{schema}}.parcels_{{year}} a
     JOIN {{schema}}.parcels_backup_with_geom b ON a.pecas_parcel_num = b.pecas_parcel_num;
