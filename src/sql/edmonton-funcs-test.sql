--
-- mapit_2.0/src/sql/edmonton-funcs-test.sql ---
--

do
$$
begin
  -- to see what a failure looks like:
  -- ASSERT 1 = 0;

  -----

  -- edmonton_zone_to_luz
  ASSERT edmonton_zone_to_luz(12300) = 123;
  ASSERT edmonton_zone_to_luz(23499) = 234;

  -----

  -- edmonton_taz_to_region_group
  ASSERT edmonton_taz_to_region_group(0) = 'Rest of Province';
  ASSERT edmonton_taz_to_region_group(99999999) = 'Rest of Province';

  --
  ASSERT edmonton_taz_to_region_group(112508) = 'Edmonton City';
  ASSERT edmonton_taz_to_region_group(112708) = 'Edmonton City';
  ASSERT edmonton_taz_to_region_group(220001) = 'Edmonton City';

  --
  ASSERT edmonton_taz_to_region_group(110001) = 'Edmonton Region';
  ASSERT edmonton_taz_to_region_group(119999) = 'Edmonton Region';

  --
  ASSERT edmonton_taz_to_region_group( 60000) = 'Calgary Metro';
  ASSERT edmonton_taz_to_region_group( 69999) = 'Calgary Metro';
  ASSERT edmonton_taz_to_region_group(210000) = 'Calgary Metro';
  ASSERT edmonton_taz_to_region_group(219999) = 'Calgary Metro';

  --
  ASSERT edmonton_taz_to_region_group(160000) = 'Wood Buffalo';
  ASSERT edmonton_taz_to_region_group(169999) = 'Wood Buffalo';

  --
  ASSERT edmonton_taz_to_region_group(190000) = 'Grande Prairie';
  ASSERT edmonton_taz_to_region_group(199999) = 'Grande Prairie';

  --
  ASSERT edmonton_taz_to_region_group( 80000) = 'Red Deer';
  ASSERT edmonton_taz_to_region_group( 89999) = 'Red Deer';

end;
$$
LANGUAGE plpgsql;
