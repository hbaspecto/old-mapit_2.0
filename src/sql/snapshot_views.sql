create view i237.parcels_2080_view
as select g.geom, p.parcel_id, p.pecas_parcel_num, p.year_built, p.taz, p.space_type_id, p.space_quantity, p.land_area, 
p.available_services_code, p.is_derelict, p.is_brownfield
	FROM i237.parcels_2080 p
	join i237.parcels_backup_with_geom g using (pecas_parcel_num)