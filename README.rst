README
==================================================

Mapit2 is a flexible web system for taking PECAS output and
viewing it in maps.


Links
--------------------------------------------------

- BB: https://bitbucket.org/hbaspecto/mapit_2.0
- Docs: http://docs.office.hbaspecto.com/mapit_2.0
- Jenkins: http://jenkins-2.office.hbaspecto.com/job/mapit_2.0
- Licence: http://www.hbaspecto.com/products/pecas/software

Quickstart
--------------------------------------------------

::

    # get the source.
    git clone git@bitbucket.org:hbaspecto/mapit_2.0.git
    cd mapit_2.0

    # you may want to make a symlink to a "local.env" file.
    # (cd configs && ln -fs ./other/host-brooks.env ./local.env)

    # If you dont have a redis server, disable it in local.env
    # (There is one running on leduc.)

    # with that done, set the env vars.
    # (If you want a different config, you can look in ./configs.)
    source ./hba-setup.env

    # Maybe you have some of your own environment variables or settings
    # If you haven't simlinked configs/local.env to your these, you can include them manually:
    source ./configs/other/<your_machine>/setup.env

    #
    make _ve_rebuild

    # You might need to load crosstab functions in ``./src/sql``.
    # (@TODO: add instructions)

    # If you havent created a DB for mapit-2 configuration
    # yet you can using these commands:
    # (cd db && make _postgres_create_user)
    # (cd db && make _postgres_create_database)
    # (cd migrations && make _alembic_upgrade_head)

Note that by default the database with the actual model run data in it is
``mapit2_hba_dev`` on ``brooks.office.hbaspecto.com``, you can adjust this in
``configs/hba-dev/setup.env``

::

    # To test, you need to:
    # - run the backend worker:
    make _worker_run

    # - run the webserver
    # ctrl z and bg if you don't want to use the same window, otherwise
    # source ./hba-setup.env in a new window after cd to the directory
    make _server_run

    # the webserver defaults to: ``http://127.0.0.1:5733``

Ports
--------------------------------------------------

The web server runs on these ports:

::

    prd: 5731
    stg: 5732
    dev: 5733

Adding new views/tables
--------------------------------------------------

When you add a new query to mapit2, you will need to add
tables as well.  Create the tables with:

::

    # edit code to add tables.
    emacs ./mapit2/mapit2/FOO.py

    # create the migration
    (cd migrations && make _alembic_revision_autogenerate)

    # Edit the generated file, and rename it
    emacs ./migrations/scripts/versions/FOO.py

    # Apply migration to the DB.
    (cd migrations && make _alembic_upgrade_head)


Who do I talk to?
--------------------------------------------------

- jea@hbaspecto.com (Ideas and code)
- harley@hbaspecto.com (Janitor)


What is ``mapit2``?
----------------------------

GIS systems generally require a column for each thing you want to view,
and a row for each geometry (e.g. multipolygon) on the map. The PECAS
outputs are many and varied, and most are stored in the MapIt database. The role
of Mapit2 is to make it easy to select which things you want to review
from PECAS, and create an appropriate data view where the things you
want are connected (in columns) to the shapes you want (as rows, these
will generally be parcels, TAZs or LUZs).

Mapit 1 was written long ago in Django. It was designed specifically to
look at Commodity (renamed Put) categories by LUZs and TAZs, or Activity
categories by LUZ and TAZ. It also provided some essential web maps
which had functionality (such as bar-graphs of values over time by zone)
which were not generally available in open source GIS systems. It has
become difficult to maintain, its design to allow selection only on
puts and activities has made it limited, and the maps it produces are
less essential now that QGIS is more powerful.

Mapit 1 will remain in service, with Mapit 2 initially focusing on
easily bringing into QGIS those sorts of things that Mapit 1 cannot do.
Initially, it will still focus on crosstabbing selections from the MapIt
database, but could be extended to the SD database, or even the file
system on the run computer. It could also show non-spatial result on web pages,
or in other desktop apps such as Excel.

Design priorities
-----------------

1. Allow user-specified views

2. Growing collection of standardized views as the tool is used and
   extended

   -  clear instructions on how to add a new view to the tool

3. Each use generates a materialized view of the crosstabbed data, which
   is joined (in a separate view) to the approriate geometry, so that
   the view can be directly imported to GIS systems

4. Each use also has a layer definition file that can be downloaded, for
   easy import to QGIS

5. Useful for both the Mapit database, and the SD database

Final Components
----------------

1. A browser page for users to specify the input parameters for POST,
   specifically

   -  scenario or 2 scenarios
   -  set of occupation codes
   -  set of industry codes
   -  begin year and end year

2. A api endpoint for the post that then does the following

   -  creates a permanent view in the analysis schema that sums the
      employment amount by TAZ for the user-selected categories and
      years

   -  creates a materialized view in the analysis schema that crosstabs
      the amounts from the view above, so there’s a row for each TAZ and
      a column for each year

   -  creates a view in the analysis schema that uses the above view,
      but connects it to the TAZ geometry and also adds some supporting
      columns (percent change from begin year to end year, land area of
      TAZ, etc.)

   -  Adds metadata that describes the selection and the map in both
      human readable terms and computer readable terms.

3. Create web URLS for browser and web services.

   -  This could use MapServer or some other more modern geoserver
      product to draw web maps and also to serve map tiles and map data
      over standard protocols.

   -  creates a permanent URL for the view.

      -  Lets the user download a layer definition file, to bring the
         data into QGIS \*

      -  can be used to view the maps in the browser OpenLayers or
         Leaflet or QGIS-Server some other javascript web viewing
         library. The map tiles could be served by MapServer or some
         other web mapping software.

      -  This webpage should have some options for managing the view,
         such as deleting it or refreshing the materialized view from
         the current data.

      -  If MapServer is used, this means that a custom mapfile needs to
         be generated off of a template mapfile, like what Mapit 1.x
         does.

   -  creates a permanent URL that serves as both a WMS endpoint and a
      WFS endpoint, so that the maps can be imported via the URL into
      GIS platforms like QGIS. These services should also have
      appropriate metadata, such as the human-readable description of
      the map.

Design spec
-----------

The `design spec <docs/spec/initial_spec.md>`_ is a series of stories
in development priority sequence.

Adding Views to the Project
---------------------------

There are five files you need to create, which you can copy
from another view.  If your view is named my_view the five
files are:

- ``my_view.sql`` This is the templated SQL for the 3-column
  view of data.  Use double curly brackets around anything
  you want the user to specify, such as ``{{year}}` or
  ``{{scenario}}``.

- ``my_view.py`` This defines the user inputs to the view, to
  be stored as columns in a database table. Things you've
  templated out with curly brackets in the SQL should be
  listed here as table items.  You can also name the
  database view, and describe the view, and fine tune the
  way the sql and qlr template files will have their curly
  bracket items replaced with values.  You also can define a
  little of how the form will appear in html here, in
  particular the validators to be used on user entries.

- ``my_view_page.py`` This doesn't have to modified much,
  although you need to search-and-replace the view name, the
  table name, and put in some assignments into
  ``process_form()`` so that all of the entries come back from
  the user

- ``my_view.html`` This is the html view.  You mostly just
  have to make sure all of the user entries are in it as
  rows, unless you would like to make it prettier.

Files which end in ``_page.py`` are automatically imported
when the server starts.

Templates
--------------------------------------------------

After the views are created, MapIt2 will allow you to connect
a "template" file to your views. The templates are stored in the
``site-templates`` directory.  These are generally QGIS template
files and are changed by the XML parser to point to your new views.
Your ``my_view.py`` can suggest a template through the ``qgsfile_template`` function.
(Other kinds of files could be changed by the XML parser or by
other templating systems such as jinja2 ). Note there is a hba-dev and
hba-prd directory in site-templates, so you can test things out in development
before adding them to production.

After you think you've created the views correctly, and you've
tried to run the worker and the server, you will get an
error message that there is no table in the mapit2 apps
database to store your new views.  So you need to create
them using SQL alchemy and the migration system. Check
`migrations/README.rst` for instructions.


The ``site-templates/TENANT-DEPLOY`` directory contains templates for qgis.
The web templates are in the ``mapit2/mapit2/templates``.


Deployment Docs
--------------------------------------------------

See instructions in :doc:`DEPLOYMENT <DEPLOYMENT>`.


Debugging
--------------------------------------------------

This runs the view making process so you can see the output
and do debugging stuff.

::

    mapit2-debug --debug-make-views 528

::

    docker restart mapit2_hba_prd_svr mapit2_hba_prd_work_qgs mapit2_hba_prd_work_view redis_1
    docker logs --since '24h' redis_1


TODO
----

To avoid large schemas and big storage, create a new schema every month
mapit2_schema_YYYYMM to store the views in.  As well, if a materialized view
has no data, we should trap the error and refresh the view.  That way we
can reclaim space if desired by refreshing materialized views with no data.

Send logs to ``/var/logs/mapit2``.
