#
# mapit2/Makefile ---
#

_default:

include ./Makefile.inc

#####

# for redis-cli
_apt_get_install:
	sudo apt-get install redis-tools

#####

sys_python3_exe=$(shell PATH=/usr/local/bin:/usr/bin:${PATH} type -p python3)
pip3_cmd:=${MAPIT2_VE_DIR}/bin/python3 -m pip

_ve_build:
	mkdir -p "$(dir ${MAPIT2_VE_DIR})"
#
	${sys_python3_exe} -m venv "${MAPIT2_VE_DIR}"
	${pip3_cmd} install --upgrade pip
#
ifeq (${MAPIT2_DEPLOY},dev)
# only need these in dev mode.
	${pip3_cmd} install --upgrade autopep8 isort
	${pip3_cmd} install --upgrade black
endif
#
	${pip3_cmd} install --upgrade alembic
#
	${pip3_cmd} install --upgrade rq rq-dashboard
#
	${pip3_cmd} install -e ./mapit2

# restview is handy for looking at ReStructuredText .rst files in the browser
	${pip3_cmd} install restview

# @todo: some sort of sql reformatter?
#        perhaps "sqlparse"?

_ve_rm:
	-rm -rf "${MAPIT2_VE_DIR}"

_ve_rebuild: _ve_rm _ve_build

${MAPIT2_VE_DIR}:
	make _ve_rebuild

_ve_exists: ${MAPIT2_VE_DIR}

#####

autopep8_files:=
autopep8_files+=$(wildcard mapit2/*.py)
autopep8_files+=$(wildcard mapit2/mapit2/*.py)
autopep8_files+=$(wildcard mapit2/mapit2/*/*.py)
autopep8_files+=$(wildcard test/*.py)

# When someone else owns the files, take them back.
_chown_user:
	sudo chown ${USER} -R .

_ansible_deploy:
	cd ./ansible && make ${@}

_mapit2_build:
	cd ./docker && make ${@}

#####

_docs_all _docs_rsync_to_swdocs:
	cd sphinx && make ${@}

#
# @TODO: check the '*.sql' starts with 'SELECT'
_precommit+=_ve_rebuild
_precommit+=_isort
_precommit+=_autopep8
_precommit+=_test
_precommit+=_docs_all

_precommit: ${_precommit}

#####

# Dont mess with non-DEV databases.
_test_check_for_DEV:
	$(if $(findstring dev,${MAPIT2_DEPLOY}),echo "### DEV check ok.",$(error Only run tests in DEV))

_test_unit:
	cd ./tests && make ${@}

_test+=_test_check_for_DEV
_test+=_ve_exists
_test+=_test_unit

_test: ${_test}

##### DEBUG

# Load the "PRD" setup, as these errors are in PRD.
# source ./configs/hba-prd/setup.env

# "392" had an error - here we run it again so we can debug it.
_debug_make_views_392:
	mapit2-debug --debug-make-views 392

# "416" did as well.
_debug_make_views_416:
	mapit2-debug --debug-make-views 416

#####

#
_server_run:
	mapit2-server

# an alias...
_worker_run: _worker_run_work_both

_worker_run_work_both:
	mapit2-rq-worker --work-qgs --work-view

_worker_run_work_qgs:
	mapit2-rq-worker --work-qgs

_worker_run_work_view:
	mapit2-rq-worker --work-view

# The optional RQ dashboard
_rq_dashboard_run:
	rq-dashboard
