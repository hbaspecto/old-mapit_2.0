Title: Mapit 2.0 Employment Spec
Author: John Abraham
Date: 2019-12-13

# A SQL Alchemy and Flask based web crosstabber and WMS/WFS Server 

We are going to rebuild the MapIt functionality, but rather than
rebuilding everything, we are going to start small by building
a new product focused on mapping employment patterns. This is ideal 
because the existing MapIt does not support employment outputs.  

## Final Components

1. A browser page for users to specify the input parameters for post, specifically
    
    * scenario or 2 scenarios
    * set of occupation codes
    * set of industry codes
    * begin year and end year
    
2. A api endpoint for the post that then does the following

    * creates a permanent view in the analysis schema that sums the employment amount 
    by TAZ for the user-selected categories and years

    * creates a materialized view in the analysis schema that crosstabs the amounts
    from the view above, so
    there's a row for each TAZ and a column for each year

    * creates a view in the analysis schema that uses the above view, but 
    connects it to the TAZ geometry and also adds some supporting columns 
    (percent change from begin year to end year,
    land area of TAZ,  etc.)

    * Adds metadata that describes the selection and the map in both human readable
    terms and computer readable terms. 
    
3. Create web URLS for browser and web services.

    * This could use MapServer or some other more modern geoserver product to draw web maps and 
    also to serve map tiles and map data over standard protocols.  

    * creates a permanent URL that can be used to view the maps using OpenLayers
    or Leaflet or some other javascript web viewing library.  The map tiles
    could be served by MapServer or some other web mapping software.

        * This webpage should have some options for managing the view, such as deleting
        it or refreshing the materialized view from the current data. 
    
        * If MapServer is used, this means that a custom mapfile needs to be 
        generated off of a template mapfile, like what Mapit 1.x does. 
    
    * creates a permanent URL that serves as both a WMS endpoint and a WFS endpoint, 
    so that the maps can be imported via the URL into GIS platforms like QGIS.  These
    services should also have appropriate metadata, such as the human-readable description
    of the map.
    
## Stories

### PL/SQL function that generates crosstab views of ZonalMakeUse

We already have a function output.generate_crosstab_from_mapit_query_params,
we should call it inside another function that then uses 
EXEC SQL EXECUTE IMMEDIATE
to create the materialized view and the geom view.  This is then useful immediately
for clients, but also serves as a prototype function that can be called by (or ported 
to python within) the webserver

- View 1 is the materialized view containing the crosstab query
- View 2 just joins it to the geom

###  Webserver with API Post that creates views for users

Tasks:

1. Flask webserver that talks to an existing (dev/test/stg) mapit database.
2. SQL Alchemy connection to the test/dev/stg mapit database
   1. Uses SQL Alchemy Reflection to reflect on the existing mapit database
3. API endpoint that accepts POST requests, and returns an error message if 
the entries are not validated (occupation codes invalid, industry codes invalid, scenario 
names invalid, years do not exist in scenarios, etc.)
4. Creating the view of the data:
    ```
    select scenario, year_run, sum(amount)... where 
    scenario in {scenario1,scenario2} and year > begin_year and year < end_year and occupation in {occupations} and
    industry in {industries} 
    group by scenario, year_run 
    ```
as well as the crosstab materialized view with one column for year.

POST request should return JSON results with the list of the views, or html results
with the names of the views.   
    
### Connect API Post to a user UI

Browser web page that allows user to select scenario or 2 scenarios, occupation codes,
industry codes, begin year and end year, and then posts the data, and returns either
the validation error message or the names of the views that were created.

Tasks:

1. Build a form
2. Pretty up the form
3. Test the form

### Generate GIS view

Create a view that connects the materialized crosstab view to the zone system
GIS

Tasks:

1. Decide on extra fields (percent change last-year minus first-year, absolute change, land area
of TAZ, and perhaps others)
1. Code to build the view.
2. Test importing the GIS view to QGIS.

### Static map of GIS view

User should be able to view a map of their results on a static web page.

Tasks:

1. Choose a web mapping technology (e.g. MapServer)
2. Connect the web mapping technology to the views, e.g. by writing code that takes a 
template MapServer 'mapfile' and inserts the appropriate view-specific information
3. Change the POST request from the browser to return a static map view if html is
returned, if JSON is returned the url of the image file should be returned along with the
names of the views
4. Add context to static map (background roads, town outlines, etc.)

### WFS and WMS URL

The user should be able to import the map into their favourite GIS using WFS and
WMS. 

Tasks:

1. Determine how the web mapping technology serves WFS and WMS, and create and configure
using code a URL for WFS/WMS for each of the user's queries.
2. Test to make sure it works with QGIS, ArcGIS, perhaps Google Earth. 
3. Test scrolling and zooming for both WFS and WMS


### WFS and WMS URL work with MapIt Plugin

The MapIt import plugin brings in WMS layers with their metadata from the URL. WMS
import should support charts only, as well as charts+context.  The Mapit
import plugin brings in WFS layers _with their metadata_ from the URL and also styles the
layers using equal interval choroplethic maps, and also creates bar charts or pie charts
that are initially turned off.

Tasks:

1. Test MapIt import plugin with WMS link
2. Test MapIt import plugin with WFS link. 
3. Make changes to the plugin or the WFS/WMS service until they work together

### Web dynamic maps

Users should be able to pan around the map and zoom in and out using their browser.

Tasks:

1. Choose javascript technology, either Openlayers or Leaflet unless there is another option.
2. Setup map portal html view to be opened when a user clicks on the static map. 
3. Test panning and zooming with real users
4. Users should be able to turn off layers and turn on layers (although with mapserver
the chart objects are rendered as part of the background, so turning of context was not 
possible in openlayers.)

### Web view deletion and other maintenance

Maps created that were never viewed can eventually be deleted, but their URL, POST parameters,
and their initial simple view
should be permanently stored in case anyone asks for them again by URL.  Maps used for
reports should be flagged so their views, mapfiles, and urls are never deleted.

Tasks:

1. Buttons to mark a view as permanent (used in a report, so never to be deleted) or temporary (just goofing around, can be deleted).
2. Button to delete all but the original (uncrosstabbed) view (and of course not the record of the URL and the associated POST parameters)
3. Webserver should auto renerate materialized view, gis view, and geoserver entries (e.g. mapfile) from the original (uncrosstabbed) view, if they were deleted.
2. Cron tasks that deletes materialized views more than a month old that are flagged as temporary. 

### POST from MapIt plugin

User should be able to select the parameters from within the mapit plugin,
either a WebView to the html form or a QT form.  Then, mapit plugin should
be able to POST and return any validation error message.  If there are no
errors, the Mapit plugin should import the resulting URL 
just the same as if the user had done the selection and POST from their browser. 

## Sprints


