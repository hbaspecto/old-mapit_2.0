QGIS package installer does not have the server, so we need to install the brew version

```
brew edit osgeo-qgis-res
# remove   #depends_on "pygobject" and  #depends_on "pygtk" as these don't exist anymore
brew edit osgeo-qgis
brew install qgis
