-- FUNCTION: output.generate_crosstab_from_3_col_query(text, text, text, double precision, text, text, text, text, text)

-- DROP FUNCTION output.generate_crosstab_from_3_col_query(text, text, text, double precision, text, text, text, text, text);

CREATE OR REPLACE FUNCTION output.generate_crosstab_from_3_col_query(
	src_query text,
	output_qry_name text,
	category_name text,
	default_value double precision DEFAULT NULL::double precision,
	schema text DEFAULT 'analysis'::text,
	prefix_col text DEFAULT 'sum'::text,
	suffix_col text DEFAULT 'quantity'::text,
	geom_table text DEFAULT NULL::text,
	geom_zone_column text DEFAULT 'luz'::text)
    RETURNS void
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
    sql_src_query text;  -- add 'select * from' to the beginning of the query, if it isn't already there
    sql_col_query text; -- query to find the distinctive column names in order
    sql_colnames  text; -- output of query to find the distinctive column names
    col_names     text[]; -- the actual distinctive column names
    sql_generate  text[]; -- this gets built up as the command to generate the materialized view
    record_sel    text[];
    record_types  text[];
    geom_create_string text;
    default_value_string text;
BEGIN
	-- First make sure it's a Select query, e.g. the user may have given a table name instead of a SELECT * FROM query.
    IF upper( left( ltrim( src_query ), 6 ) ) = 'SELECT' THEN
        sql_src_query := src_query;
        src_query := '(' || src_query || ') srcq';
    ELSE
        sql_src_query := 'SELECT * FROM ' || src_query;
    END IF;

    -- This is to get our column names
    sql_col_query :=
        'SELECT DISTINCT "' || category_name ||
        '"  FROM ' || src_query ||
        ' ORDER BY 1 ';
    sql_colnames :=
        'SELECT array_agg( "' || category_name || '" )' ||
        '  FROM (' || sql_col_query || ') cols;';
    EXECUTE sql_colnames INTO col_names;
    raise notice 'Colnames: %', col_names;
	if col_names is Null then
		raise exception 'Colnames is null, query to retreive column names was %', sql_colnames;
	end if;

	-- now we start building up our big ugly crosstab query.  First we set up the "create materialized view" part and specify the first column name
    sql_generate := ARRAY[
        'CREATE materialized VIEW ' || schema || '."' || output_qry_name || '" AS ',
        '    SELECT zonenumber ,'
    ];
	raise notice 'SQL generate part 1: %', sql_generate;

	-- Now we have to remorgify our column names in the right syntax for the dumb crosstab function, but we get
	-- to add a prefix and a suffix to them conveniently if we want, so the user knows its the "sum_2011_demand" or whatever.
	default_value_string := case when default_value is null then 'NULL' else default_value::text end;
    record_sel := (
        SELECT array_agg( concat( '           ',
                                   'coalesce( ct."', col, '", ', default_value_string, ' ) AS "',
                                   prefix_col, col, suffix_col, '"' ) )
          FROM unnest( col_names ) AS col
    );

	raise notice 'Record selection: %', record_sel;

	-- Now we build up most of the dumb crosstab syntax that PostgreSQL tablefunc actually needs
    sql_generate := sql_generate || ARRAY[
        array_to_string( record_sel, E',\n' ),
        '      FROM crosstab(',
        '          ' || quote_literal( sql_src_query ) || '::text,',
        '          ' || quote_literal( sql_col_query ) || '::text',
        '      ) as ct(',
        '          zonenumber integer,'
    ];

	raise notice 'SQL generate part 2: %', sql_generate;

    -- but we also need to specify the output types of the crosstab amounts, assume they are double precision
    record_types := (
        SELECT array_agg( concat( '          "',  col, '" double precision' ) )
          FROM unnest( col_names ) AS col
    );
 	raise notice 'Record types: %', record_types;

 	-- Add the output types to the query, and sort it as well.
    sql_generate := sql_generate || ARRAY[
        array_to_string( record_types, E',\n' ),
        '      )',
        '  ORDER BY 1;'
    ];
 	raise notice 'SQL generate part 3 (final): %', sql_generate;

   	-- Now we finally execute the SQL, which generates the crosstab materialized view so we can use it.
    EXECUTE array_to_string( sql_generate, E'\n' );

    if geom_table is not null then
    	geom_create_string := 'create view ' || schema || '."' || output_qry_name ||'_geom" '
			'as select a.*, b.geom from ' || schema || '."'|| output_qry_name ||'" a join "' || geom_table || '" b on (a.zonenumber = b."' || geom_zone_column ||'");';
		raise notice 'geom view query: %', geom_create_string;
		execute geom_create_string;
    end if;

END;
$BODY$;



