create table manual_views.detached_use_rates as
select ur.*, l.geom from
(SELECT a.zonenumber, -(sum(a.probability * b.quantity * c.use_rate))/(sum(a.probability * b.quantity)) as use_rate,
                                   
                                    -sum(a.probability * b.quantity * c.use_rate) AS space_ued,
                                    sum(a.probability * b.quantity) AS number_of_hh,
                                   
                                    a.year_run,
                                    a.scenario
                                   FROM output.all_technology_choice_widx a
                                     JOIN output.activity_numbers an ON an.activitynumber = a.activity
                                     JOIN output.all_activity_locations b ON an.activity::text = b.activity::text AND a.scenario::text = b.scenario::text AND a.zonenumber = b.zonenumber AND a.year_run = b.year_run
                                     JOIN output.option_use_rates c ON a.option_id = c.option_id
                                     JOIN output.technology_space_type x ON a.option_id = x.id
                                  WHERE an.activity_type_id = 2 AND a.scenario::text = 'I240'::text AND a.year_run = 2016 AND an.activity != 'HH_RCPatients'
and x.commodity in ('R011 - Detached Economy','R012 - Detached Luxury') and b.quantity > 0 and a.probability > 0
--and a.zonenumber in (2234,2222,2223)
                                  GROUP BY a.zonenumber, a.scenario, a.year_run
                                  order by  -(sum(a.probability * b.quantity * c.use_rate))/(sum(a.probability * b.quantity))) ur join
                                  luz_v4 l on ur.zonenumber = l.luz




select ur.*, l.geom from
(SELECT a.zonenumber, -(sum(a.probability * b.quantity * c.use_rate))/(sum(a.probability * b.quantity)) as use_rate,
 -sum(a.probability * b.quantity * c.use_rate) AS space_ued,
   sum(a.probability * b.quantity) AS number_of_hh,
                                   
                                    a.year_run,
                                    a.scenario
                                   FROM output.all_technology_choice_widx a
                                     JOIN output.activity_numbers an ON an.activitynumber = a.activity
                                     JOIN output.all_activity_locations b ON an.activity::text = b.activity::text AND a.scenario::text = b.scenario::text AND a.zonenumber = b.zonenumber AND a.year_run = b.year_run
                                     JOIN output.option_use_rates c ON a.option_id = c.option_id
                                     JOIN output.technology_space_type x ON a.option_id = x.id
                                  WHERE an.activity_type_id = 2 AND a.scenario::text = 'Q241i'::text AND a.year_run = 2020 AND an.activity != 'HH_RCPatients'
and x.commodity like ('R%') and b.quantity > 0 and a.probability > 0
--and a.zonenumber in (2234,2222,2223)
                                  GROUP BY a.zonenumber, a.scenario, a.year_run
                                  order by  -(sum(a.probability * b.quantity * c.use_rate))/(sum(a.probability * b.quantity))) ur join
                                  luz_v4 l on ur.zonenumber = l.luz


