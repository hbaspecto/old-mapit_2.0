#
# mrsgui/setup.py ---
#

from __future__ import (
    absolute_import,
)

import os
from distutils.core import (
    setup,
)

#####

setup(
    name="sd-editor-viewer",
    version="0.0.1",
    description="Some python to build SD views and edit SD inputs",
    author="HBA Specto Incorporated",
    url="www.hbaspecto.com",
    install_requires=[
        "psycopg2-binary",
        # "flask",
        # "flask-sqlalchemy",
        "WTForms",
        # "flask_table",
        # "flask_wtf",
        "Jinja2"
        # "git://bitbucket.org/hbaspecto/hba-pg-csv",
    ],
    packages=[
        "sd-editor-viewer",
    ],
    package_dir={
        'sd-editor-viewer': 'sd_editor_viewer',
    },
    # entry_points={
    #    'console_scripts': [
    #        "mapit2-server = mapit2.app:server_run",
    #        "build_app_tables = mapit2.appbase:build_tables",
    #    ],
    # },
)
