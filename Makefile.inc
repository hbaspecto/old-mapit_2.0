#
# mrsgui/Makefile.inc ---
#

# Usage: include ./Makefile.inc

ifeq (${MAPIT2_DIR},)
  $(error source ./hba-setup.env)
endif

__include_default:
	@echo "Makefile.inc: The first target should be before 'Makefile.inc'."
	exit 1

SHELL:=/bin/bash
.SUFFIXES:

#####

psql_cmd:=psql \
	-v ON_ERROR_STOP=1 \
	--pset pager=off

psql_cmd_superuser:=PGPASSWORD=${MAPIT2_SUPERUSER_PGPASSWORD} \
	${psql_cmd} \
	-U ${MAPIT2_SUPERUSER_PGUSER} \

_psql_cmd:
	${psql_cmd}

_psql_cmd_superuser:
	${psql_cmd_superuser}


#####

python3_exe:=${MAPIT2_VE_DIR}/bin/python3

pip_cmd:=${python3_exe} -m pip
unittest_cmd:=${python3_exe} -m unittest

#####

autopep8_files ?= $(wildcard */*.py */*/*.py)

_isort:
	isort \
	  --multi-line 3 \
	  --trailing-comma \
	  --line-width 1 \
	  ${autopep8_files}

_autopep8:
	autopep8 \
	  --aggressive \
	  --in-place \
	  --max-line-length 120 \
	  ${autopep8_files}

_pylint:
	pylint --py3k ${autopep8_files}

#####

_delete_backup_files:
	find . \( -name \*~ -o -name \*.pyc \) -print -delete
