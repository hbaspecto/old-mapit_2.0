--
-- mapit_2.0/scripts/cleanup-bad-mapview-rows.sql ---
--
-- look for bad rows and clean them up.
-- This only cleans up the mapview rows, not the generated views.
--
-- \i ./scripts/cleanup-bad-mapview-rows.sql

select id, rqjob_status, created_by, description_field
from mapit2_app.mapview
where rqjob_status is null or 
      rqjob_status in ('waiting') or
      created_by in ('test')
order by id desc
;

-- uncomment to delete.
/*
delete 
from mapit2_app.mapview cascade
where rqjob_status is null or
      rqjob_status in ('waiting') or
      created_by in ('test')
--*/
