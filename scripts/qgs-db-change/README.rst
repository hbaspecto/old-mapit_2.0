mapit_2.0/scripts/qgs-db-change/README
==================================================

The ``qgs-util`` script can be used to change the DB
settings inside a ``QGS`` file.  This is needed when the QGS
file is used at a different site than it was created at.

The ``mapit-2`` server uses this code to generate QGS files
which are modified for the site they will be used at.

Usage
--------------------------------------------------

The script is part of
`mapit_2.0 <https://bitbucket.org/hbaspecto/mapit_2.0/src/master>`_.
So you will need a copy of the code to use it.

Once you have it checked out:

::

    cd mapit_2.0
    cd ./mapit_2.0/scripts/qgs-db-change

    # Set env vars to what you want t
    export PGHOST=...

    qgs-util \
      --output-suffix ".edmonton" \
      --db-change \
      FILE.qgs

To make this even quicker, ``qgs-db-change-edmonton`` has
these settings in the script. To use it:

::

    qgs-db-change-edmonton  FILE.qgs
